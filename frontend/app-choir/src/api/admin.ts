import axios from 'axios';
import { Merge } from 'type-fest';

import { IUserStatus } from './common';

export interface IUsersParams {
  user_status: IUserStatus;
  company: number;
  id: number;
  courses__title: string;
  courses__id: string;
  ordering: string | 'id' | '-id';
  page: number;
  page_size: number;
}

export type ILastTestPassings = {
  id: number;
  task: number;
  is_final_task: boolean;
  success_passed: number;
  response_rate: string;
  start_time: string;
  out_of_time?: boolean;
  finish_time: string;
  travel_time?: number;
  retake_seconds?: number;
}[];

export type IUserCourse = {
  id: number;
  title: string;
  description?: string | null;
  author?: number;
  last_test_passings?: ILastTestPassings;
};

export type IUser = {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  description: string | null;
  design: number;
  webinars: boolean;
  password?: string;
  old_password: string;
  user_status: IUserStatus;
  is_active: boolean;
  company: number;
  company_title: string;
  curator_company?: number[];
  time_limitation: string | null;
  date_joined: string;
  courses?: IUserCourse[];
  start_course: string;
  last_passing_dt: string | null;
  end_course: string;
  teacher_chat: boolean;
  group_chat: boolean;
  tech_chat: boolean;
  on_check: number;
  finish_time_exam?: string;
  company_name?: string;
  course_id?: number;
  last_test_passings: ILastTestPassings;
};

export interface IUsersRes {
  count: number;
  next?: string | null;
  previous?: string | null;
  results: IUser[];
}

export async function getUsers(params?: Partial<IUsersParams>) {
  try {
    const { data } = await axios.get<IUsersRes>(`/api/v1/users/user/`, { params });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export type TRequiredCreateUser = Pick<IUser, 'username' | 'password'>;
export type TCreateUser = Merge<Partial<IUser>, TRequiredCreateUser>;

export async function createUser(model: TCreateUser) {
  try {
    const { data } = await axios.post<{ message: string }>('/api/v1/users/user/', model);

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export interface IMateralPassingParams {
  id?: number;
  user?: number;
  material?: number;
  material__course?: string;
  status?: string;
  ordering?: string;
}

export interface IMaterialPassing {
  id: number;
  created: string;
  updated: string;
  status: 0 | 1 | 2;
  material: number;
  user: number;
}

export async function getMaterialPassings(params?: IMateralPassingParams) {
  try {
    const { data } = await axios.get<IMaterialPassing[]>('/api/v1/courses/material_passing/', {
      params,
    });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export interface IMaterialsParams {
  id?: string;
  course?: string;
  title?: string;
  is_download?: boolean;
  created?: string;
  parent?: string;
  ordering?: string;
  search?: string;
}

export interface IMaterial {
  id: number;
  course: number;
  title: string;
  rank: number;
  text: string;
  video: string | File;
  video_link: string;
  pdf: string | File;
  is_download: boolean;
  parent?: number | null;
  original_link: number;
  original_data?: string;
  created: string;
  updated: string;
  is_active: boolean;
}

export async function getMaterials(params?: IMaterialsParams) {
  try {
    const { data } = await axios.get<IMaterial[]>(`/api/v1/courses/material/`, { params });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export interface IAppVersion {
  apikey: string;
}

export async function updateAppVersion(model: IAppVersion) {
  try {
    const { data } = await axios.put<string>('/api/v1/conference/apikey/change/', model);

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}
