import { useMutation, useQuery } from 'react-query';

import {
  IMaterialPassingResponse,
  TPassingStatus,
  createMaterialPassing,
  getMaterialPassings,
  updateMaterialPassing,
} from '@/api/client';
import { ICurrentUser } from '@/api/common';
import { queryClient } from '@/hooks/queryClient';

const getPassingByMaterialKey = (material: number) => ['passing-by-material', material];

export const usePassingByMaterial = (material: number) => {
  const key = getPassingByMaterialKey(material);

  return useQuery(key, () => getMaterialPassings({ material }), {
    refetchOnWindowFocus: false,
    refetchOnMount: false,
  });
};

export const useCheck = (
  material: number,
  onError: () => void,
  lastPassing?: IMaterialPassingResponse,
) => {
  const key = getPassingByMaterialKey(material);

  return useMutation<unknown, unknown, { status: TPassingStatus; user: ICurrentUser }>(
    async ({ status, user }) => {
      try {
        if (lastPassing) {
          const res = await updateMaterialPassing({
            id: lastPassing?.id,
            status: status,
            material,
            user: user.id,
          });

          queryClient.setQueryData(key, [res]);
        } else {
          const res = await createMaterialPassing({
            material,
            user: user.id,
            status: status,
          });

          queryClient.setQueryData(key, [res]);
        }
      } catch (e) {
        throw Error();
      }
    },
    { onError },
  );
};
