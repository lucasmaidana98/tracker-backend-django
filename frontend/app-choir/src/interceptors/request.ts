import axios from 'axios';
import Cookie from 'js-cookie';

import { cookieKeys } from '@/hooks/queryClient';

axios.interceptors.request.use(async (requestConfig) => {
  if (requestConfig.url) {
    if (!/^\/api\/v1\/token/.test(requestConfig.url)) {
      const access = Cookie.get(cookieKeys.token);
      if (access) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        requestConfig.headers = {
          ...requestConfig.headers,
          Authorization: `Bearer ${access}`,
        };
      }
    }
  }

  return requestConfig;
});
