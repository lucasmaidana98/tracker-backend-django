import React from 'react';

import { Box } from '@mui/material';
import { green, orange, red } from '@mui/material/colors';

const CheckItem: React.FC<{
  active?: boolean;
  onClick: () => void;
  value: 0 | 1 | 2;
  children: React.ReactElement | string;
}> = ({ active, onClick, value, children }) => {
  const getColor = () => {
    switch (value) {
      case 1:
        return red[500];
      case 2:
        return green[500];
      default:
        return orange[500];
    }
  };
  return (
    <Box
      onClick={onClick}
      sx={{ px: 2, py: 1, cursor: 'pointer' }}
      style={{
        color: active ? '#fff' : getColor(),
        background: active ? getColor() : '#f1f1f1',
      }}
    >
      {children}
    </Box>
  );
};

export default CheckItem;
