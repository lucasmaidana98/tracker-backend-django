import TextField from '@mui/material/TextField';
import { styled } from '@mui/material/styles';

const Input = styled(TextField)({
  '& .MuiInputBase-root': {
    width: '100%',
  },
  '& label.Mui-focused': {
    color: 'black',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: 'black',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: 'black',
    },
    '&:hover fieldset': {
      borderColor: 'black',
    },
    '&.Mui-focused fieldset': {
      borderColor: 'black',
      borderWidth: 1,
    },
  },
});

export default Input;
