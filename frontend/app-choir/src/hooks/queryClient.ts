import { QueryClient } from 'react-query/core';

export const queryClient = new QueryClient();

export const queryKeys = {
  APP_VERSION: 'app-version',
};

export const cookieKeys = {
  token: 'access',
};
