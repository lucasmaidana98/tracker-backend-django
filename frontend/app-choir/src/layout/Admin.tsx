import { useState } from 'react';
import { useQuery } from 'react-query';

import { ArrowCircleLeftOutlined, ArrowCircleRightOutlined } from '@mui/icons-material';
import { Box, Grid } from '@mui/material';

import { getMaterials, getUsers } from '@/api/admin';
import Meeting from '@/containers/Meeting';

const Admin: React.FC = () => {
  const { data: users } = useQuery('users', () => getUsers({ courses__id: '4' }));
  const { data: materials } = useQuery('materials', () =>
    getMaterials({ is_download: true, course: '4' }),
  );
  const [currentCol, setCurrentCol] = useState(0);

  const meetings = materials ? materials?.filter((m) => !!m.parent).map((m) => m) : [];
  const titles = meetings.filter((_, i) => i === currentCol).map((m) => m.title);

  const clients = users?.results.filter((u) => u.user_status == 2) || [];

  const rows = clients.map((u) => {
    return meetings
      .filter((_, i) => i === currentCol)
      .map((m, i) => <Meeting key={i} user={u.id} material={m.id} first_name={u.first_name} />);
  });

  const hasPrev = currentCol !== 0;
  const hasNext = currentCol < (meetings?.length || 0) - 1;

  return (
    <Box sx={{ p: 2 }}>
      <Box sx={{ display: 'flex', justifyContent: 'space-between', py: 2 }}>
        <ArrowCircleLeftOutlined
          onClick={() => hasPrev && setCurrentCol(currentCol - 1)}
          sx={{ opacity: hasPrev ? 1 : 0.2 }}
        />
        {titles}
        <ArrowCircleRightOutlined
          onClick={() => hasNext && setCurrentCol(currentCol + 1)}
          sx={{ opacity: hasNext ? 1 : 0.2 }}
        />
      </Box>
      <Grid>{rows.map((r) => r)}</Grid>
    </Box>
  );
};

export default Admin;
