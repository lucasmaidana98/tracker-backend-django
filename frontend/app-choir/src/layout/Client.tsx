import { useQuery } from 'react-query';

import { Box } from '@mui/material';

import { getAllMaterials } from '@/api/client';
import Check from '@/containers/Check';

const Client: React.FC = () => {
  const { data: materials } = useQuery('materials', () =>
    getAllMaterials({ is_download: true, course: 4 }),
  );

  return (
    <Box sx={{ p: 2 }}>
      {materials?.map((m) => (
        <Box
          key={m.id}
          sx={{
            fontWeight: !m.parent ? 'bold' : 'normal',
            display: 'flex',
            alignItems: 'center',
            mb: !m.parent ? 3 : 2,
            justifyContent: !m.parent ? 'center' : 'space-between',
          }}
        >
          <span>{m.title}</span>
          {!!m.parent && <Check material={m.id} />}
        </Box>
      ))}
    </Box>
  );
};

export default Client;
