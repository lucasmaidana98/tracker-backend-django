import { useEffect, useState } from 'react';

import { Box } from '@mui/material';

import { TPassingStatus } from '@/api/client';
import { useCheck, usePassingByMaterial } from '@/cache/client';
import { useCurrentUser } from '@/cache/common';
import CheckItem from '@/components/CheckItem';

const Check: React.FC<{
  material: number;
}> = ({ material }) => {
  const [value, setValue] = useState<TPassingStatus>(0);
  const { data: user } = useCurrentUser();

  const { data: passings, isLoading } = usePassingByMaterial(material);
  const lastPassing = passings?.length ? passings[0] : undefined;

  const { mutate } = useCheck(material, () => setValue(lastPassing?.status || 0), lastPassing);

  useEffect(() => {
    lastPassing && setValue(lastPassing.status);
  }, [lastPassing]);

  const handleChange = (status: TPassingStatus) => {
    if (!user) return;
    setValue(status);
    mutate({ status, user });
  };

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        textTransform: 'uppercase',
      }}
    >
      <CheckItem onClick={() => handleChange(2)} active={!isLoading && value === 2} value={2}>
        ✓
      </CheckItem>

      <CheckItem onClick={() => handleChange(0)} active={!isLoading && value === 0} value={0}>
        ?
      </CheckItem>

      <CheckItem onClick={() => handleChange(1)} active={!isLoading && value === 1} value={1}>
        ×
      </CheckItem>
    </Box>
  );
};

export default Check;
