import { useQuery } from 'react-query';

import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import { Chip } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

import { getMaterialPassings } from '@/api/admin';

const Meeting: React.FC<{ user: number; material: number; first_name: string }> = ({
  material,
  user,
  first_name,
}) => {
  const { data: passings, isLoading } = useQuery(
    ['passings-by-users-materials', material, user],
    () => getMaterialPassings({ material, user }),
    { refetchOnWindowFocus: false, refetchOnMount: false },
  );
  const lastPassing = passings?.length ? passings[0] : undefined;

  return (
    <>
      <Chip
        size="medium"
        variant="outlined"
        color={`${
          lastPassing
            ? lastPassing?.status === 2
              ? 'success'
              : lastPassing?.status === 1
              ? 'error'
              : 'warning'
            : 'default'
        }`}
        onDelete={() => null}
        deleteIcon={
          lastPassing ? (
            lastPassing?.status === 2 ? (
              <CheckIcon />
            ) : lastPassing?.status === 1 ? (
              <CloseIcon />
            ) : (
              <QuestionMarkIcon />
            )
          ) : (
            <></>
          )
        }
        label={isLoading ? <CircularProgress size={10} sx={{ color: '#000' }} /> : first_name}
        sx={{ m: '2px', opacity: !lastPassing || lastPassing.status === 0 ? 0.3 : 1 }}
      />
    </>
  );
};

export default Meeting;
