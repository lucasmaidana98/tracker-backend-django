import Chart from 'react-apexcharts';
import { useQuery } from 'react-query';

import { green, orange, red } from '@mui/material/colors';

import { getMaterialPassings, getUsers } from '@/api/admin';
import { getAllMaterialPassings, getUsersWithCurrentCourse } from '@/api/client';
import { getCurrentUser } from '@/api/common';
import { FullSizeCenteredFlexBox } from '@/components/styled';

export const Passings: React.FC<{ material: number }> = ({ material }) => {
  const { data: user } = useQuery('user', getCurrentUser);
  const isAdmin = user?.user_status === 0;
  const isClient = user?.user_status === 2;
  const { data: courseUsersClient } = useQuery('courseUsersClient', getUsersWithCurrentCourse, {
    enabled: isClient,
  });
  const { data: courseUsersAdmin } = useQuery(
    'courseUsersAdmin',
    () => getUsers({ courses__id: '4' }),
    { enabled: isAdmin },
  );
  const { data: passings } = useQuery(
    ['passings-by-material', material],
    () =>
      user?.user_status === 0
        ? getMaterialPassings({ material })
        : getAllMaterialPassings({ material }),
    { enabled: !!user },
  );

  const users = isAdmin ? courseUsersAdmin?.count || 0 : courseUsersClient?.length || 0;
  const passedUsers = passings?.filter((p) => p.status === 2)?.length || 0;
  const notPassedUsers = passings?.filter((p) => p.status === 1)?.length || 0;
  const rest = users - passedUsers - notPassedUsers;

  return (
    <FullSizeCenteredFlexBox>
      <Chart
        options={{
          chart: {
            id: 'donut-chart',
            fontFamily: "'DM Sans', sans-serif",
            foreColor: '#adb0bb',
          },
          dataLabels: {
            enabled: true,
          },
          plotOptions: {
            pie: {
              donut: {
                size: '50px',
              },
            },
          },
          legend: {
            show: true,
            position: 'bottom',
            width: '50px',
          },
          colors: [green[500], red[500], orange[500]],
          tooltip: {
            theme: 'dark',
            fillSeriesColor: false,
          },
          labels: ['Присутствие', 'Отсутствие', 'Неизвестно'],
        }}
        series={[passedUsers, notPassedUsers, rest]}
        // series={[{ name: 'Series 1' }, { data: [45] }]}
        type="donut"
        height="300px"
      />
    </FullSizeCenteredFlexBox>
  );
};
