import { useState } from 'react';
import { useQuery } from 'react-query';

import { ArrowCircleLeftOutlined, ArrowCircleRightOutlined } from '@mui/icons-material';
import { Grid, Typography } from '@mui/material';

import { getMaterials } from '@/api/admin';
import { getAllMaterials } from '@/api/client';
import { getCurrentUser } from '@/api/common';
import Meta from '@/components/Meta';

import { Passings } from './Passings';

function Page2() {
  const { data: user } = useQuery('user', getCurrentUser);
  const { data: materials } = useQuery(
    'materials',
    () =>
      user?.user_status === 0
        ? getMaterials({ is_download: true, course: '4' })
        : getAllMaterials({ is_download: true, course: 4 }),
    { enabled: !!user },
  );
  const [currentCol, setCurrentCol] = useState(0);

  const meetings = materials ? materials?.filter((m) => !!m.parent).map((m) => m) : [];

  const hasPrev = currentCol !== 0;
  const hasNext = currentCol < (meetings?.length || 0) - 1;

  const currentMaterialId = meetings?.length ? meetings[currentCol].id : undefined;

  return (
    <Grid container sx={{ p: 2 }}>
      <Meta title="Статистика хора" />
      <Typography sx={{ textAlign: 'center', width: '100%', py: 1, fontSize: 24 }}>
        Общая посещаемость
      </Typography>
      <Grid container sx={{ justifyContent: 'space-between', py: 2 }}>
        <ArrowCircleLeftOutlined
          onClick={() => hasPrev && setCurrentCol(currentCol - 1)}
          sx={{ opacity: hasPrev ? 1 : 0.2 }}
        />
        {!!meetings?.length && meetings[currentCol].title}
        <ArrowCircleRightOutlined
          onClick={() => hasNext && setCurrentCol(currentCol + 1)}
          sx={{ opacity: hasNext ? 1 : 0.2 }}
        />
      </Grid>
      {currentMaterialId && <Passings material={currentMaterialId} />}
    </Grid>
  );
}

export default Page2;
