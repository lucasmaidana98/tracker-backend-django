import { useForm } from 'react-hook-form';

import { Box } from '@mui/material';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';

import { IAuthParams } from '@/api/common';
import Input from '@/components/Input';
import { useAuth } from '@/store/auth';

const LoginForm: React.FC = () => {
  const { register, handleSubmit, reset, formState } = useForm<IAuthParams>();
  const { login, error, setError, loading, setLoading } = useAuth();

  const submit = async (data: IAuthParams) => {
    try {
      setLoading(true);
      await login(data);
      reset();
      setLoading(false);
      setError(false);
    } catch (e) {
      setError(true);
      setLoading(false);
      setTimeout(() => setError(false), 4000);
    }
  };

  return (
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 2, display: 'block', width: 'calc(100% - 30px)' },
      }}
      autoComplete="off"
      onSubmit={handleSubmit(submit)}
    >
      <Input
        helperText={error ? 'Такой хорист не найден' : ''}
        error={error}
        label="Логин"
        variant="outlined"
        color="primary"
        {...register('username', { required: true })}
      />
      <Input
        label="Пароль"
        variant="outlined"
        {...register('password', { required: true })}
        type="password"
      />

      {loading ? (
        <CircularProgress
          size={25}
          sx={{ color: 'black' }}
          style={{ left: '0', right: '0', position: 'absolute', margin: '0 auto' }}
        />
      ) : (
        <Button
          type="submit"
          variant="outlined"
          color="inherit"
          disabled={!formState.isValid || loading}
        >
          Вход
        </Button>
      )}
    </Box>
  );
};

export default LoginForm;
