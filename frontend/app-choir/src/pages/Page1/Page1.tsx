import { useCurrentUser } from '@/cache/common';
import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';

function Profile() {
  const { data: user } = useCurrentUser();

  return (
    <>
      <Meta title="Профиль" />
      <FullSizeCenteredFlexBox sx={{ flexDirection: 'column' }}>
        <div>
          <b>Имя</b>: <span>{user?.first_name}</span>
        </div>
        <div>
          <b>Логин</b>: <span>{user?.username}</span>
        </div>
        {user?.user_status === 2 && (
          <div>
            <b>Пароль</b>: <span>{user?.old_password}</span>
          </div>
        )}
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default Profile;
