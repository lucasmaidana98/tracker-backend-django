import { useEffect, useMemo, useState } from 'react';
import Chart from 'react-apexcharts';
import { useQuery } from 'react-query';

import { green, orange, red } from '@mui/material/colors';

import { IMaterialPassingResponse, getAllMaterials, getMaterialPassings } from '@/api/client';
import { getCurrentUser } from '@/api/common';

interface GroupedData {
  [key: string]: IMaterialPassingResponse[];
}

function groupDataByDate(data?: IMaterialPassingResponse[]): GroupedData {
  if (!data) return {};
  const result: GroupedData = {};
  data.forEach((item) => {
    const date = new Date(item.created);
    const month =
      date.toLocaleString('ru-RU', { month: 'long' }).charAt(0).toUpperCase() +
      date.toLocaleString('ru-RU', { month: 'long' }).slice(1);

    const formattedDate = `${month.slice(0, 3)}.`;

    if (!result[formattedDate]) {
      result[formattedDate] = [];
    }

    result[formattedDate].push(item);
  });

  return result;
}

function getMonthsDataByStatus(data: GroupedData, status: number): number[] {
  const reversedMonths = Object.keys(data).reverse();
  const result: number[] = [];

  reversedMonths.forEach((month) => {
    const monthData = data[month];
    let count = 0;
    monthData.forEach((item) => {
      if (item.status === status) {
        count++;
      }
    });
    result.push(count);
  });

  return result;
}

export const Graphic = () => {
  const { data: user } = useQuery('user', getCurrentUser);
  const { data: materials } = useQuery('all-materials-of-current-user', () => getAllMaterials({ course: 4 }));
  const materialIds = useMemo(() => materials?.map((m) => m.id), [materials]);

  const { data: passings } = useQuery('last-month-passings', () => getMaterialPassings(), {
    enabled: !!user,
  });

  const [keys, setKeys] = useState<string[]>([]);
  const [object, setObject] = useState<GroupedData>({});

  useEffect(() => {
    if (passings) {
      const filtered = passings.filter((p) => materialIds?.includes(p.material));
      const object = groupDataByDate(filtered);
      setKeys(Object.keys(object).reverse());
      setObject(object);
    }
  }, [passings, materialIds]);

  return (
    <Chart
      options={{
        chart: {
          id: 'area',
          toolbar: {
            show: false,
          },
          animations: {
            enabled: true,
          },
          clipPath: false,
        },
        stroke: {
          curve: 'smooth',
        },
        dataLabels: {
          enabled: false,
        },
        colors: [green[500], red[500], orange[500]],
        xaxis: { categories: keys },
      }}
      series={[
        {
          name: 'Присутствие',
          data: getMonthsDataByStatus(object, 2),
        },
        {
          name: 'Отсутствие',
          data: getMonthsDataByStatus(object, 1),
        },
        {
          name: 'Неизвестно',
          data: getMonthsDataByStatus(object, 0),
        },
      ]}
      type="area"
      width={window.innerWidth}
      height="300px"
    />
  );
};
