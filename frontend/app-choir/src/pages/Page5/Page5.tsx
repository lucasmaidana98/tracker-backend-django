import { useQuery } from 'react-query';

import { Typography } from '@mui/material';

import { getCurrentUser } from '@/api/common';
import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';

import { Graphic } from './Graphic';

function Page5() {
  const { data: user } = useQuery('user', getCurrentUser);

  return (
    <>
      <Meta title="График" />
      <FullSizeCenteredFlexBox flexDirection="column">
        {user?.user_status !== 2 ? (
          <Typography textAlign="center">Раздел доступен только для хористов</Typography>
        ) : user?.id === 77 ? (
          <Graphic />
        ) : (
          <Typography textAlign="center">Раздел в разработке</Typography>
        )}
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default Page5;
