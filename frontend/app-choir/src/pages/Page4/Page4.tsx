import { useQuery } from 'react-query';

import { Box, Typography } from '@mui/material';

import { getMaterials } from '@/api/admin';
import { getAllMaterials } from '@/api/client';
import { getCurrentUser } from '@/api/common';
import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';

function Page4() {
  const { data: user } = useQuery('user', getCurrentUser);
  const { data: materials } = useQuery(
    'penalties',
    () =>
      user?.user_status === 0
        ? getMaterials({ is_download: true, course: '7' })
        : getAllMaterials({ is_download: true, course: 7 }),
    { enabled: !!user },
  );
  return (
    <>
      <Meta title="Предупреждения" />
      <FullSizeCenteredFlexBox flexDirection="column">
        {!materials?.length ? (
          <Typography sx={{ color: '#999' }}>У вас нету предупреждений</Typography>
        ) : (
          materials?.map((m) => (
            <Box key={m.id}>
              {m.title} {m.text && `(${m.text})`}
            </Box>
          ))
        )}
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default Page4;
