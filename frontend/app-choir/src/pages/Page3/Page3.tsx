import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextareaAutosize,
} from '@mui/material';

import { getMaterials } from '@/api/admin';
import {
  createMaterialPassing,
  getAllMaterialPassings,
  getAllMaterials,
  updateMaterialPassing,
} from '@/api/client';
import { getCurrentUser } from '@/api/common';
import Meta from '@/components/Meta';
import { queryClient } from '@/hooks/queryClient';

const border = '1px solid rgba(224, 224, 224, 1)';

function Page3() {
  const { data: user } = useQuery('user', getCurrentUser);
  const { data: materials } = useQuery(
    'materials-yougth',
    () =>
      user?.user_status === 0
        ? getMaterials({ is_download: true, course: '6' })
        : getAllMaterials({ is_download: true, course: 6 }),
    { enabled: !!user },
  );

  return (
    <>
      <Meta title="page 3" />
      <TableContainer style={{ height: 'calc(100vh - 65px)' }}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell
                style={{
                  width: 80,
                  textAlign: 'left',
                  borderRight: border,
                  position: 'sticky',
                  left: 0,
                  background: 'white',
                  zIndex: 10,
                }}
              >
                Спевка
              </TableCell>
              <TableCell
                style={{
                  minWidth: 20,
                  textAlign: 'center',
                  borderRight: border,
                }}
              >
                Псалмы
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {materials?.map((m) => (
              <TableRow key={m.id} sx={{ height: 72 }}>
                <TableCell
                  align="left"
                  sx={{
                    borderRight: border,
                    position: 'sticky',
                    left: 0,
                    background: 'white',
                  }}
                >
                  {m.title}
                </TableCell>
                <TableCell>
                  <Passing material={m.id} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default Page3;

const Passing = ({ material }: { material: number }) => {
  const key = ['songs-by-rehersal', material];
  const { data: passings } = useQuery(key, () => getAllMaterialPassings({ material }));
  const { data: user } = useQuery('user', getCurrentUser);
  const [value, setValue] = useState('');
  const data = passings?.filter((p) => p.user === 57);

  const hasYouthAccess = user?.id === 57;

  const lastPassing = data?.length && data[0];

  useEffect(() => {
    lastPassing && setValue(lastPassing.text);
  }, [lastPassing]);

  const update = async () => {
    if (!user || !lastPassing) return;
    try {
      const res = await updateMaterialPassing({
        id: lastPassing.id,
        status: 2,
        material,
        user: user.id,
        text: value,
        userName: user.first_name,
      });

      queryClient.setQueryData(key, [res]);
    } catch (e) {
      throw Error();
    }
  };

  const [creating, setCreating] = useState(false);

  const create = async () => {
    setCreating(true);
    if (!user) return;
    try {
      const res = await createMaterialPassing({
        material,
        user: user.id,
        status: 0,
        text: value,
        userName: user.first_name,
      });
      setCreating(false);
      queryClient.setQueryData(key, [res]);
    } catch (e) {
      setCreating(false);
      throw Error();
    }
  };

  return !data?.length ? (
    <button className="button" onClick={create} disabled={creating || !hasYouthAccess}>
      Добавить псалмы
    </button>
  ) : (
    <form onSubmit={update}>
      <TextareaAutosize
        className="textarea"
        minRows={4}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onBlur={update}
        disabled={!hasYouthAccess}
      />
    </form>
  );
};
