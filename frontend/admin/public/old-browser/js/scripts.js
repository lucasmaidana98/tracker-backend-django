// browser old test
if (typeof window["Modernizr"] != "undefined") {
  if (
    !Modernizr.borderradius ||
    !Modernizr.boxshadow ||
    !Modernizr.multiplebgs ||
    !Modernizr.opacity ||
    !Modernizr.rgba ||
    !Modernizr.svg
  ) {
    if (document.cookie.indexOf("doNotUpdate=true") === -1) {
      location = "browser.html";
    }
  }
}
var ui,
  android = device.android();

$(document).ready(function () {
  $("#j-enter-form").submit(function () {
    if (ui !== undefined) $("#j-enter-form").find('input[name="ui"]').val(ui);
    else return false;
  });

  $("#demo-video").bind("click", function () {
    makeCont();
  });
});
function makeCont() {
  if (!$("#demo-video-cont").length) {
    $("body").prepend(
      '<div id="demo-video-cont"><div class="demo-video-cont"><div id="demo-video-close" class="demo-video-close"></div><iframe width="600" height="400" src="https://www.youtube.com/embed/pIaUsVq3rkU" frameborder="0" allowfullscreen></iframe></div><div class="modal-cont-bg"></div></div>'
    );
  }
  $("#demo-video-close").click(function () {
    $("#demo-video-cont").remove();
  });
}

(function () {
  if ($.fn.jScrollPane != undefined) {
    $.fn.jScrollPane.defaults.mouseWheelSpeed = 50;
  }
  var _portfolio, _messages, _wrapper, _popups;

  function wrapperPosition() {
    _wrapper.height("auto");
    var bodyH = $("body").height();
    if (bodyH > 1536) {
      _wrapper.height(1536).css("top", (bodyH - 1536) / 2);
    } else {
      _wrapper.height(bodyH).css("top", 0);
    }
  }
  $(window).bind({
    load: function () {
      $("#j-space").each(function () {
        new Space(this);
      });
      $("#j-error-page-tiles").each(function () {
        new Tiles(this, { imgFolder: "images/404-tiles/" });
      });
    },
    resize: wrapperPosition,
  });

  // document init
  $(function () {
    _wrapper = $("#j-wrapper");
    wrapperPosition();
    _popups = new Popups();
    $("#j-panel").each(function () {
      new Panel(this);
    });
    $(".j-browser-old").each(function () {
      new HoverAnimate(this, { frames: 5, delay: 15, inc: 0.8 });
    });
    $("#j-feedback-form").each(function () {
      new FeedbackForm(this);
    });
    $("#j-space-text").each(function () {
      new SpaceText(this);
    });
    $("#j-login").each(function () {
      new LoginForm(this);
    });
    $("#j-recovery").each(function () {
      new RecoveryForm(this);
    });
    if ($.fn.placeholder != undefined) {
      $("input, textarea").placeholder();
    }
  });
  // /document init

  // HoverAnimate
  function HoverAnimate(container, options) {
    var _this = this,
      frames = options.frames || 1,
      delayInit = options.delay || 18,
      inc = options.inc || 1.2,
      $el = $(container),
      i = 0,
      delay = delayInit,
      intervalIdUp = 0,
      intervalIdDown = 0,
      classPrev = "";
    function up() {
      if (i < frames - 1) {
        i++;
        delay *= inc;
        intervalIdUp = window.setTimeout(up, delay);
      }
      $el.removeClass(classPrev).addClass("frame" + i);
      classPrev = "frame" + i;
    }
    function down() {
      if (i > 0) {
        i--;
        delay *= inc;
        intervalIdDown = window.setTimeout(down, delay);
      } else {
        $el.removeClass("animated");
      }
      $el.removeClass(classPrev).addClass("frame" + i);
      classPrev = "frame" + i;
    }
    this.enter = function () {
      $el.addClass("animated");
      clearInterval(intervalIdDown);
      delay = delayInit;
      intervalIdUp = window.setTimeout(up, delay);
    };
    this.leave = function () {
      clearInterval(intervalIdUp);
      delay = delayInit;
      intervalIdDown = window.setTimeout(down, delay);
    };
    $el.bind({
      "mouseenter.hoverAnimate": _this.enter,
      "mouseleave.hoverAnimate": _this.leave,
    });
  }
  // /HoverAnimate

  // Panel
  function Panel(container) {
    var panel = this,
      $panel = $(container),
      $scroll = $panel.find("#j-panel-scroll"),
      $select = $panel.find(".select"),
      button = ".panel-btn-circle",
      $panelPetals = $panel.find("#j-panel-petals"),
      petal = ".panel-petal",
      petalArrow = ".panel-petal-arrow",
      $scrollHelper = $('<br class="j-scroll-helper" />'),
      $table = $panel.find("table").eq(0),
      count = false;
    $scroll.jScrollPane({
      verticalDragMaxHeight: 0,
      showArrows: true,
      speed: $panel.width(),
    });
    $select.each(function () {
      new Select(this);
    });
    $(window).resize(function () {
      $scroll.data("jsp").scrollToPercentX(0);
      $scroll.data("jsp").reinitialise({ speed: $panel.width() });
    });
    $panel
      .on("click", petal + ", " + petalArrow, function () {
        var $petals = $panelPetals.find(petal);
        $petals
          .filter(":visible")
          .eq(0)
          .animate(
            { opacity: "hide" },
            {
              duration: 300,
              complete: function () {
                if (!$petals.filter(":visible").length) {
                  $panelPetals.hide();
                  $scroll.data("jsp").reinitialise({ speed: $panel.width() });
                }
              },
            }
          );
      })
      .on("click", button, function () {
        $(this).hide();
        $scroll.data("jsp").reinitialise({ speed: $panel.width() });
      })
      .bind({
        "panel.resize": function () {
          panelResizeHelper();
          $scroll.data("jsp").reinitialise({ speed: $panel.width() });
        },
        "panel.scrollToRight": function () {
          $scroll.data("jsp").scrollToPercentX(100);
        },
      });
    function panelResizeHelper() {
      // reinitialise у jScrollPane не работает, если контент не изменился. Поэтому данная функция имитирует изменение контента добавлением/удалением <br />
      if (!count) {
        $scroll.data("jsp").getContentPane().append($scrollHelper);
        count = true;
      } else {
        $scrollHelper.remove();
        count = false;
      }
    }
  }
  // /Panel

  // Tiles
  function Tiles(container, options) {
    var _this = this,
      container = $(container),
      cw = options.maxWidth || 2048,
      ch = options.maxHeight || 1536,
      countX = Math.ceil(cw / 256, 10),
      countY = Math.ceil(ch / 256, 10),
      imgFolder = options.imgFolder || "",
      prefixImg = options.prefixImg || "",
      prefixId = options.prefixId || prefixImg,
      exceptionStart = options.exceptionStart || -1,
      exceptionEnd = options.exceptionEnd || -1,
      ww,
      wh,
      resizeTimer = 0,
      offset,
      notBuiled = countX * countY; // количество незагруженных тайлов
    function exceptionCheck(number) {
      return exceptionStart && exceptionEnd
        ? number >= exceptionStart && number <= exceptionEnd
        : false;
    }
    this.rebuild = function () {
      if (!notBuiled) return;
      ww = $("body").width();
      wh = $("body").height();
      offset = container.offset();
      offset.bottom = offset.top + ch;
      offset.right = offset.left + cw;
      // start
      startX = offset.left >= 0 ? 0 : Math.floor(-offset.left / 256);
      startY = offset.top >= 0 ? 0 : Math.floor(-offset.top / 256);
      // end
      endX = offset.right > ww ? Math.ceil((-offset.left + ww) / 256) : Math.ceil(cw / 256);
      endY = offset.bottom > wh ? Math.ceil((-offset.top + wh) / 256) : Math.ceil(ch / 256);
      //
      for (var j = startY; j < endY; j++) {
        for (var i = startX; i < endX; i++) {
          var imgNumber = j * countX + i + 1;
          if (imgNumber < 10) {
            imgNumber = "0" + imgNumber;
          }
          if (!$("#" + prefixId + imgNumber).length && !exceptionCheck(imgNumber)) {
            var img = $('<img alt="" id="' + prefixId + imgNumber + '" />');
            img
              .attr("src", imgFolder + prefixImg + imgNumber + ".jpg")
              .css({
                position: "absolute",
                left: i * 256,
                top: j * 256,
              })
              .appendTo(container);
            notBuiled--;
          } else if (exceptionCheck(imgNumber)) {
            notBuiled--;
          }
        }
      }
      container.triggerHandler("tilesBuilded");
    };
    this.rebuild();
    $(window).resize(function () {
      if ((ww != $("body").width() || wh != $("body").height()) && !resizeTimer) {
        resizeTimer = setTimeout(function () {
          _this.rebuild();
          resizeTimer = 0;
        }, 200);
      }
    });
  }
  // /Tiles

  // Select
  function Select(container) {
    var sel = this,
      $sel = $(container),
      $radio = $sel.find(":radio"),
      $default = $sel.find(".select-default"),
      $body = $sel.find(".select-body"),
      $content = $sel.find(".select-content"),
      defaultHtml = $default.html(),
      bodyHeight = 400;
    sel.val = undefined;
    sel.contentHeight = function () {
      bodyHeight = _wrapper.height() + _wrapper.offset().top - $body.offset().top - 48;
      if ($content.height() < bodyHeight) {
        bodyHeight = $content.height();
      }
      $body.height(bodyHeight);
    };
    sel.show = function () {
      $sel.addClass("opened");
      $body.show();
      sel.contentHeight();
      if ($body.data("jsp") == undefined) {
        $body.jScrollPane();
      } else {
        $body.data("jsp").reinitialise();
      }
      sel.selBind();
      $sel.triggerHandler("show");
    };
    sel.hide = function () {
      $sel.removeClass("opened");
      $body.hide();
      $sel.triggerHandler("hide");
      //$(document).unbind('.sel');
    };
    sel.toggle = function () {
      if ($sel.hasClass("opened")) {
        sel.hide();
      } else {
        sel.show();
      }
    };
    sel.selBind = function () {
      $(document).bind({
        "mouseup.sel": function (e) {
          if (!$(e.target).closest("#" + $sel.attr("id")).length) {
            sel.hide();
          }
        },
      });
    };
    sel.reset = function () {
      $default.html(defaultHtml);
      sel.val = undefined;
    };
    sel.change = function () {
      sel.val = $(this).val();
      var $selectedItem = $(this).closest("label").clone();
      $selectedItem.find(":radio").remove();
      $default.html($selectedItem);
      sel.hide();
      ui = sel.val;
      var inputClass = $("#j-enter-form").find('input[name="schoolClass"]');
      if (inputClass.size()) {
        inputClass.val(ui);
      }
      if ($(this).data("payment-param")) {
        $("#payment-form input").attr("name", $(this).data("payment-param"));
        $("#payment-form").submit();
      }
    };
    $radio.bind({ change: sel.change, click: sel.change });
    $default.click(function () {
      sel.toggle();
    });
    sel.setDefault = function () {
      $radio.each(function () {
        if ($(this).data("default")) {
          var $selectedItem = $(this).closest("label").clone();
          $selectedItem.find(":radio").remove();
          $default.html($selectedItem);
          sel.hide();
          ui = sel.val;
        }
      });
    };
    sel.setDefault();
  }
  // /Select

  // Popups
  function Popups() {
    var pop = this;
    pop.last = [];
    pop.arr = [];
    pop.arrOn = [];
    pop.again = [];

    pop.on = function (i) {
      pop.arrOn[i] = 1;
      pop.last.push(i);
    };
    pop.off = function (i) {
      pop.arrOn[i] = 0;
    };
    pop.push = function (obj) {
      return pop.arr.push(obj) - 1;
    };
    pop.hideAll = function () {
      for (k = 0; k < pop.arr.length; k++) {
        if (pop.arrOn[k]) {
          pop.arr[k].hideExtend();
          pop.again[k] = 1;
        } else {
          pop.again[k] = 0;
        }
      }
    };
    pop.showAllHided = function () {
      for (k = 0; k < pop.arr.length; k++) {
        if (pop.again[k]) {
          pop.arr[k].showExtend();
          pop.again[k] = 0;
        }
      }
    };
    $(document).bind({
      "keyup.popup": function (e) {
        if (e.keyCode == "27") {
          for (k = pop.last.length - 1; k >= 0; k--) {
            var number = pop.last.pop();
            if (pop.arrOn[number] == 1) {
              pop.arr[number].hide();
              break;
            }
          }
          if ($("#demo-video-cont").length) {
            $("#demo-video-cont").remove();
          }
        }
      },
    });
  }
  // /Popups

  // Popup
  function Popup(
    containerSelector,
    hideBtnSelector,
    showBtnSelector,
    overlaySelector,
    hideOnOverlayClick
  ) {
    var index = _popups.push(this),
      popup = this,
      container = $(containerSelector),
      hideBtn = $(hideBtnSelector),
      showBtn = $(showBtnSelector),
      overlay = $(overlaySelector);
    popup.showExtend = function (coverUp) {
      if (container.length) {
        container.fadeIn(400);
      }
      if (overlay.length) {
        overlay.fadeIn(400);
      }
    };
    popup.hideExtend = function () {
      if (container.length) {
        container.fadeOut(400);
      }
      if (overlay.length) {
        overlay.fadeOut(400);
      }
      //$(document).unbind('.popup');
    };
    popup.show = function (coverUp) {
      popup.showExtend(coverUp);
      if (hideOnOverlayClick == undefined || hideOnOverlayClick != false) {
        overlay.bind({
          "click.popup": function () {
            popup.hide();
          },
        });
      }
      _popups.on(index);
      container.triggerHandler("show");
    };
    popup.hide = function () {
      popup.hideExtend();
      _popups.off(index);
      container.triggerHandler("hide");
    };
    showBtn.click(function () {
      if (_popups.arrOn[index]) {
        popup.hide();
      } else {
        popup.show();
      }
    });
    hideBtn.click(popup.hide);
  }
  // /Popup

  // LoginForm
  function LoginForm(container) {
    var _this = this;

    this.$container = $(container);
    this.$panel = $("#j-panel");
    this.$showLoginFormBtn = $("#j-show-login-form");
    this.$loginForm = $("#j-login-form");
    this.$error = $("#j-login-form-error");
    this.$again = $("#j-login-again");
    this.$submit = $("#j-login-submit");
    //     this.$exit = $('#j-exit');
    this.$login = $("#j-login-val");
    this.$password = $("#j-password-val");

    this.init();
  }
  LoginForm.prototype.init = function () {
    var _this = this;
    this.$again.click(function () {
      _this.showLoginForm();
      return false;
    });
    this.$submit.click(function (e) {
      //e.preventDefault();
      _this.checkData();
      return false;
    });
    this.$showLoginFormBtn.click(function () {
      _this.showLoginForm();
    });
    function toggleLoginForm() {
      if (_wrapper.width() <= 1300 && _this.$showLoginFormBtn.is(":hidden")) {
        if (android == true) {
          _this.showLoginForm();
        } else _this.showLoginFormBtn();
      } else if (_wrapper.width() > 1300 && _this.$loginForm.is(":hidden")) {
        _this.showLoginForm();
      }
    }
    toggleLoginForm();
    $(window).resize(toggleLoginForm);
  };
  LoginForm.prototype.checkData = function () {
    var _this = this,
      login = $.trim(_this.$login.val()),
      password = $.trim(_this.$password.val()),
      submit = this.$submit;
    //console.log('else', 'else');return;
    if (login != "" && password != "") {
      $.ajax({
        type: "post",
        url: "login.html",
        data: { login: login, password: password },
        cache: false,
        beforeSend: function () {
          submit.prop("disabled", true);
        },
        success: function (r) {
          if (r == 1) {
            window.location.reload();
          } else {
            _this.showError(r);
            submit.prop("disabled", false);
          }
        },
        error: function (e) {
          console.log(e);
          alert("Произошла ошибка на сервере. Приносим свои извинения");
        },
      });
    }
  };

  LoginForm.prototype.showError = function (txt) {
    this.$loginForm.hide();
    if (txt !== undefined && txt != "") this.$error.find("span").text(txt);
    this.$error.show();
  };
  LoginForm.prototype.panelResize = function () {
    this.$panel.triggerHandler("panel.resize");
    this.$panel.triggerHandler("panel.scrollToRight");
  };
  LoginForm.prototype.showLoginForm = function () {
    this.$error.hide();
    this.$loginForm.show();
    this.$showLoginFormBtn.hide();

    this.panelResize();
  };
  LoginForm.prototype.showLoginFormBtn = function () {
    var _this = this;
    this.$error.hide();
    this.$loginForm.hide();
    this.$showLoginFormBtn.show();

    this.panelResize();
  };
  // /LoginForm

  // RecoveryForm
  function RecoveryForm(container) {
    this.recovery = "#j-recovery";
    this.recoveryLink = "#j-recovery-link";
    this.again = ".popup-form-again";
    this.close = ".popup-form-close";

    this.$container = $(container);
    this.$recovery = $(this.recovery);
    this.$recoveryLink = $(this.recoveryLink);
    this.$submit = $("#j-recovery-submit");
    this.$recoveryForm = $("#j-recovery-form");
    this.$text = this.$recoveryForm.find(".text");
    this.$again = $("#j-login-again");

    this.recoveryPopup;
    this.validator;

    this.init();
  }
  RecoveryForm.prototype.init = function () {
    var _this = this;
    this.recoveryPopup = new Popup(
      _this.recovery,
      _this.close,
      _this.recoveryLink,
      "#j-overlay",
      false
    );
    this.$recovery.bind({
      hide: function () {
        _this.$again.triggerHandler("click");
      },
    });
    this.$recovery.bind({
      show: function () {
        _this.resetRecoveryForm();
        _this.showRecoveryForm();
      },
    });
    this.$container.click(function (e) {
      if ($(e.target).is(_this.again)) {
        _this.showRecoveryForm();
      }
    });
    if (this.$recoveryForm.length) {
      this.validator = this.$recoveryForm.validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
          email: {
            required: true,
            email: true,
          },
          antibot: {
            required: true,
          },
        },
        messages: {
          email: {
            required: "Пожалуйста, введите адрес электронной почты",
            email: "Пожалуйста, введите корректный адрес электронной почты",
          },
          antibot: {
            required: "Пожалуйста, введите символы с картинки",
          },
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
          if (_this.$recoveryForm.is(":visible")) {
            _this.showError(error);
          }
        },
        highlight: function (element, errorClass) {},
        unhighlight: function (element, errorClass) {},
        submitHandler: function (form) {
          _this.checkData();
          //form.submit();
        },
      });
    }
  };
  RecoveryForm.prototype.checkData = function () {
    var _this = this,
      antibotError = false,
      emailError = false,
      email = "xxxxxx@xxx.xx";
    // some code…
    if (antibotError) {
      _this.showError($("<div>Введенные символы не соответствуют символам на картинке.</div>"));
    } else if (emailError) {
      _this.showError($("<div>Электронный адрес не зарегистрирован в системе.</div>"));
    } else {
      _this.showSuccess(
        $("<div>Логин и пароль отправлены на электронный адрес " + email + "</div>")
      );
    }
  };
  RecoveryForm.prototype.resetRecoveryForm = function () {
    this.$text.val("");
  };
  RecoveryForm.prototype.showRecoveryForm = function () {
    this.$container.find(".popup-form-error, .popup-form-success").remove();
    this.$recoveryForm.show();
  };
  RecoveryForm.prototype.showError = function (msg) {
    this.$recoveryForm.hide();
    msg
      .addClass("popup-form-error")
      .prepend('<span class="popup-form-again">Попробовать снова</span>');
    this.$container.prepend(msg);
  };
  RecoveryForm.prototype.showSuccess = function (msg) {
    this.$recoveryForm.hide();
    this.$container.prepend(msg.addClass("popup-form-success"));
  };
  // /RecoveryForm

  // FeedbackForm
  function FeedbackForm(container) {
    (this.$feedbackForm = $(container)), (this.$msg = $("#j-feedback-msg"));
    this.$successMsg = $("#j-feedback-success");
    this.$text = this.$feedbackForm.find(".text, textarea");
    this.$textarea = this.$text.filter("textarea");
    this.$scroller = this.$feedbackForm.closest(".text-scroller");

    this.textareaBaron,
      (this.textareaScroll = ".textarea-scroll"),
      (this.textareaScroller = ".scroller"),
      (this.track = ".textarea-track"),
      (this.bar = ".textarea-bar"),
      (this.barOnCls = "baron"),
      (this.textareaMaxHeight = 297);
    this.textareaMinHeight = 50;

    this.validator;
    this.email = "#" + this.$feedbackForm.attr("id") + " input[name=email]";
    this.$email = $(this.email);
    this.emailTimer;

    this.init();
  }
  FeedbackForm.prototype.init = function () {
    var _this = this;
    this.$text.val("").focus(function () {
      if ($(this).attr("name") == "antibot" && $(this).val() == "Ошибка") {
        $(this).val("").removeClass("error");
      } else if ($(this).val() == "Обязательное поле для заполнения") {
        $(this).val("");
      }
    });
    this.$scroller.bind({
      show: function () {
        _this.setTextareaHeight();
      },
    });
    this.textareaBaron = baron({
      root: _this.textareaScroll,
      scroller: _this.textareaScroller,
      track: _this.track,
      bar: _this.bar,
      barOnCls: _this.barOnCls,
    });
    if (this.$feedbackForm.length) {
      jQuery.validator.addMethod(
        "placeholder",
        function (value, element) {
          return (
            !(value == "Ошибка") &&
            !(value == $(element).attr("placeholder")) &&
            !(value == "Обязательное поле для заполнения")
          );
        },
        ""
      );
      _this.validator = this.$feedbackForm.validate({
        ignore: ".ignore",
        //onfocusout: false,
        rules: {
          name: {
            required: true,
            placeholder: true,
          },
          email: {
            required: true,
            placeholder: true,
            email: true,
          },
          msg: {
            required: true,
            placeholder: true,
          },
          hiddenRecaptcha: {
            required: function () {
              //noinspection JSUnresolvedVariable,JSUnresolvedFunction
              return grecaptcha.getResponse() == "";
            },
            placeholder: true,
          },
        },
        messages: {
          name: {
            required: "Обязательное поле для заполнения",
          },
          email: {
            required: "Обязательное поле для заполнения",
            email: "Пожалуйста, введите корректный адрес электронной почты",
          },
          msg: {
            required: "Обязательное поле для заполнения",
          },
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
          if (element.val() == "" && !element.is(":focus")) {
            element.val(error.text());
          }
        },
        highlight: function (element, errorClass) {
          if ($(element).is("textarea")) {
            $(element).closest(".textarea").addClass("error");
          } else {
            $(element).addClass("error");
          }
          _this.$msg.addClass("error-msg");
        },
        unhighlight: function (element, errorClass) {
          if ($(element).is("textarea")) {
            $(element).closest(".textarea").removeClass("error");
          } else {
            $(element).removeClass("error");
          }
          if (_this.validator.numberOfInvalids() == 0) {
            _this.$msg.removeClass("error-msg");
          }
        },
        invalidHandler: function (form) {
          _this.bindValidatorTimer();
        },
        submitHandler: function (form) {
          $.ajax({
            type: "post",
            url: "/contact.html",
            data: {
              name: _this.$text.val(),
              email: _this.$email.val(),
              text: _this.$textarea.val(),
              verifyCode: grecaptcha.getResponse(),
            },
            cache: false,
            beforeSend: function () {
              $("#j-feedback-form-submit").prop("disabled", true);
            },
            success: function (r) {
              if (r == 1) {
                grecaptcha.reset();
                _this.checkData();
              } else {
                $("#j-feedback-form-submit").prop("disabled", false);
              }
            },
            error: function () {
              alert("Произошла ошибка на сервере. Приносим свои извинения");
            },
          });

          return false;
        },
      });
    }
  };
  FeedbackForm.prototype.bindValidatorTimer = function () {
    var _this = this;
    this.$email.bind({
      "focus.feedbackFormEmail": function () {
        clearTimeout(_this.emailTimer);
        _this.emailTimer = setInterval(function () {
          _this.validator.element(_this.email);
        }, 500);
      },
      "focusout.feedbackFormEmail": function () {
        clearTimeout(_this.emailTimer);
      },
    });
  };
  FeedbackForm.prototype.unbindValidatorTimer = function () {
    this.$email.unbind(".feedbackFormEmail");
  };
  FeedbackForm.prototype.resetFeedbackForm = function () {
    this.$text.val("");
    this.textareaBaron.update();
    this.unbindValidatorTimer();
  };
  FeedbackForm.prototype.checkData = function () {
    var _this = this;
    this.$msg.hide();
    this.$successMsg.show();
    this.resetFeedbackForm();
    setTimeout(function () {
      _this.$msg.show();
      _this.$successMsg.hide();
    }, 5000);
  };
  FeedbackForm.prototype.setTextareaHeight = function () {
    this.$textarea.height(this.textareaMaxHeight);
    var inc = this.$scroller.get(0).scrollHeight - this.$scroller.get(0).clientHeight,
      tH;
    if (inc != 0) {
      tH = this.textareaMaxHeight - inc;
      if (tH < this.textareaMinHeight) {
        tH = this.textareaMinHeight;
      }
      this.$textarea.height(tH);
      this.textareaBaron.update();
    }
  };
  // /FeedbackForm

  // SpaceText
  function SpaceText(container) {
    (this.$container = $(container)),
      (this.$panel = $("#j-panel")),
      (this.$back = this.$panel.find("#j-back").hide()),
      (this.$a = this.$panel.find(".panel-menu a")),
      (this.$login = $("#j-login")),
      (this.$items = $([])),
      this.$curItem,
      this.textBaron,
      (this.textScroll = ".text-scroll"),
      (this.textScroller = ".text-scroller"),
      (this.$textScroller = $(this.textScroller));
    (this.track = ".track"),
      (this.bar = ".bar"),
      (this.barOnCls = "baron"),
      (this.duration = 400),
      this.wrapperHeight,
      this.panelHeight,
      this.panelTop,
      this.panelBottom,
      this.scrollerHeight;

    this.init();
  }
  SpaceText.prototype.init = function () {
    var _this = this;
    this.setVars();
    this.$container.hide().css({ top: "-100%" });
    // space text page custom scroll
    this.textBaron = baron({
      root: _this.textScroll,
      scroller: _this.textScroller,
      track: _this.track,
      bar: _this.bar,
      barOnCls: _this.barOnCls,
    });
    this.$a
      .each(function () {
        _this.$items = _this.$items.add($(this).attr("href"));
      })
      .click(function () {
        if (parseInt($(this).attr("rel")) > 0) {
          return;
        }
        if (_this.$container.is(":hidden")) {
          _this.$back.show();
          _this.$login.hide();
          _this.panelResize();
          _this.$container.show().animate(
            { top: 0 },
            {
              queue: false,
              duration: _this.duration,
              easing: "easeOutCubic",
              complete: function () {
                _this.textBaron.update();
              },
            }
          );
          _this.$panel.animate(
            { top: _this.panelBottom },
            { queue: false, duration: _this.duration, easing: "easeOutCubic" }
          );
        }
        _this.$items.hide();
        $($(this).attr("href")).show();
        _this.$textScroller.height(_this.scrollerHeight);
        _this.$textScroller.triggerHandler("show");
        _this.textBaron.update();
        return false;
      });
    this.$back.click(function () {
      _this.$back.hide();
      _this.$login.show();
      _this.panelResize();
      _this.$container.animate(
        { top: "-100%" },
        {
          queue: false,
          duration: _this.duration,
          easing: "easeOutCubic",
          complete: function () {
            _this.$container.hide();
          },
        }
      );
      _this.$panel.animate(
        { top: 0 },
        { queue: false, duration: _this.duration, easing: "easeOutCubic" }
      );
      return false;
    });
    $(window).resize(function () {
      _this.setVars();
      if (_this.panelTop > 0) {
        _this.$panel.css({ top: _this.panelbottom });
      }
      _this.$textScroller.height(_this.scrollerHeight);
      _this.$textScroller.triggerHandler("show");
      _this.textBaron.update();
    });
  };
  SpaceText.prototype.setVars = function () {
    this.wrapperHeight = _wrapper.height();
    this.panelHeight = this.$panel.height();
    this.panelTop = this.$panel.position().top;
    this.panelBottom = _wrapper.height() - this.panelHeight;
    this.scrollerHeight = this.wrapperHeight - 524;
  };
  SpaceText.prototype.panelResize = function () {
    this.$panel.triggerHandler("panel.resize");
  };
  // /SpaceText

  // Space
  function Space(container) {
    this.$container = $(container);
    this.$logoBtn = $("#demo-start");
    this.$spaceAreas = this.$container.find(".space-area");
    this.$panel = $("#j-panel");
    this.tiles = "#j-space-tiles";
    this.zoom = 0;
    this.newZoom = 0;

    this.init();
  }
  Space.prototype.init = function () {
    var _this = this,
      zoom;
    this.$logoBtn.click(function () {
      _this.$container.toggleClass("noactive");
      if (_this.$container.hasClass("noactive")) {
        _this.$spaceAreas
          .stop(true)
          .animate({ opacity: "hide" }, { duration: 800, queue: false, easing: "easeOutCubic" });
        _this.$logoBtn.text("Демо-режим");
        _this.$panel
          .stop(true)
          .animate({ top: 0 }, { duration: 500, queue: false, easing: "easeOutCubic" });
      } else {
        _this.$spaceAreas
          .css({ display: "block", opacity: 0 })
          .stop(true)
          .animate({ opacity: 1 }, { duration: 500, queue: false, easing: "easeincubic" });
        _this.$logoBtn.text("Вернуться");
        _this.$panel
          .stop(true)
          .animate({ top: -60 }, { duration: 500, queue: false, easing: "easeOutCubic" });
      }
    });
    new Tiles(_this.tiles, {
      imgFolder: "images/space-tiles/",
      prefixImg: "space_",
      maxWidth: 2731,
      maxHeight: 1536,
    });
    this.setZoom();
    $(window).resize(function () {
      _this.setZoom();
    });
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "/enter.html");
    $(".space-area").click(function () {
      var clases = $(this).attr("rel");
      var ui = $(this).data("ui");
      var isShowDemo = $(this).data("isshowdemo");
      if (isShowDemo != "") {
        return false;
      }
      if (ui == 0) {
        $("#j-modal-cont").show();
        return false;
      }
      var userId = document.createElement("input");
      userId.setAttribute("type", "hidden");
      userId.setAttribute("name", "ui");
      userId.setAttribute("value", ui);
      form.appendChild(userId);
      var schoolClass = document.createElement("input");
      schoolClass.setAttribute("type", "hidden");
      schoolClass.setAttribute("name", "schoolClass");
      schoolClass.setAttribute("value", clases);
      form.appendChild(schoolClass);
      document.body.appendChild(form);
      form.submit();
    });
    $("#j-modal-cont-but").click(function () {
      $("#j-modal-cont").hide();
    });
  };
  Space.prototype.setZoom = function () {
    wrapperHeight = _wrapper.height();
    if (wrapperHeight <= 700) {
      this.newZoom = 0;
    } else if (wrapperHeight <= 950) {
      this.newZoom = 1;
    } else if (wrapperHeight <= 1150) {
      this.newZoom = 2;
    } else if (wrapperHeight <= 1380) {
      this.newZoom = 3;
    } else {
      this.newZoom = 4;
    }
    this.$container.removeClass("zoom" + this.zoom).addClass("zoom" + this.newZoom);
    this.zoom = this.newZoom;
  };
  // /Space
})(jQuery);
