import { ReactLocation, Router } from '@tanstack/react-location';

const location = new ReactLocation();

export const LocationWrapper: React.FC = ({ children }) => {
  return (
    <Router location={location} routes={[]}>
      {children}
    </Router>
  );
};
