import { useQuery } from 'react-query';
import { getCurrentUser } from '../../../api/common';
import { key } from '.';

export const useCurrentUser = () => useQuery(key, getCurrentUser);
