import { QueryObserverOptions } from 'react-query';

/** to show typings remove any **/
export const BASE_CACHE_OPTIONS: QueryObserverOptions | any = {
  refetchOnWindowFocus: false,
  staleTime: 300000,
  refetchOnMount: false,
};
