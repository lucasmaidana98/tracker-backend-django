import { useMutation } from 'react-query';
import { updateUserCourseSettings } from '../../../api/common';
import { queryClient } from '../../../hooks/queryClient';
import { IMutateNotification, ISettingData } from '../../../typings/common';
import { getKey } from '.';

export const useUpdateUserCourseSetting = (onMutate: (data: IMutateNotification) => void) => {
  return useMutation<ISettingData, unknown, ISettingData>(
    (model) => updateUserCourseSettings(model),
    {
      onSuccess: ({ user, course }) => {
        queryClient.refetchQueries(getKey({ user, course }));
        onMutate({ appearance: 'success', type: 'update' });
      },
      onError: () => onMutate({ appearance: 'error', type: 'update' }),
    },
  );
};
