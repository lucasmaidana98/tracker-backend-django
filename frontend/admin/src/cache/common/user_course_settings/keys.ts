import { ISettingParams } from '../../../typings/common';

export const getKey = ({ user, course }: ISettingParams) => ['settings', user, course];
