import { useQuery } from 'react-query';
import { API } from '../../../api';
import { adminsKey } from './keys';

export const useAdmins = () =>
  useQuery(adminsKey, () => API.admin.user.all({ user_status: 0 }), {
    staleTime: 9999999,
  });
