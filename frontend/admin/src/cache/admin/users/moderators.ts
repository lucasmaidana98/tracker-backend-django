import { AxiosError } from 'axios';
import { useMutation, useQuery } from 'react-query';
import { API } from '../../../api';
import { queryClient } from '../../../hooks/queryClient';
import { IUser, IUsersRes, TCreateUser, TUpdateUser } from '../../../typings/admin';
import { getError } from '../../../utils/api';
import { moderatorsKey } from './keys';

export const useModerators = () =>
  useQuery(moderatorsKey, () => API.admin.user.all({ user_status: 1 }), {
    staleTime: 9999999,
  });

export const useRemoveModerator = (onSuccess: () => void, onError: () => void) =>
  useMutation<unknown, unknown, number>((user) => API.admin.user.remove(user), {
    onSuccess: (_, user) => {
      queryClient.setQueryData(moderatorsKey, () => {
        const users = queryClient.getQueryData<IUsersRes>(moderatorsKey);
        if (users) {
          return { ...users, results: users.results?.filter((u) => u.id !== user) };
        }
      });
      onSuccess();
    },
    onError,
  });

export const useAddModerator = (onSuccess: () => void, onError: (e: string) => void) =>
  useMutation<{ message: string }, unknown, TCreateUser>(
    (data) => API.admin.user.create({ ...data, user_status: 1 }),
    {
      onSuccess: (_, user) => {
        onSuccess();
        queryClient.refetchQueries(moderatorsKey);
        queryClient.setQueryData(moderatorsKey, () => {
          const oldData = queryClient.getQueryData<IUsersRes>(moderatorsKey);
          if (oldData) {
            return { ...oldData, results: [...oldData.results, user] };
          }
        });
      },
      onError: (e) => onError(getError(e as AxiosError)),
    },
  );

export const useEditModerator = (onSuccess: () => void, onError: (e: string) => void) =>
  useMutation<IUser, unknown, TUpdateUser>(
    (data) => {
      if (data.password)
        API.admin.user.updatePassword({ password: data.password, user_id: data.id });
      return API.admin.user.update(data);
    },
    {
      onSuccess: () => {
        onSuccess();
        queryClient.refetchQueries(moderatorsKey);
      },
      onError: (e) => onError(getError(e as AxiosError)),
    },
  );
