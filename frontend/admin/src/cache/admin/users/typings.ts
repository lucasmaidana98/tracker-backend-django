import { IUser, IUsersRes } from '../../../typings/admin';
import { Dictionary } from 'lodash';

export interface AdminUsersStore {
  data: IUsersRes;
  usersById: Dictionary<IUser>;
}
