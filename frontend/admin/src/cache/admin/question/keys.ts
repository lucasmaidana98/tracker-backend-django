export const getQuestionsKey = (testId: string) => ['questions', `test-${testId}`];
