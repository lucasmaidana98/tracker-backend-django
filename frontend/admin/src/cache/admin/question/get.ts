import { useQuery } from 'react-query';
import { API } from '../../../api';
import { getQuestionsKey } from '.';

export const useQuestionsByTest = (testId: string) => {
  return useQuery(getQuestionsKey(testId), () =>
    API.admin.question.all({ tasks: testId, ordering: 'id' }),
  );
};
