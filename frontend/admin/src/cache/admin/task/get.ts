import { useQuery } from 'react-query';
import { API } from '../../../api';
import { getKey } from '.';

export const useTestsByCourseId = (courseId: string) => {
  return useQuery(getKey(courseId), () => API.admin.test.all({ material__course: courseId }), {
    enabled: !!courseId,
  });
};
