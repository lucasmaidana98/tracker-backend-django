import { useQuery } from 'react-query';
import { API } from '../../../api';
import { ITagParams } from '../../../typings/admin';
import { key } from '.';

export const useGetTags = (params?: ITagParams) => useQuery(key, () => API.admin.tag.all(params));
