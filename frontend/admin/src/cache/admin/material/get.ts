import { useQuery } from 'react-query';
import { API } from '../../../api';
import { getSearchKey } from '.';

export const useMaterialsSearch = (search: string) => {
  return useQuery(getSearchKey(search), () => API.admin.material.all({ search }), {
    enabled: !!search,
  });
};
