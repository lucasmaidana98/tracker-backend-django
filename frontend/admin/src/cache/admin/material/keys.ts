import { IMaterialsParams } from '../../../typings/admin';

export const getMaterialsKey = (params: IMaterialsParams) => ['materials', params];
export const getSearchKey = (search: string) => ['courses-search', search];
