export const key = 'courses';
export const getKeyCourse = (courseId?: string | number) =>
  courseId ? ['course', courseId] : 'course';
