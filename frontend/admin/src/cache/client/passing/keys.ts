const byCourse = 'test-passings-by-course';

export const getTestBassingsByCourseKey = (course?: number) =>
  course ? [byCourse, course] : byCourse;
