import { useWebSocket } from 'react-use-websocket/dist/lib/use-websocket';

const Ws: React.FC = () => {
  const { sendJsonMessage } = useWebSocket('ws://localhost:8000', {
    shouldReconnect: () => true,
    onMessage: ({ data }) => {
      console.log('message', data);
    },
  });

  return (
    <>
      <button onClick={() => sendJsonMessage('message text')}>send</button>
    </>
  );
};

export default Ws;
