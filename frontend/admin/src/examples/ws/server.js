const WS = require('ws').Server;
const http = require('http');
const express = require('express');

const app = express();

const server = http.createServer(app);
const port = 8000;

const WebSocketServer = new WS({ server: server });

WebSocketServer.on('connection', function (ws) {
  ws.send('hello');
  ws.on('message', (message) => ws.send(`you send a message ${message}`));
});

server.listen(port, () => console.log(`server running on ${port}`));
