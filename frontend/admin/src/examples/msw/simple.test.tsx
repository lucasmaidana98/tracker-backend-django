import { renderHook } from '@testing-library/react-hooks';
import axios from 'axios';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';

type TData = {
  completed: boolean;
  id: number;
  title: string;
  userId: number;
};

function useCustomHook() {
  return useQuery('customHook', async () => {
    try {
      const { data } = await axios.get<TData>('https://jsonplaceholder.typicode.com/todos/1');
      return data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  });
}

test('simple hooks', async () => {
  const queryClient = new QueryClient();

  const wrapper: React.FC = ({ children }) => (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );

  const { result, waitFor } = renderHook(() => useCustomHook(), { wrapper });

  await waitFor(() => result.current.isSuccess);

  expect(result.current.data).toEqual({
    completed: false,
    id: 1,
    title: 'delectus aut autem',
    userId: 1,
  });
});
