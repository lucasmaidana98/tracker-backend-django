import { Link, Outlet, ReactLocation, Router } from '@tanstack/react-location';
import { getActiveProps } from './Post';
import Home from './Home';
import Posts from './Posts';
import { fetchPosts } from './api';
import { Post, PostsIndex } from './Post';
import { fetchPostById } from './api';

const location = new ReactLocation();

function LocationApp() {
  return (
    <Router
      location={location}
      routes={[
        { path: '/', element: <Home /> },
        {
          path: 'posts',
          element: <Posts />,
          loader: async () => {
            return {
              posts: await fetchPosts(),
            };
          },
          children: [
            { path: '/', element: <PostsIndex /> },
            {
              path: ':postId',
              element: <Post />,
              loader: async ({ params: { postId } }) => {
                return {
                  post: await fetchPostById(postId),
                };
              },
            },
          ],
        },
      ]}
    >
      <div>
        <Link to="/" getActiveProps={getActiveProps} activeOptions={{ exact: true }}>
          Home
        </Link>{' '}
        <Link to="posts" getActiveProps={getActiveProps}>
          Posts
        </Link>
      </div>
      <hr />
      <Outlet />
    </Router>
  );
}

export default LocationApp;
