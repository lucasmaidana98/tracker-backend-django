import { LocationGenerics } from './location-types';
import { useMatch } from '@tanstack/react-location';

export function getActiveProps() {
  return {
    style: {
      fontWeight: 'bold',
    },
  };
}

export function PostsIndex() {
  return (
    <>
      <div>Select an post.</div>
    </>
  );
}

export function Post() {
  const {
    data: { post },
  } = useMatch<LocationGenerics>();

  return (
    <div>
      <h4>{post?.title}</h4>
      <p>{post?.body}</p>
    </div>
  );
}
