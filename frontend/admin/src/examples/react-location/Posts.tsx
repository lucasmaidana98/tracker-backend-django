import { Link, Outlet, useMatch } from '@tanstack/react-location';
import { LocationGenerics } from './location-types';
import { getActiveProps } from './Post';

function Posts() {
  const {
    data: { posts },
  } = useMatch<LocationGenerics>();

  return (
    <div>
      <div>
        {posts?.map((post) => {
          return (
            <div key={post.id}>
              <Link to={post.id} getActiveProps={getActiveProps}>
                <pre>{post.title}</pre>
              </Link>
            </div>
          );
        })}
      </div>
      <hr />
      <Outlet />
    </div>
  );
}

export default Posts;
