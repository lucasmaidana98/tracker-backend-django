/** Шаблон с футером, приклееным к низу страницы */
const Template: React.FC = () => {
  return (
    <>
      <div className="min-h-[100vh]">
        <header>header</header>
        <main className="pb-[100px]">main</main>
      </div>
      <footer className="mt-[-100px] h-[100px]">footer</footer>
    </>
  );
};

export default Template;
