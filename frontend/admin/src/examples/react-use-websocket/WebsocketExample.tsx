import { useState, useEffect } from 'react';
import useWebSocket from 'react-use-websocket';

const SOCKET_URL_ONE = 'wss://echo.websocket.events';
const READY_STATE_OPEN = 1;
const readyStateString = ['CONNECTING', 'OPEN', 'CLOSING', 'CLOSED'];

export const WebSocketExample = () => {
  const [currentSocketUrl, setCurrentSocketUrl] = useState(null);
  const [messageHistory, setMessageHistory] = useState([]);
  const [inputtedMessage, setInputtedMessage] = useState('');
  const { sendMessage, lastMessage, readyState } = useWebSocket(currentSocketUrl, {
    share: true,
    shouldReconnect: () => false,
  });

  useEffect(() => {
    // @ts-ignore
    setCurrentSocketUrl(SOCKET_URL_ONE);
  }, []);

  useEffect(() => {
    lastMessage && setMessageHistory((prev) => prev.concat(lastMessage.data));
  }, [lastMessage]);

  const submit = (e: any) => {
    e.preventDefault();
    sendMessage(inputtedMessage);
    setInputtedMessage('');
  };

  return (
    <div>
      Whatever you send will be echoed from the Server
      <form onSubmit={submit}>
        <input
          type={'text'}
          value={inputtedMessage}
          onChange={(e) => setInputtedMessage(e.target.value)}
        />
        <button disabled={readyState !== READY_STATE_OPEN}>Send</button>
      </form>
      ReadyState: {readyStateString[readyState]}
      <br />
      MessageHistory: {messageHistory.join(', ')}
    </div>
  );
};
