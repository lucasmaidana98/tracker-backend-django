const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = (app) => {
  app.use(
    '/api/v1/',
    createProxyMiddleware({
      target: 'https://saynschuman.pp.ua/',
      ws: false,
      changeOrigin: true,
    }),
  );
  app.use(
    '/media/material/',
    createProxyMiddleware({
      target: 'https://saynschuman.pp.ua/',
      secure: false,
      changeOrigin: true,
    }),
  );
};
