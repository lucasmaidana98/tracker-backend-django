import type { RefObject } from 'react';
import { useEffect, useState } from 'react';

export function useOnScreen<T extends Element>(
  ref: RefObject<T>,
  rootMargin = '0px',
  nameIdRootElement?: string,
) {
  const [isIntersecting, setIntersecting] = useState(false);

  useEffect(() => {
    const element = ref.current;
    const root = nameIdRootElement ? document.getElementById(nameIdRootElement) : null;

    const observer = new IntersectionObserver(
      ([entry]) => {
        setIntersecting(entry.isIntersecting);
      },
      {
        rootMargin,
        root,
      },
    );

    if (element) {
      observer.observe(element);
    }

    return () => {
      if (element) {
        observer.unobserve(element);
      }
    };
  }, [nameIdRootElement, ref, rootMargin]);

  return isIntersecting;
}
