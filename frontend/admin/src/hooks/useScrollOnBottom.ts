import { useEffect } from 'react';

/**
 * Хук проверяет проскроленность элемента до самого низу и выполняет передаваемый колбек
 * @param ref элемент
 * @param callback колбек
 */
export const useScrollOnBottom = <T extends HTMLElement>(
  ref: React.RefObject<T>,
  callback: (evt: Event) => void,
) => {
  const element = ref.current;

  useEffect(() => {
    const listener = (evt: Event) => {
      if (element) {
        if (element.scrollHeight - element.scrollTop < element.clientHeight + 1) {
          callback(evt);
        }
      }
    };

    element?.addEventListener('scroll', listener);
    return () => element?.removeEventListener('scroll', listener);
  }, [callback, element]);
};
