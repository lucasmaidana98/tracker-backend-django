const ClientTaskTemplate: React.FC = ({ children }) => {
  return (
    <div className="p-5 md:p-12 text-[17px] transition-all duration-300 h-auto">{children}</div>
  );
};

export default ClientTaskTemplate;
