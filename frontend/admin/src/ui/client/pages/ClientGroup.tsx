import ClientTemplate from '../templates/ClientTemplate';

const ClientGroup: React.FC = () => {
  return <ClientTemplate>ClientGroup</ClientTemplate>;
};

export default ClientGroup;
