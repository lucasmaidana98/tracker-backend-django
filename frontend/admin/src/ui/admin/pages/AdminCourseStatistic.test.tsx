import { render, screen } from '@testing-library/react';
import { LocationWrapper } from '../../../mocks/components/Location';
import { UserLink } from './AdminCourseStatistic';

describe('тестирование компонентов страницы AdminCourseStatistic', () => {
  it('компонент UserLink должен рендерить ссылку на профиль пользователя', () => {
    render(<UserLink id={1}>user-1</UserLink>, { wrapper: LocationWrapper });
    const link = screen.getByTestId('user-link');
    expect(link.getAttribute('href')).toBe('/admin/users/1/');
  });
});
