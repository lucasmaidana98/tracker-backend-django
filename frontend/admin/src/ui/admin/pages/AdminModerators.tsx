import { useState } from 'react';
import { FaUserTimes } from 'react-icons/fa';
import ConfirmModal from '../../../shared/ConfirmModal';
import Table from '../../../shared/Table';
import { checkIsIntelleka } from '../../../utils/link';
import { russianEndings } from '../../../utils/string';
import { convertTimestampToDate } from '../../../utils/time';
import AdminAddModerator from '../containers/AdminAddModerator';
import AdminEditModerator from '../containers/AdminEditModerator';
import { useCheck } from '../store/useModerators';
import AdminTemplate from '../templates/AdminTemplate';
import { useModerators, useRemoveModerator } from '../../../cache/admin/users';
import { useToasts } from 'react-toast-notifications';

const AdminModerators: React.FC = () => {
  const { addToast } = useToasts();
  const { data, isLoading } = useModerators();

  const [confirmVisible, setConfirmVisible] = useState(false);
  const { checked, setChecked } = useCheck();

  const onSuccess = () => {
    setChecked([]);
    setConfirmVisible(false);
  };

  const onError = () => {
    addToast('Не удалось удалить преподавателя', { appearance: 'error' });
    setConfirmVisible(false);
  };

  const { mutate: deleteUserRequest, isLoading: deleting } = useRemoveModerator(onSuccess, onError);

  const titles = [
    <input
      type="checkbox"
      disabled={!data?.results.length}
      checked={!!data?.results.length ? checked.length === data?.results?.length : false}
      onChange={() => {
        checked.length ? setChecked([]) : data && setChecked(data.results.map((u) => u.id));
      }}
    />,
    'ФИО',
    'Логин',
    'Дата создания',
    'Курсы',
    'Примечание',
    'Редактировать',
  ];

  const moderators = data?.results.map((u) => [
    <input
      type="checkbox"
      checked={checked.includes(u.id)}
      onChange={() => {
        checked.includes(u.id)
          ? setChecked(checked.filter((id) => id !== u.id))
          : setChecked([...checked, u.id]);
      }}
    />,
    u.first_name,
    u.username,
    convertTimestampToDate(u.date_joined, 'DD.MM.YYYY'),
    <>
      {u.courses?.map((c) => (
        <div key={c.id}>{c.title}</div>
      ))}
    </>,
    <>{u.description}</>,
    <AdminEditModerator user={u} />,
  ]);

  const isIntelleka = checkIsIntelleka();

  return (
    <AdminTemplate>
      <div className="mb-1 flex items-center justify-between">
        <AdminAddModerator />
        <FaUserTimes
          className={`cursor-pointer ${!!checked.length ? 'block' : 'hidden'}`}
          onClick={() => setConfirmVisible(true)}
        />
      </div>
      <Table titles={titles} rows={moderators?.reverse()} isLoading={isLoading} />
      <ConfirmModal
        isIntelleka={isIntelleka}
        confirm={() => checked.forEach((u) => deleteUserRequest(u))}
        isOpen={!!confirmVisible}
        hide={() => setConfirmVisible(false)}
        loading={deleting}
      >
        {`Удалить ${checked.length} ${russianEndings(checked.length, [
          'пользователя',
          'пользователей',
          'пользователей',
        ])}?`}
      </ConfirmModal>
    </AdminTemplate>
  );
};

export default AdminModerators;
