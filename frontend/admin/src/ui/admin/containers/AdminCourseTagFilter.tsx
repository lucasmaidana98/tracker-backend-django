import RSelect from '../../../shared/ReactSelect';

export type TOption = {
  label: string;
  value: number;
};

interface TagFilterProps {
  tags?: TOption[];
  value: number;
  onChange(): void;
  isIntelleka: boolean;
}

const TagFilter: React.FC<TagFilterProps> = ({ isIntelleka = false, ...props }) => {
  return (
    <RSelect
      className="select"
      value={props.value}
      onChange={props.onChange}
      options={[]}
      placeholder="Тэги"
      isMulti
      isIntelleka={isIntelleka}
    />
  );
};

export default TagFilter;
