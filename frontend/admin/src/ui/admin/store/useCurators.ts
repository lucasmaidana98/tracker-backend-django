import create from 'zustand';

export const useCheck = create<{ checked: number[]; setChecked: (checked: number[]) => void }>(
  (set) => ({
    checked: [],
    setChecked: (checked) => set({ checked }),
  }),
);
