import { UseFormHandleSubmit } from 'react-hook-form';
import { IAuthParams } from '../../../typings/common';

export interface IAuthProps {
  onLogin: (token: string) => void;
}

export type TInputProps = { closeErrors(): void } & React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

export type TFormProps = IAuthProps & {
  handleSubmit: UseFormHandleSubmit<IAuthParams>;
};

export interface ISubmitButtonProps {
  isIntelleka: boolean;
  loading: boolean;
  isValid: boolean;
  isVisible: boolean;
}

export interface ISubmitProps {
  isValid: boolean;
}
