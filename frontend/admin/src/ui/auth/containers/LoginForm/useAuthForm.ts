import { useForm } from 'react-hook-form';

export const useAuthForm = () => {
  return useForm<{ username: string; password: string }>({ mode: 'onChange' });
};
