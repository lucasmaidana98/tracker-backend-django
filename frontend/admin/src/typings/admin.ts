import { IUserStatus } from './common';
import { Merge } from 'type-fest';

export interface ICoursesParams {
  id: string;
  title: string;
  author: string;
  tag: string;
  created: string;
  ordering: string;
}

export interface ICourse {
  id: number;
  title: string;
  description: string;
  teacher: string;
  author: number;
  author_name: string;
  moderators: number[];
  tag: number[];
  users: number[];
  created: string;
  registered_num: number;
  visit_num: number;
  completed_num: number;
  is_active: boolean;
}

type TRequiredCreateCourse = Pick<ICourseUsersDetail, 'title'>;
type TRequiredUpdateCourse = Pick<ICourseUsersDetail, 'title' | 'id'>;

export type TCreateCourse = Merge<Partial<ICourseUsersDetail>, TRequiredCreateCourse>;
export type TUpdateCourse = Merge<Partial<ICourseUsersDetail>, TRequiredUpdateCourse>;

export interface IUsersDetail {
  id: number;
  username: string;
}

export interface ICourseUsersDetail extends ICourse {
  users_detail: IUsersDetail[];
}

export type ILastTestPassings = {
  id: number;
  task: number;
  is_final_task: boolean;
  success_passed: number;
  response_rate: string;
  start_time: string;
  out_of_time?: boolean;
  finish_time: string;
  travel_time?: number;
  retake_seconds?: number;
}[];

export type IUserCourse = {
  id: number;
  title: string;
  description?: string | null;
  author?: number;
  last_test_passings?: ILastTestPassings;
};

export type IUser = {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  description: string | null;
  design: number;
  webinars: boolean;
  password?: string;
  old_password: string;
  user_status: IUserStatus;
  is_active: boolean;
  company: number;
  company_title: string;
  curator_company?: number[];
  time_limitation: string | null;
  date_joined: string;
  courses?: IUserCourse[];
  start_course: string;
  last_passing_dt: string | null;
  end_course: string;
  teacher_chat: boolean;
  group_chat: boolean;
  tech_chat: boolean;
  on_check: number;
  finish_time_exam?: string;
  company_name?: string;
  course_id?: number;
  last_test_passings: ILastTestPassings;
};
export interface IUsersParams {
  user_status: IUserStatus;
  company: number;
  id: number;
  courses__title: string;
  courses__id: string;
  ordering: string | 'id' | '-id';
  page: number;
  page_size: number;
}

export interface IUsersRes {
  count: number;
  next?: string | null;
  previous?: string | null;
  results: IUser[];
}

export interface IParamsCompanies {
  id: number;
  title: string;
  created: string;
  ordering: string;
}

export interface ICompany {
  id: number;
  title: string;
  created: string;
  active_users: number;
  all_users: number;
}

export type TRequiredCreateCompany = Pick<ICompany, 'title'>;
export type TRequiredUpdateCompany = Pick<ICompany, 'title' | 'id'>;

export type TCreateCompany = Merge<Partial<ICompany>, TRequiredCreateCompany>;
export type TUpdateCompany = Merge<Partial<ICompany>, TRequiredUpdateCompany>;

export interface IMaterialsParams {
  id?: string;
  course?: string;
  title?: string;
  is_download?: string;
  created?: string;
  parent?: string;
  ordering?: string;
  search?: string;
}

export interface IMaterial {
  id: number;
  course: number;
  title: string;
  rank: number;
  text: string;
  video: string | File;
  video_link: string;
  pdf: string | File;
  is_download: boolean;
  parent?: number | null;
  original_link: number;
  original_data?: string;
  created: string;
  updated: string;
  is_active: boolean;
}

export interface IMaterialWithChildren extends IMaterial {
  children?: IMaterialWithChildren[];
}

export type TRequiredCreateMaterial = Pick<IMaterial, 'course' | 'title'>;
export type TRequiredUpdateMaterial = Pick<IMaterial, 'id'>;

export type TCreateMaterial = Merge<Partial<IMaterial>, TRequiredCreateMaterial>;
export type TUpdateMaterial = Merge<Partial<IMaterial>, TRequiredUpdateMaterial>;

export interface IMaterialCopy {
  course_id: number;
  material_id: number;
}

export interface ITestParams {
  id?: number;
  title?: string;
  created?: string;
  is_necessarily?: string | boolean;
  is_final?: string | boolean;
  is_hidden?: string | boolean;
  material?: string;
  material__course?: string;
  is_active?: string | boolean;
  ordering?: string;
}

export interface ITest {
  id: number;
  material: number;
  title: string;
  description: string;
  text_explanation: string;
  prepared_text: string;
  travel_time: string;
  retake_seconds: number;
  passing: number;
  attempts: number;
  is_necessarily: boolean;
  is_chance: boolean;
  is_mix: boolean;
  is_miss: boolean;
  is_final: boolean;
  is_hidden: boolean;
  rank: number;
  trial_attempts: number;
  num_questions: number;
  is_active: boolean;
  trial_percents: number;
}

export interface IQuestionParams {
  id?: number;
  is_free_answer?: string;
  tasks?: string;
  score?: number;
  ordering?: 'id' | 'is_free_answer' | 'tasks' | 'score';
}

export interface IQuestion {
  id: number;
  created: string;
  updated: string;
  text: string;
  m_file: File | string | null;
  is_free_answer: boolean;
  score: number;
  tasks: number[];
  file: FileList;
}

export interface IQuestionFile {
  file?: File | string;
  task: string;
}

export type TPartialQuestion = Partial<IQuestion>;
export type TRequiredCreateQuestion = Pick<IQuestion, 'text' | 'tasks'>;
export type TRequiredUpdateQuestion = Pick<IQuestion, 'id' | 'text' | 'tasks'>;

export type TCreateQuestion = Merge<TPartialQuestion, TRequiredCreateQuestion>;
export type TUpdateQuestion = Merge<TPartialQuestion, TRequiredUpdateQuestion>;

export interface IAnswerParams {
  id?: number;
  question?: number;
  is_true?: boolean;
  rank?: number;
  ordering?: 'id' | 'question' | 'is_true' | 'rank';
}

export interface IAnswer {
  id: number;
  created: string;
  updated: string;
  is_true: boolean;
  text: string;
  rank: number;
  question: number;
}

export type TRequiredCreateAnswer = Pick<IAnswer, 'question' | 'text'>;
export type TRequiredUpdateAnswer = Pick<IAnswer, 'id' | 'question' | 'text'>;

export type TCreateAnswer = Merge<Partial<IAnswer>, TRequiredCreateAnswer>;
export type TUpdateAnswer = Merge<Partial<IAnswer>, TRequiredUpdateAnswer>;

export interface ITestPassingParams {
  id?: number;
  task?: string | number;
  task__material__course?: string;
  user?: string;
  success_passed?: string;
  is_trial?: boolean;
  ordering?: string;
}

/** Интерфейс попытки сдачи теста */
export interface ITestPassing {
  id: number;
  task: number;
  is_final_task: string;
  user: number;
  success_passed: 0 | 1 | 2 | 3 | 4 | 5;
  response_rate: string;
  start_time: string;
  out_of_time: boolean;
  finish_time: string;
  travel_time: string;
  retake_seconds: string;
  is_trial: boolean;
}

export interface IOutOfTimeTestPassing {
  id: number;
  task: number;
  is_final_task: boolean;
  user: number;
  out_of_time: boolean;
  is_trial: boolean;
}

export type TRequiredUpdateTestPassing = Pick<IOutOfTimeTestPassing, 'id'>;
export type TUpdateTestPassing = Merge<Partial<IOutOfTimeTestPassing>, TRequiredUpdateTestPassing>;

export interface IUserAnswer {
  id: number;
  passing: number;
  question: number;
  variants: [number, string, boolean][];
  correct_answer: number[];
  max_points: number;
  answer: number;
  file: string;
  user_points: number;
  get_user_points: number;
  text: string;
  verifier: number;
  answers: number[];
}

export interface IUserAnswersParams {
  id?: number;
  passing?: string;
  question?: string;
  answer?: string;
  ordering?: string;
}
export type TRequiredModeratorUserAnswer = Pick<IUserAnswer, 'id'>;
export type TUpdateModeratorUserAnswer = Merge<Partial<IUserAnswer>, TRequiredModeratorUserAnswer>;

export interface IUserActivityParams {
  id?: number;
  user?: string;
  day?: string;
  course_id?: number;
  ordering?: string;
}

export interface IUserDevice {
  id: number;
  user: number;
  course_id: number;
  payload: {
    user: number;
    os: { name: string; version: string };
    browser: { name: string; version: string; major: string };
    time: number;
    course: string | number;
  };
}

export interface IUserDeviceParams {
  id?: number;
  user?: number;
  course_id?: number;
  ordering?: 'id' | 'user' | 'course_id';
}

export type IStatisticUser = {
  id: number;
  user: number;
  course_id: number;
  day: string;
  seconds: number;
}[];

export interface IMateralPassingParams {
  id?: number;
  user?: string;
  material?: string;
  material__course?: string;
  status?: string;
  ordering?: string;
}

export interface IMaterialPassing {
  id: number;
  created: string;
  updated: string;
  status: 0 | 1 | 2;
  material: number;
  user: number;
}

export type TRequiredCreateUser = Pick<IUser, 'username' | 'password'>;
export type TRequiredUpdateUser = Pick<IUser, 'id'>;

export type TCreateUser = Merge<Partial<IUser>, TRequiredCreateUser>;
export type TUpdateUser = Merge<Partial<IUser>, TRequiredUpdateUser>;

export interface IMassUsers {
  text_users?: string;
  file_users?: File | string;
  course_id: number;
  design: number;
}

export interface ICreatedMassUsers {
  code: number;
  data: string;
  message: string;
  status: string;
}

export interface ITagParams {
  tag?: string;
  created?: string;
  id?: number;
  ordering?: string;
}

export interface ITag {
  id: number;
  tag: string;
  created: string;
}

export interface IAuthPassword {
  user_id: number;
  password: string;
}

export interface IUserUpdateCourse {
  user_id: number;
  course_id: number;
  action: 'add' | 'del';
}

export type TRequiredCreateTask = Pick<ITest, 'material' | 'title' | 'travel_time'>;
export type TRequiredUpdateTask = Pick<ITest, 'id'>;

export type TCreateTask = Merge<Partial<ITest>, TRequiredCreateTask>;
export type TUpdateTask = Merge<Partial<ITest>, TRequiredUpdateTask>;

export interface IOnlineUsersResponse {
  online_users: 0;
}

export interface IOnlineHistoryParams {
  min_date: string;
  max_date: string;
}

export interface IOnlineHistory {
  date: string;
  active_clients_count: number;
}

export type IOnlineHistoryResponse = IOnlineHistory[];

export interface IMassMessage {
  users: number[];
  message: string;
}

export type IResultMassMassage = {
  chat_id: number;
  message_id: number;
  user_id: number;
}[];
