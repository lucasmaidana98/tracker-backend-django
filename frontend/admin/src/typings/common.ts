/** параметры для авторизации */
export interface IAuthParams {
  username?: string;
  password?: string;
  [x: string]: any;
}

/** ответ авторизации */
export interface IAuthResponce {
  access: string;
  refresh: string;
}

/** статус пользователя */
export type IUserStatus = 0 | 1 | 2 | 3 | number;

/** текущий пользователь */
export interface ICurrentUser {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  design: number;
  old_password: string;
  user_status: IUserStatus;
  is_active: boolean;
  teacher_chat: boolean;
  group_chat: boolean;
  tech_chat: boolean;
}

/**
 * Статусы чата:
 * 0 - поддержка
 * 1 - чат с преподавателем
 * 2 - чат с группой
 */
export type IChatStatus = 0 | 1 | 2;

/** пользователь */
export interface IUser {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  user_status: IUserStatus;
  time_limitation: string;
  company: number;
}

/** чат */
export interface IChat {
  id?: number;
  chat_status?: number;
  users: number[];
  course?: number;
  company?: number;
  num_messages?: number;
  list_users?: IUser[];
  last_message_date?: string;
}

/** параметр чатов */
export interface IChatParams {
  id?: number;
  chat_status?: number;
  users?: string | string[] | number | number[];
  course?: string;
  company?: string;
  created?: string;
  ordering?: string;
}

/** ответ непрочитанных сообщения */
export interface IUnreadMessagesResponse {
  total_unread_messages: number;
  chats: {
    chat_id: number;
    chat_type: number;
    unread_messages: number;
  }[];
}

/** нормализация непрочитанных сообщений */
export interface IUnreadMessagesNormalized {
  [chatId: number]: number;
}

/** нормализация непрочитанных чатов */
export interface IUnreadChatTypesNormalized {
  [chatType: number]: number;
}

/** сообщение */
export interface IMessage {
  id: number;
  user?: {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    user_status: IUserStatus;
    time_limitation: string;
    company: number;
  };
  created?: string;
  updated?: string;
  mess: string;
  image?: string;
  chat?: number;

  attachment?: unknown[];
  loading?: boolean;
}

/** параметры настроек пользователей */
export interface ISettingParams {
  id?: number;
  course?: number;
  user?: number;
  ordering?: 'course' | 'user';
}

/** ответ настроек пользователей */
export interface ISettingData {
  id?: number;
  user: number;
  course: number;
  start_course: string;
  end_course: string;
}

/** ответ серверного времени */
export interface IServerTime {
  server_time: string;
}

/** Интерфейс настроек быстрой авторизации */
export interface IQuiclkLogin {
  client_login?: string;
  curator_login?: string;
  moderator_login?: string;
  admin_login?: string;
  client_password?: string;
  curator_password?: string;
  moderator_password?: string;
  admin_password?: string;
}

export interface IMutateNotification {
  appearance: 'success' | 'error' | 'warning' | 'info';
  type?: 'read' | 'create' | 'update' | 'delete';
}
