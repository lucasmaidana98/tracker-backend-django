export interface IPassings {
  created: string;
  task: number;
  success_passed: 0 | 1 | 2 | 3 | 4 | 5;
  is_trial: boolean;
  response_rate: string;
}

export interface IIntermediatePassing {
  created: string;
  is_trial: boolean;
  response_rate: string;
  success_passed: number;
  task: {
    course_id: number;
    course_title: string;
    id: number;
    is_final: boolean;
    material: number;
    title: string;
  };
}

export interface IUser {
  id: number;
  first_name: string;
  company: {
    id: number;
    title: string;
  };
  courses: {
    end_course: string;
    g_chat: boolean;
    id: number;
    intermediate_passings: IIntermediatePassing[];
    final_passings: IPassings[];
    num_intermediate_tests: number;
    s_chat: boolean;
    start_course: string;
    t_chat: boolean;
    title: string;
  }[];
  start_course: string;
  end_course: string;
  last_activity: string;
  final_passings: IPassings[];
  intermediate_passings: IPassings[];
  intermediate_test_count: number;
}

export type IUsersList = IUser[];

export interface IParamsUsersList {
  id?: number;
  company?: number;
  courses?: string;
  ordering?: string;
  page?: number;
  page_size?: number;
  search?: string;
}

export interface IPagesUsersList {
  count: number;
  next?: string;
  previous?: string;
  results: IUsersList;
}

export interface ISettingParams {
  id?: number;
  course?: number;
  user?: number;
  ordering?: 'course' | 'user';
}

export interface ISettingData {
  id?: number;
  user: number;
  course: number;
  start_course: string;
  end_course: string;
}

export interface IUserCourse {
  title: string;
  end_course: string;
  g_chat: boolean;
  id: number;
  intermediate_passings: IIntermediatePassing[];
  num_intermediate_tests: number;
  s_chat: boolean;
  start_course: string;
  t_chat: boolean;
}

export type IUserCourses = IUserCourse[];

export interface ICuratorUserActivityParams {
  user?: string;
  ordering?: string;
  page?: number;
  page_size?: number;
}

export interface ICuratorUserActivity {
  user: number;
  day: string;
  seconds: number;
  course_id: number;
}
export interface ICuratorUsersActivityPage {
  count: number;
  next?: string;
  previous?: string;
  results: ICuratorUserActivity[];
}
