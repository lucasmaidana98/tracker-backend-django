export interface IParams {
  id?: number;
  title?: string;
  author?: string;
  tag?: string;
  ordering?: string;
}

export interface ICourseClient {
  id: number;
  author: {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
  };
  tag: {
    id: number;
    tag: string;
  }[];
  created: string;
  updated: string;
  title: string;
  description: string;
  teacher: string;
  is_active: boolean;
  moderators: number[];
  end_course: string;
  start_course: string;
  g_chat: boolean;
  s_chat: boolean;
  t_chat: boolean;
}

export type ICoursesClient = ICourseClient[];

export interface ISettingParams {
  id?: number;
  course?: number;
  user?: number;
  ordering?: 'course' | 'user';
}

export interface ISettingData {
  id?: number;
  user: number;
  course: number;
  start_course: string;
  end_course: string;
}

export interface IStatisticParams {
  id?: number;
  user?: number;
  course_id?: number;
  day?: string;
  ordering?: string;
}

/**
 * Типизация статистики пользователя
 */
export type IStatisticUser = {
  id: number;
  user: number;
  course_id: number;
  day: string;
  seconds: number;
}[];

export interface IMaterialsParams {
  id?: number;
  course?: number;
  title?: string;
  is_download?: boolean;
  parent?: string;
  ordering?: string;
}

export interface IMaterial {
  id: number;
  created: string;
  updated: string;
  title: string;
  text: string;
  video: string;
  pdf: string;
  is_active: boolean;
  is_download: boolean;
  rank: number;
  video_link: string;
  course: number;
  parent: number;
  original_link: number;
}

export interface IMaterialWithChildren extends IMaterial {
  children?: IMaterialWithChildren[];
}

export interface ITasksParams {
  id?: number;
  material?: string;
  title?: string;
  is_necessarily?: boolean;
  is_final?: boolean;
  ordering?: string;
  material__course__in?: number;
}

export interface ITask {
  id: number;
  material: number;
  title: string;
  description: string;
  text_explanation: string;
  prepared_text: string;
  travel_time: string;
  retake_seconds: number;
  passing: number;
  attempts: number;
  is_necessarily: boolean;
  is_chance: boolean;
  is_mix: boolean;
  is_miss: boolean;
  is_final: boolean;
  is_hidden: boolean;
  rank: number;
  trial_attempts: number;
  trial_percents?: unknown;
}

export interface IMateriaPassingsParams {
  id?: number;
  material?: number;
  status?: number;
  ordering?: string;
}

export interface IMaterialPassingResponse {
  id: number;
  created: string;
  updated: string;
  status: 0 | 1 | 2;
  material: number;
  user: number;
}

export interface ICreateMaterialPassing {
  status: 0 | 1 | 2;
  material: number;
  user: number;
}

export interface IUpdateMaterialPassing extends ICreateMaterialPassing {
  id: number;
}

export interface IQuestionsParams {
  id?: number;
  tasks?: number;
  is_free_answer?: boolean;
  score?: number;
  ordering?: string;
}

export interface IQuestion {
  id: number;
  created: string;
  updated: string;
  text: string;
  m_file: string;
  is_free_answer: boolean;
  score: number;
  tasks: number[];
}

export interface ITestPassingsParams {
  id?: number;
  task?: number;
  success_passed?: string;
  ordering?: string;
}

export interface ITestPassing {
  id: number;
  task: number;
  is_final_task: boolean;
  user: number;
  success_passed: 0 | 1 | 2 | 3 | 4 | 5;
  response_rate: string;
  start_time: string;
  out_of_time: boolean;
  finish_time: string;
  travel_time: string;
  task_attempts: string;
  retake_seconds: string;
  is_trial: boolean;
}

export interface IUserAnswer {
  id?: number;
  passing: number;
  question: number;
  answers?: number[];
  text?: string;
  file?: File;
}

export interface IParamsUserAnswers {
  id?: number;
  passing?: number;
  question?: number;
  answer?: number;
  ordering?: string;
}

export interface IAnswersParams {
  id?: number;
  question?: number;
  is_true?: boolean;
  ordering?: 'id' | 'question' | 'is_true';
}

export interface IAnswer {
  id: number;
  created: string;
  updated: string;
  is_true: boolean;
  text: string;
  rank: number;
  question: number;
}

export interface ICurrentUserClient {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  description: string;
  design: 0 | 1; // 0 - main, 1 - intelleca
  webinars: boolean;
  old_password: string;
  user_status: number;
  is_active: boolean;
  company: number;
  company_title: string;
  curator_company: [];
  time_limitation: string;
  date_joined: string;
  courses: {
    id: number;
    title: string;
    description: string;
    author: number;
  }[];
  start_course: string;
  end_course: string;
  teacher_chat: boolean;
  group_chat: boolean;
  tech_chat: boolean;
  last_test_passings: {
    id: number;
    task: number;
    is_final_task: boolean;
    success_passed: number;
    response_rate: string;
    start_time: string;
    out_of_time: boolean;
    finish_time: string;
    travel_time: number;
    retake_seconds: number;
  }[];
}

export interface IBookmarkParams {
  id?: number;
  material?: number;
  title?: string;
  ordering?: string;
}

export interface IBookmark {
  id: number;
  material: number;
  course: string;
  user: number;
  title: string;
  page: number;
}
