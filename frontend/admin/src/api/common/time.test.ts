import axios from 'axios';
import { getServerTime } from './time';
import { setupServer } from 'msw/node';
import { handlers } from '../../mocks/server/handlers';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = 'http://localhost:3000/';
});

afterAll(() => {
  server.close();
});

describe('ендпоинты времени', () => {
  test('функция getServerTime должна вернуть серверное время', async () => {
    const data = await getServerTime();

    expect(data.server_time).toBe('2022-07-26T20:20:12.551399');
  });
});
