import axios from 'axios';
import { setupServer } from 'msw/node';
import { USER_MOCK } from '../../mocks/common';
import { handlers } from '../../mocks/server/handlers';
import { getCurrentUser } from './user';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = 'http://localhost:3000/';
});

afterAll(() => {
  server.close();
});

describe('ендпоинт общей модели пользователя', () => {
  test('функция getCurrentUser должна вернуть обьект текущего пользователя', async () => {
    const data = await getCurrentUser();
    expect(data).toStrictEqual(USER_MOCK);
  });
});
