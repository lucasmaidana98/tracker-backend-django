import axios from 'axios';
import { setupServer } from 'msw/node';
import { handlers } from '../../mocks/server/handlers';
import { getAllChats } from './chat';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = 'http://localhost:3000/';
});

afterAll(() => {
  server.close();
});

describe('ендпоинт общей модели чатов', () => {
  test('функция getAllChats должна вернуть массив чатов', async () => {
    const data = await getAllChats();
    expect(Object.keys(data).length).toBe(2);
  });
});
