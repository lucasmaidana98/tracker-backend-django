import axios from 'axios';
import { setupServer } from 'msw/node';
import { TOKEN_MOCK } from '../../mocks/common';
import { handlers } from '../../mocks/server/handlers';
import { getToken } from './auth';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = 'http://localhost:3000/';
});

afterAll(() => {
  server.close();
});

describe('ендпоинты авторизации', () => {
  test('функция getToken должна вернуть обьект с постоянным и временным токенами', async () => {
    const data = await getToken({ username: 'admin', password: '123' });
    expect(data).toStrictEqual(TOKEN_MOCK);
  });
});
