import axios from 'axios';
import { setupServer } from 'msw/node';
import { handlers, url } from '../../mocks/server/handlers';
import { API } from '..';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = url;
});

afterAll(() => {
  server.close();
});

describe('ендпоинты модели пользователей', () => {
  it('метод all должен вернуть массив курсов', async () => {
    const data = await API.admin.user.all({ page: 1 });
    const output = data.results[0];
    expect(output.id).toBe(1);
  });
});
