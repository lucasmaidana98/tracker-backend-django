import axios from 'axios';
import { setupServer } from 'msw/node';
import { API } from '..';

import { COURSE_DETAIL_MOCK, COURSE_MOCK } from '../../mocks/admin';
import { handlers, url } from '../../mocks/server/handlers';

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
  axios.defaults.baseURL = url;
});

afterAll(() => {
  server.close();
});

describe('api клиент модели курсов интерфейса администратора', () => {
  test('метод all должен вернуть массив курсов', async () => {
    const data = await API.admin.course.all();
    expect(data.length).toBe(1);
    expect(data[0].title).toBe(COURSE_MOCK.title);
  });

  test('метод create должен вернуть объект созданного курса', async () => {
    const data = await API.admin.course.create({ ...COURSE_MOCK, id: undefined });
    expect(Object.keys(data).length).toBe(14);
    expect(data.title).toBe(COURSE_MOCK.title);
  });

  test('метод byId должен вернуть объект курса по айди', async () => {
    const data = await API.admin.course.byId(1);
    expect(data).toStrictEqual(COURSE_DETAIL_MOCK);
  });

  test('метод update должен вернуть объект обновленного курса', async () => {
    const data = await API.admin.course.update(COURSE_MOCK);
    expect(data).toStrictEqual(COURSE_MOCK);
  });

  test('метод remove должен вернуть пустую строку', async () => {
    const data = await API.admin.course.remove(1);
    expect(data).toBe('');
  });
});
