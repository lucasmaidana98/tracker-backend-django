import axios from 'axios';
import Cookie from 'js-cookie';

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
      window.location.replace('/');
      Cookie.remove('access');
    }
    return Promise.reject(error);
  },
);
