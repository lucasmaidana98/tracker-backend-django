export * as activity from './activity';
export * as answer from './answer';
export * as bookmark from './bookmark';
export * as course from './course';
export * as material from './material';
export * as material_passing from './material_passing';
export * as question from './question';
export * as settings from './settings';
export * as test from './test';
export * as test_passing from './test_passing';
export * as user from './user';
