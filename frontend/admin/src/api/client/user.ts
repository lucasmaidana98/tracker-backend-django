import axios from 'axios';
import { ICurrentUserClient } from '../../typings/client';

export async function getCurrentUserClient() {
  try {
    const { data } = await axios.get<ICurrentUserClient>(`/api/v1/client/users/user/`);

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}
