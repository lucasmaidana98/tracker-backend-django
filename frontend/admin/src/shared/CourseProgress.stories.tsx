import CourseProgress from './CourseProgress';

export default {
  title: 'CourseProgress',
  component: CourseProgress,
};

export const SingleCourse = () => (
  <CourseProgress
    courseId={1}
    startDate="2022-08-22T10:00:00Z"
    isShort={false}
    passed={40}
    endDate="2023-08-22T10:00:00Z"
  />
);

export const PluralCourse = () => (
  <div>
    <CourseProgress
      courseId={1}
      startDate="2022-08-22T10:00:00Z"
      isShort={true}
      passed={30}
      endDate="2023-08-22T10:00:00Z"
    />
    <CourseProgress
      courseId={1}
      startDate="2022-08-22T10:00:00Z"
      isShort={true}
      passed={85}
      endDate="2023-08-22T10:00:00Z"
    />
  </div>
);
