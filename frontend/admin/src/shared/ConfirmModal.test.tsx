import { fireEvent, render, screen } from '@testing-library/react';
import ConfirmModal from './ConfirmModal';

describe('ConfirmModal tests', () => {
  it('должен сработать колбек', () => {
    const callback = jest.fn();
    render(
      <ConfirmModal
        confirm={callback}
        isOpen={true}
        hide={() => null}
        loading={false}
        isIntelleka={false}
      >
        <div className="text-left">Вы действительно хотите удалить чат?</div>
      </ConfirmModal>,
    );
    const confirmBtn = screen.getByTestId('confirm');
    fireEvent.click(confirmBtn);
    expect(callback.mock.calls.length).toBe(1);
  });
});
