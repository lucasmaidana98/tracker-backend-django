/** Компонент-заглушка */
const SectionInProgress: React.FC = () => {
  return <div className="text-gray-500 italic">Раздел в разработке</div>;
};

export default SectionInProgress;
