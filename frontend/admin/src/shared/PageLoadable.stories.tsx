import { Document, Page, pdfjs } from 'react-pdf';
import PageLoadable from './PageLoadable';
import pdf from '../mocks/pdf/large.pdf';
import { useState } from 'react';
import { rangeNumbers } from '../utils/array';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

export default {
  title: 'PageLoadable',
  component: PageLoadable,
};

export const Template = () => {
  const [pages, setPages] = useState<number[]>([1, 2, 3, 4, 5, 6, 7]);

  const onLoadSuccess = ({ numPages }: { numPages: number }) => {
    setPages(rangeNumbers(numPages));
  };
  return (
    <Document file={pdf} onLoadSuccess={onLoadSuccess} loading="" noData="" error="">
      {pages.length &&
        pages.map((page) => {
          return (
            <div id={`page-${page}`} key={page}>
              <PageLoadable>
                <Page
                  className="pdf-page mb-3"
                  key={page}
                  loading=""
                  pageNumber={page}
                  scale={1}
                  renderMode="svg"
                  renderTextLayer={false}
                />
              </PageLoadable>
            </div>
          );
        })}
    </Document>
  );
};
