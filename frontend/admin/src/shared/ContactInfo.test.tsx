import { render, screen } from '@testing-library/react';
import ContactInfo from './ContactInfo';

describe('ContactInfo tests', () => {
  it('должен присутствовать имейл интелеки', () => {
    render(<ContactInfo isIntelleka />);
    expect(screen.getByText('support@intelleka.ru')).toBeInTheDocument();
  });

  it('должен присутствовать телефон промресурса', () => {
    render(<ContactInfo />);
    expect(screen.getByText('+7(495)663-71-07')).toBeInTheDocument();
  });
});
