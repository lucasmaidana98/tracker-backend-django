import { render, screen } from '@testing-library/react';
import Arrow from './ArrowLeft';

describe('Arrow component test', () => {
  it('component Arrow should be in document', () => {
    render(<Arrow />);
    expect(screen.getByTestId('arrow-left')).toBeInTheDocument();
  });
});
