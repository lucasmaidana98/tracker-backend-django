import ProgressbarPreloader from './ProgressbarPreloader';

export default {
  title: 'ProgressbarPreloader',
  component: ProgressbarPreloader,
};

export const Template = () => <ProgressbarPreloader />;
