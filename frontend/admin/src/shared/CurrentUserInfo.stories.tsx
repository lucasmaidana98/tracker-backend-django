import CurrentUserInfo from './CurrentUserInfo';

export default {
  title: 'CurrentUserInfo',
  component: CurrentUserInfo,
};

export const Admin = () => (
  <CurrentUserInfo onLogout={() => null} name="admin" description="Администратор" />
);

export const Curator = () => <CurrentUserInfo onLogout={() => null} name="Вовнянко Дарья" />;

export const User = () => (
  <CurrentUserInfo
    onLogout={() => null}
    name="Рогалев Сергей Евгеньевич"
    description="Физическое лицо"
  />
);
