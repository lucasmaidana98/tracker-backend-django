import { TReadyStateLiteral } from '../../utils/react-use-websocket';

export interface IProps {
  value: string;
  connectionStatus: TReadyStateLiteral;
  tooltipText: string;
}
