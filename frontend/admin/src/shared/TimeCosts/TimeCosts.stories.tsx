import TimeCosts from './TimeCosts';

export default {
  title: 'TimeCosts',
  component: TimeCosts,
};

export const Open = () => (
  <TimeCosts connectionStatus="Open" tooltipText="Время проведенное в системе" value="10 часов" />
);

export const Connecting = () => (
  <TimeCosts
    connectionStatus="Connecting"
    tooltipText="Время проведенное в системе"
    value="10 часов"
  />
);

export const Closed = () => (
  <TimeCosts connectionStatus="Closed" tooltipText="Время проведенное в системе" value="10 часов" />
);
