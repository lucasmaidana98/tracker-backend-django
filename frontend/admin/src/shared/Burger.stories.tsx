import { useState } from 'react';
import Burger from './Burger';

export default {
  title: 'Burger',
  component: Burger,
};

export const Close = () => {
  const [open, toggle] = useState(false);

  return <Burger open={open} onClick={() => toggle(!open)} className="cursor-pointer" />;
};

export const Open = () => {
  const [open, toggle] = useState(true);

  return <Burger open={open} onClick={() => toggle(!open)} className="cursor-pointer" />;
};
