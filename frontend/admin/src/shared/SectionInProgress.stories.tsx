import SectionInProgress from './SectionInProgress';

export default {
  title: 'SectionInProgress',
  component: SectionInProgress,
};

export const Template = () => <SectionInProgress />;
