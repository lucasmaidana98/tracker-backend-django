import { fireEvent, render, screen } from '@testing-library/react';
import ButtonSplit from './ButtonSplit';

const options = ['загрузить всех', 'загрузить последние 100', 'загрузить последние 10'];

describe('ButtonSplit tests', () => {
  it('accessibility', () => {
    const mockCallBack = jest.fn();
    render(<ButtonSplit options={options} onClick={mockCallBack} />);
    const button = screen.getByTestId('button-split');
    fireEvent.click(button);
    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
