import { ADMIN_MOCK_USER, ADMIN_USERS_MOCK } from '../mocks/admin';
import { LocationWrapper } from '../mocks/components/Location';
import UserCourses from './UserCourses';

export default {
  title: 'UserCourses',
  component: UserCourses,
};

export const Single = () => (
  <LocationWrapper>
    <UserCourses i={1} u={ADMIN_MOCK_USER} />
  </LocationWrapper>
);
export const Plural = () => (
  <LocationWrapper>
    <UserCourses i={1} u={ADMIN_USERS_MOCK.results[0]} />
  </LocationWrapper>
);
