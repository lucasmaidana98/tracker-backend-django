import { useState } from 'react';
import { ThemeButton } from './Button';
import Modal from './Modal';

export default {
  title: 'Modal',
  component: Modal,
};

export const Template = () => {
  const [open, toggle] = useState(false);

  return (
    <>
      <ThemeButton isIntelleka={false} onClick={() => toggle(true)}>
        Открыть модальное окно
      </ThemeButton>
      <Modal
        open={open}
        onClose={() => toggle(false)}
        className="rounded max-w-screen-modal_md md:mt-7 p-4 flex items-center justify-between"
      >
        <div>Модальное окно</div>
        <ThemeButton isIntelleka={false} className="mt-2" isSecondary onClick={() => toggle(false)}>
          Закрыть
        </ThemeButton>
      </Modal>
    </>
  );
};
