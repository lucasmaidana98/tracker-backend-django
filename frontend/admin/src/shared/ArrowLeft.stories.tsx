import ArrowLeft from './ArrowLeft';

export default {
  title: 'Arrow',
  component: ArrowLeft,
};

const Scale: React.FC<{ scale?: number }> = ({ children, scale = 1 }) => {
  return <div style={{ transform: `scale(${scale})` }}>{children}</div>;
};

const Left = () => <ArrowLeft />;
const Right = () => <ArrowLeft className="rotate-180 max-w-min" />;
const Top = () => <ArrowLeft className="rotate-90 max-w-min" />;
const Bottom = () => <ArrowLeft className="-rotate-90 max-w-min" />;

const Arrows: React.FC<{ scale?: number }> = ({ scale = 1 }) => {
  return (
    <div className="flex max-w-[150px] justify-between">
      <Scale scale={scale}>
        <Left />
      </Scale>
      <Scale scale={scale}>
        <Right />
      </Scale>
      <Scale scale={scale}>
        <Top />
      </Scale>
      <Scale scale={scale}>
        <Bottom />
      </Scale>
    </div>
  );
};

export const Small = () => <Arrows />;
export const Medium = () => <Arrows scale={2} />;
export const Big = () => <Arrows scale={3} />;
