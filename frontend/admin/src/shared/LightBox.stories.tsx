import { useState } from 'react';
import LightBox from './LightBox';
import authBackground from '../img/authBackground.png';

export default {
  title: 'LightBox',
  component: LightBox,
};

export const Image = () => {
  const [isOpen, setOpen] = useState(true);

  return (
    <>
      <div className="relative z-30 w-[100px] cursor-zoom-in" onClick={() => setOpen(true)}>
        <img src={authBackground} alt="media file" className="w-fit" />
      </div>

      {isOpen && <LightBox mainSrc={authBackground} onCloseRequest={() => setOpen(false)} />}
    </>
  );
};
