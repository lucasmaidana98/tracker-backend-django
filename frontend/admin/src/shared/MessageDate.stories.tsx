import { daySeconds } from '../constants';
import { convertSecondsToFormat } from '../utils/time';
import MessageDate from './MessageDate';

export default {
  title: 'MessageDate',
  component: MessageDate,
};

const today = new Date().getTime() / 1000;

const getDate = (seconds: number) => convertSecondsToFormat(seconds, 'dddd, D MMMM');

export const Today = () => <MessageDate date={getDate(today)} />;
export const Yesterday = () => <MessageDate date={getDate(today - daySeconds)} />;
export const Tomorrow = () => <MessageDate date={getDate(today + daySeconds)} />;
