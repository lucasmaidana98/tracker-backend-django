import { useState } from 'react';
import { convertLocalTimeToFormat } from '../utils/time';
import DatePicker from './DatePicker';

export default {
  title: 'DatePicker',
  component: DatePicker,
};

export const Promrgroup = () => {
  const [date, setValue] = useState('');

  return (
    <>
      Дата {date ? date : 'не выбрана'}
      <div className="pt-64 max-w-sm">
        <DatePicker
          onChange={(value) => value && setValue(convertLocalTimeToFormat(value, 'YYYY-MM-DD'))}
          value={date}
          field="date"
          disabled={false}
          dateFormat="dd.MM.yyyy"
        />
      </div>
    </>
  );
};

export const Intelleka = () => {
  const [date, setValue] = useState('');

  return (
    <>
      Дата {date ? date : 'не выбрана'}
      <div className="pt-64 max-w-sm">
        <DatePicker
          onChange={(value) => value && setValue(convertLocalTimeToFormat(value, 'YYYY-MM-DD'))}
          value={date}
          field="date"
          disabled={false}
          dateFormat="dd.MM.yyyy"
          isIntelleka
        />
      </div>
    </>
  );
};
