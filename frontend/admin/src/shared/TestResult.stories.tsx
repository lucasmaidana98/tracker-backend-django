import TestResult from './TestResult';

export default {
  title: 'TestResult',
  component: TestResult,
};

const Template: React.FC<{
  isIntelleka?: boolean;
  isChecking?: boolean;
  isHidden: boolean;
  isTrial?: boolean;
  notPassed?: boolean;
  responseRate: string;
}> = ({ isIntelleka = false, isChecking = false, isTrial = false, ...props }) => {
  const questionsCount = 20;
  const rate = 75;
  const passed = Number(props.responseRate?.replaceAll('%', ''));

  return (
    <TestResult
      isChecking={isChecking}
      isHidden={props.isHidden}
      isIntelleka={isIntelleka}
      isTrial={isTrial}
      notPassed={passed < rate}
      onClose={() => null}
      responseRate={props.responseRate}
      rate={rate}
      retakeSeconds="3600"
      questionsCount={questionsCount}
    />
  );
};

export const OnCheck = () => <Template isChecking isHidden={false} responseRate="80%" />;
export const IsTrial = () => <Template isHidden={false} isTrial responseRate="10:8" />;
export const NotPassed = () => <Template isHidden={false} responseRate="70%" />;
export const Success = () => <Template isHidden={false} responseRate="80%" />;
export const Intelleka = () => <Template isHidden={false} responseRate="80%" isIntelleka />;
