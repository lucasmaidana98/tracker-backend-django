import ThemeTextarea from './ThemeTextarea';

export default {
  title: 'ThemeTextarea',
  component: ThemeTextarea,
};

export const Promrgroup = () => (
  <ThemeTextarea isIntelleka={false} className="w-full resize-none" />
);
export const Intelleka = () => <ThemeTextarea isIntelleka={true} className="w-full resize-none" />;
