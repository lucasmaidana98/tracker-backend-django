import { useState } from 'react';
import DragAndDrop from './DragAndDrop';
import { UPLOAD_IMAGE } from '../img/data-image';

const code = `
<DragAndDrop handleDrop={(files) => console.log(files)}>
  <input type="file"/>
</DragAndDrop>
`;

export default {
  title: 'DragAndDrop',
  component: DragAndDrop,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

export const AllFiles = () => {
  const [file, setFile] = useState<File>();

  const [fileName, changeFileName] = useState('');
  const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = e.target;
    if (files) {
      setFile(files[0]);
      changeFileName(files[0].name);
    }
  };

  return (
    <>
      {file && <div>Размер файла: {file?.size}</div>}
      <DragAndDrop handleDrop={(files) => console.log(files)}>
        <input type="file" id="file_answer" className="hidden" onChange={onChangeInput} />

        <img className="w-[142px] block mx-auto mt-10 mb-4" src={UPLOAD_IMAGE} alt="upload" />
        {fileName ? (
          <div className="text-center">{fileName}</div>
        ) : (
          <div className="text-center">
            <span className="bg-gray-line py-[5px] px-[15px] text-md">Загрузите</span> или
            перетащите сюда файл
          </div>
        )}
      </DragAndDrop>
    </>
  );
};
