import { ReactLocation, Router } from '@tanstack/react-location';
import SvgBook from '../react-svg/SvgBook';
import SvgList from '../react-svg/SvgList';
import { SidebarLink } from './SidebarLink';

const code = `
const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => (
  <Router location={location} routes={[]}>
    <div>
      <SidebarLink icon={<SvgBook />} isIntelleka={isIntelleka}>
        Материалы
      </SidebarLink>
      <br />
      <SidebarLink icon={<SvgList />} isIntelleka={isIntelleka}>
        Тесты
      </SidebarLink>
    </div>
  </Router>
);
`;

export default {
  title: 'SidebarLink',
  component: SidebarLink,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

const location = new ReactLocation();

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => (
  <Router location={location} routes={[]}>
    <div>
      <SidebarLink icon={<SvgBook />} isIntelleka={isIntelleka}>
        Материалы
      </SidebarLink>
      <br />
      <SidebarLink icon={<SvgList />} isIntelleka={isIntelleka}>
        Тесты
      </SidebarLink>
    </div>
  </Router>
);

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
