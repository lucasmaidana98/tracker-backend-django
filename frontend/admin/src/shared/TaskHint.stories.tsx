import TaskHint from './TaskHint';

export default {
  title: 'TaskHint',
  component: TaskHint,
};

export const Template = () => (
  <div className="max-w-lg">
    <TaskHint prepared_text="Тестовое задание состоит из 3 вопросов. Для успешной сдачи необходимо правильно ответить на 75% вопросов. На выполнение отводится 02:00:00." />
  </div>
);
