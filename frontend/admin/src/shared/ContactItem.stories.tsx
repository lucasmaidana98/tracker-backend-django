import ContactItem from './ContactItem';
import mail from '../img/mail.svg';
import phone from '../img/phone.svg';

export default {
  title: 'ContactItem',
  component: ContactItem,
};

export const Email = () => (
  <div className="w-fit">
    <ContactItem
      tooltip={<div className="text-sm">Текст подсказки для почты</div>}
      img={mail}
      text="promr@gmail.com"
      link="link"
    />
  </div>
);

export const Phone = () => (
  <div className="w-fit">
    <ContactItem
      tooltip={<div className="text-sm">Текст подсказки для номера телефона</div>}
      img={phone}
      text="+898983932323"
      link="link"
    />
  </div>
);
