import Footer from './Footer';
import { LocationWrapper as Wrapper } from '../mocks/components/Location';

export default {
  title: 'Footer',
  component: Footer,
};

export const Admin = () => (
  <Wrapper>
    <Footer userStatus={0} />
  </Wrapper>
);
export const User = () => (
  <Wrapper>
    <Footer userStatus={2} />
  </Wrapper>
);
export const Curator = () => (
  <Wrapper>
    <Footer userStatus={3} />
  </Wrapper>
);

export const AdminIntelleka = () => (
  <Wrapper>
    <Footer userStatus={0} isIntelleka />
  </Wrapper>
);
export const UserIntelleka = () => (
  <Wrapper>
    <Footer userStatus={2} isIntelleka />
  </Wrapper>
);

export const CuratorIntelleka = () => (
  <Wrapper>
    <Footer userStatus={3} isIntelleka />
  </Wrapper>
);
