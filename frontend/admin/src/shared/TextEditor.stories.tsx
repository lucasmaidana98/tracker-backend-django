import { useState } from 'react';
import TextEditor from './TextEditor';

export default {
  title: 'TextEditor',
  component: TextEditor,
};

export const Template = () => {
  const [value, setValue] = useState('');

  return <TextEditor onChange={(text) => setValue(text)} value={value} />;
};
