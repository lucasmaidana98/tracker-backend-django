import MessageText from './MessageText';

export default {
  title: 'MessageText',
  component: MessageText,
};

export const Promrgroup = () => (
  <div className="flex flex-col justify-between h-[90px]">
    <MessageText text="Сообщение автора" isAuthorMessage />
    <MessageText text="Ответ собеседника" />
  </div>
);
export const Intelleka = () => (
  <div className="flex flex-col justify-between h-[90px]">
    <MessageText text="Сообщение автора" isAuthorMessage isIntelleka />
    <MessageText text="Ответ собеседника" isIntelleka />
  </div>
);
