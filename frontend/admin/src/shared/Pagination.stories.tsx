import { useState } from 'react';
import { getPagination } from '../utils/pagination';
import Pagination from './Pagination';

const code = `
const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const defaultParams = { page: 1, page_size: 10, search: '' } as const;
  type ParamKeys = keyof typeof defaultParams;
  const count = 1000;
  const [params, updateParams] = useState(defaultParams);
  const paginationValues = getPagination(params.page_size, count);

  const updateParam = (paramLabel: ParamKeys, paramValue: number) =>
    updateParams({ ...params, [paramLabel]: paramValue });

  return (
    <>
      <Pagination
        pagesCountOptions={paginationValues}
        activePage={params.page}
        pageSize={params.page_size}
        countElements={count}
        onClickNext={() => updateParam('page', params.page + 1)}
        onClickPrev={() => updateParam('page', params.page - 1)}
        isIntelleka={isIntelleka}
        onChangeCurrentPage={(page) => updateParam('page', page)}
        onChangePageSize={(pageSize) => {
          updateParam('page', 1);
          updateParam('page_size', pageSize);
        }}
      />
    </>
  );
};
`;

export default {
  title: 'Pagination',
  component: Pagination,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const defaultParams = { page: 1, page_size: 10, search: '' } as const;
  type ParamKeys = keyof typeof defaultParams;
  const count = 1000;
  const [params, updateParams] = useState(defaultParams);
  const paginationValues = getPagination(params.page_size, count);

  const updateParam = (paramLabel: ParamKeys, paramValue: number) =>
    updateParams({ ...params, [paramLabel]: paramValue });

  return (
    <>
      <Pagination
        pagesCountOptions={paginationValues}
        activePage={params.page}
        pageSize={params.page_size}
        countElements={count}
        onClickNext={() => updateParam('page', params.page + 1)}
        onClickPrev={() => updateParam('page', params.page - 1)}
        isIntelleka={isIntelleka}
        onChangeCurrentPage={(page) => updateParam('page', page)}
        onChangePageSize={(pageSize) => {
          updateParam('page', 1);
          updateParam('page_size', pageSize);
        }}
      />
    </>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
