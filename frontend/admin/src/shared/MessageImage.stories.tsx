import MessageImage from './MessageImage';
import authBackground from '../img/authBackground.png';

export default {
  title: 'MessageImage',
  component: MessageImage,
};

export const Image = () => <MessageImage image={authBackground} />;
