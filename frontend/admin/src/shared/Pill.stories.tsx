import Pill from './Pill';

export default {
  title: 'Pill',
  component: Pill,
};

export const Red = () => <Pill>1</Pill>;
export const Gray = () => <Pill background="gray">32</Pill>;
