import ReactSelect from './ReactSelect';

import { times } from 'lodash';
import makeAnimated from 'react-select/animated';

export default {
  title: 'ReactSelect',
  component: ReactSelect,
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const options = times(10, (v) => ({ value: v + 1, label: `value-${v + 1}` }));
  const animatedComponents = makeAnimated();

  return (
    <div className="h-96">
      <ReactSelect
        isIntelleka={isIntelleka}
        defaultValue={[options[0], options[1]]}
        isMulti
        name="colors"
        components={animatedComponents}
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
      />
    </div>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
