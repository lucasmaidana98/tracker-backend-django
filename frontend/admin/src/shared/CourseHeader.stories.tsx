import CourseHeader from './CourseHeader';

export default {
  title: 'CourseHeader',
  component: CourseHeader,
};

export const Promrgroup = () => (
  <CourseHeader title="Слесарь-электрик по ремонту электрооборудования" />
);

export const Intelleka = () => (
  <CourseHeader title="Слесарь-электрик по ремонту электрооборудования" isIntelleka />
);
