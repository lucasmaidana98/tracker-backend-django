import { useState } from 'react';
import RadioToggle from './RadioToggle';

export default {
  title: 'RadioToggle',
  component: RadioToggle,
};

export const Promrgoup = () => {
  const [checked, toggle] = useState(true);

  return <RadioToggle label="check" checked={checked} onChange={() => toggle(!checked)} />;
};

export const Intelleka = () => {
  const [checked, toggle] = useState(true);

  return (
    <RadioToggle label="check" checked={checked} onChange={() => toggle(!checked)} isIntelleka />
  );
};
