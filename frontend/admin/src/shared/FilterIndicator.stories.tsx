import { useState } from 'react';
import FilterIndicator from './FilterIndicator';

export default {
  title: 'FilterIndicator',
  component: FilterIndicator,
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const [visible, toggle] = useState(true);

  const onClick = () => {
    toggle(false);
    setTimeout(toggle, 1000, true);
  };

  return (
    <div className="h-8">
      {visible ? (
        <FilterIndicator isIntelleka={isIntelleka} onClick={onClick}>
          фильтр активен
        </FilterIndicator>
      ) : null}
    </div>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
