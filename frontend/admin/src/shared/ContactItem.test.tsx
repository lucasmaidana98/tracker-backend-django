import { render, screen } from '@testing-library/react';
import ContactItem from './ContactItem';
import mail from '../img/mail.svg';

const tooltipText = 'Текст подсказки для почты';
const mailText = 'promr@gmail.com';
const linkText = 'link';

describe('ContactItem tests', () => {
  it('Компонент должен содержать ссылку, передаваемую в свойствах', () => {
    render(
      <ContactItem
        tooltip={<div className="text-sm">{tooltipText}</div>}
        img={mail}
        text={mailText}
        link={linkText}
      />,
    );

    const link = screen.getByTestId('contact-link');
    expect(link.hasAttribute('href')).toBeTruthy();
  });
  it('Компонент должен содержать текст, передаваемый в свойствах', () => {
    render(
      <ContactItem
        tooltip={<div className="text-sm">{tooltipText}</div>}
        img={mail}
        text={mailText}
        link={linkText}
      />,
    );
    const label = screen.getByText(mailText);
    expect(label.innerHTML).toBe(mailText);
  });
});
