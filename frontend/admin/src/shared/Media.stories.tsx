import Media from './Media';
import authBackground from '../img/authBackground.png';

export default {
  title: 'Media',
  component: Media,
};

export const Image = () => (
  <div className="max-w-xs">
    <Media m_file={authBackground} />
  </div>
);
export const Empty = () => (
  <div className="max-w-xs">
    <Media m_file="not found" />
  </div>
);
