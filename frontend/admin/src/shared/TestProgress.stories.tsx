import TestProgress from './TestProgress';
import { times } from 'lodash';

export default {
  title: 'TestProgress',
  component: TestProgress,
};

const Template: React.FC<{ dotsNum?: number }> = ({ dotsNum = 10 }) => (
  <div className="p-3 max-w-[800px]">
    <TestProgress answeredQuestions={[0, 1, 2]} questions={times(dotsNum)} current={3} />
  </div>
);

export const Direct = () => <Template />;
export const Curve = () => <Template dotsNum={41} />;
