import PassingStatus from './PassingStatus';

export default {
  title: 'PassingStatus',
  component: PassingStatus,
};

export const All = () => (
  <div className="flex items-center w-[70px] justify-between">
    <PassingStatus />
    <PassingStatus status={1} />
    <PassingStatus status={2} />
  </div>
);
export const NotPassed = () => <PassingStatus />;
export const Started = () => <PassingStatus status={1} />;
export const Passed = () => <PassingStatus status={2} />;
