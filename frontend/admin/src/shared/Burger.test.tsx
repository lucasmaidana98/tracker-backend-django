import { fireEvent, render, screen } from '@testing-library/react';
import Burger from './Burger';

describe('Burger tests', () => {
  it('при клике срабатывает колбек', () => {
    const mockCallBack = jest.fn();
    render(<Burger onClick={mockCallBack} />);
    fireEvent.click(screen.getByTestId('burger-icon'));

    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
