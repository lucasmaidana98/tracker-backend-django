import BadgeButton from './BadgeButton';

export default {
  title: 'BadgeButton',
  component: BadgeButton,
};

export const Active = () => <BadgeButton onClick={() => null}>Добавить пользователей</BadgeButton>;

export const Disabled = () => (
  <BadgeButton onClick={() => null} disabled style={{ opacity: 0.5, cursor: 'not-allowed' }}>
    Добавить пользователей
  </BadgeButton>
);
