import { fireEvent, render, screen } from '@testing-library/react';
import ChatForm from './ChatForm';

describe('ChatForm tests', () => {
  it('форма отправляет сообщения', () => {
    render(
      <ChatForm onSend={(e) => expect(e).toBe('message text')} disabled={false} sending={false} />,
    );
    const sendBtn = screen.getByTestId('chat-form-btn');
    const input = screen.getByTestId('chat-textarea');
    fireEvent.change(input, { target: { value: 'message text' } });
    fireEvent.click(sendBtn);
  });
});
