import Tooltip from './Tooltip';

export default {
  title: 'Tooltip',
  component: Tooltip,
};

export const Top = () => (
  <Tooltip placement="top" text="Текст подсказки">
    <div className="my-4 mx-3">Подказка сверху</div>
  </Tooltip>
);
export const Bottom = () => (
  <Tooltip placement="bottom" text="Текст подсказки">
    <div className="my-4 mx-3">Подказка снизу</div>
  </Tooltip>
);

export const Left = () => (
  <div className="ml-36">
    <Tooltip placement="left" text="Текст подсказки">
      <div className="my-4 mx-3">Подказка слева</div>
    </Tooltip>
  </div>
);

export const Right = () => (
  <Tooltip placement="right" text="Текст подсказки">
    <div className="my-4 mx-3">Подказка справа</div>
  </Tooltip>
);
