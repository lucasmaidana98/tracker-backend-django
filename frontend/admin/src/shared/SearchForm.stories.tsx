import { useState } from 'react';
import SearchForm from './SearchForm';

export default {
  title: 'SearchForm',
  component: SearchForm,
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const [loading, toggle] = useState(false);

  return (
    <SearchForm
      isIntelleka={isIntelleka}
      loading={loading}
      submit={() => {
        toggle(true);
        setTimeout(toggle, 500, false);
      }}
    />
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
