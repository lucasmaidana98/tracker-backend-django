import { useRef } from 'react';
import { BOOKMARKS_MOCK } from '../mocks/client';
import Bookmarks from './Bookmarks';

export default {
  title: 'Bookmarks',
  component: Bookmarks,
};

const Component: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const ref = useRef(null);

  return (
    <div className="min-h-[228px]">
      <Bookmarks
        isIntelleka={isIntelleka}
        bookmarks={BOOKMARKS_MOCK}
        refObject={ref}
        setEditing={() => null}
        removeBookmark={() => null}
        deleting={false}
        onClick={() => null}
      />
    </div>
  );
};

export const Promrgroup = () => <Component />;
export const Intelleka = () => <Component isIntelleka />;
