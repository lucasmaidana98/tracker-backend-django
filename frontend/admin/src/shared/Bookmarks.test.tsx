import { render, screen } from '@testing-library/react';
import { BOOKMARKS_MOCK } from '../mocks/client';
import Bookmarks from './Bookmarks';

describe('Bookmarks tests', () => {
  it('видимость первого элемента', () => {
    render(
      <Bookmarks
        bookmarks={BOOKMARKS_MOCK}
        setEditing={() => null}
        removeBookmark={() => null}
        deleting={false}
        onClick={() => null}
      />,
    );
    expect(screen.getByText(BOOKMARKS_MOCK[0].title)).toBeInTheDocument();
  });
});
