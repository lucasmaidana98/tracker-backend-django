import { useState } from 'react';
import { times } from 'lodash';
import CreatableSelect from './CreatableSelect';

export default {
  title: 'CreatableSelect',
  component: CreatableSelect,
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  const options: any[] = times(5, (v) => ({ value: v + 1, label: `value-${v + 1}` }));
  const [value, setValue] = useState<{ value: string; label: string }[]>([options[0]]);

  return (
    <div className="h-96">
      <CreatableSelect
        options={options}
        isMulti
        value={value}
        onCreateOption={(data: string) => setValue([...value, { value: data, label: data }])}
        onChange={(data) => setValue(data)}
        isIntelleka={isIntelleka}
      />
    </div>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
