import moment from 'moment';
import { ADMIN_USERS_MOCK } from '../mocks/admin';
import Table from './Table';

export default {
  title: 'Table',
  component: Table,
};

const titles = ['ФИО', 'Логин', 'Компания', 'Дата регистрации'];
const rows = ADMIN_USERS_MOCK.results.map((u) => [
  <>{u.first_name}</>,
  <>{u.username}</>,
  <>{u.company_title}</>,
  <>{moment(u.date_joined).format('DD.MM.YYYY')}</>,
]);

export const Default = () => <Table rows={rows} titles={titles} />;
export const Loading = () => <Table rows={rows} titles={titles} isLoading />;
export const Empty = () => <Table rows={[]} titles={titles} />;
