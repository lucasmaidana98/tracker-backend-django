import { CHAT_MESSAGES_MOCK, USER_MOCK } from '../mocks/common';
import Message from './Message';

const code = `
<Message data={CHAT_MESSAGES_MOCK[0]} currentUser={USER_MOCK} isIntelleka={isIntelleka} />

<Message
  data={CHAT_MESSAGES_MOCK[1]}
  currentUser={{ ...USER_MOCK, id: 2 }}
  isIntelleka={isIntelleka}
/>
`;

export default {
  title: 'Message',
  component: Message,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => (
  <>
    <Message data={CHAT_MESSAGES_MOCK[0]} currentUser={USER_MOCK} isIntelleka={isIntelleka} />
    <Message
      data={CHAT_MESSAGES_MOCK[1]}
      currentUser={{ ...USER_MOCK, id: 2 }}
      isIntelleka={isIntelleka}
    />
  </>
);

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
