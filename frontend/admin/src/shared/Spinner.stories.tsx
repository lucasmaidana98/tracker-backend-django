import Spinner from './Spinner';

export default {
  title: 'Spinner',
  component: Spinner,
};

export const Promrgroup = () => <Spinner className="w-8 fill-active_p" />;
export const Intelleka = () => <Spinner className="w-8 fill-active_i" />;
