import { fireEvent, render, screen } from '@testing-library/react';
import { USER_MOCK } from '../mocks/common';
import Chat from './Chat';

describe('Chat tests', () => {
  it('чат работает', () => {
    render(
      <Chat
        messageHistory={[]}
        sendJsonMessage={(data) => expect(data.message).toBe('message text')}
        sending={false}
        setSending={() => null}
        WS_CONNECTING={false}
        WS_CONNECTED
        WS_CLOSED={false}
        currentUser={USER_MOCK}
        isIntelleka={false}
      />,
    );
    const sendBtn = screen.getByTestId('chat-form-btn');
    const input = screen.getByTestId('chat-textarea');
    fireEvent.change(input, { target: { value: 'message text' } });
    fireEvent.click(sendBtn);
  });
});
