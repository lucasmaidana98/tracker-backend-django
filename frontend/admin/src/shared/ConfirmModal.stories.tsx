import { useState } from 'react';
import ConfirmModal from './ConfirmModal';

export default {
  title: 'ConfirmModal',
  component: ConfirmModal,
};

const Confirm: React.FC<{ isIntelleka?: boolean }> = (props) => {
  const [open, toggle] = useState(false);
  const [loading, toggleLoading] = useState(false);

  const confirm = () => {
    toggleLoading(true);
    setTimeout(() => {
      toggle(false);
      toggleLoading(false);
    }, 1000);
  };

  return (
    <>
      <button onClick={() => toggle(true)}>Действие</button>
      <ConfirmModal
        confirm={confirm}
        isOpen={open}
        hide={() => toggle(false)}
        loading={loading}
        isIntelleka={props.isIntelleka}
      >
        <div className="text-left">Подтвердить действие?</div>
      </ConfirmModal>
    </>
  );
};

export const Promrgroup = () => {
  return <Confirm />;
};

export const Intelleka = () => {
  return <Confirm isIntelleka />;
};
