import { useState } from 'react';
import ChatForm from './ChatForm';

export default {
  title: 'ChatForm',
  component: ChatForm,
};

export const Promrgroup = () => {
  const [sending, toggle] = useState(false);

  const onSend = (data: string) => {
    console.log(data);
    toggle(true);
    setTimeout(() => toggle(false), 1000);
  };
  return <ChatForm onSend={onSend} disabled={false} sending={sending} />;
};

export const Intelleka = () => {
  const [sending, toggle] = useState(false);

  const onSend = (data: string) => {
    console.log(data);
    toggle(true);
    setTimeout(() => toggle(false), 1000);
  };
  return <ChatForm onSend={onSend} disabled={false} sending={sending} isIntelleka />;
};
