import Truncate from './Truncate';

export default {
  title: 'Truncate',
  component: Truncate,
};

export const Template = () => <Truncate count={26} title={'Компания с длинным заголовком'} />;
