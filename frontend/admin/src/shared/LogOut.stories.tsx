import LogOut from './LogOut';

export default {
  title: 'LogOut',
  component: LogOut,
};

export const Admin = () => <LogOut userName="Admin" />;
export const User = () => <LogOut userName="Алиев Роман Васильевич " />;
