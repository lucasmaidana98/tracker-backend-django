import { render, screen } from '@testing-library/react';
import AuthError from './AuthError';

describe('AuthError component test', () => {
  it('component AuthError should be in document', () => {
    render(<AuthError />);
    expect(screen.getByTestId('auth-error')).toBeInTheDocument();
  });
});
