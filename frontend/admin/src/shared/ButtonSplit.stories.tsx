import ButtonSplit from './ButtonSplit';

export default {
  title: 'ButtonSplit',
  component: ButtonSplit,
};

const options = ['загрузить всех', 'загрузить последние 100', 'загрузить последние 10'];

export const Promrgroup = () => (
  <ButtonSplit options={options} onClick={(value) => console.log(value)} />
);

export const Intelleka = () => (
  <ButtonSplit options={options} onClick={(value) => console.log(value)} isIntelleka />
);
