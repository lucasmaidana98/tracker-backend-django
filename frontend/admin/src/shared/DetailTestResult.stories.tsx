import { LocationWrapper } from '../mocks/components/Location';
import DetailTestResult from './DetailTestResult';

export default {
  title: 'DetailTestResult',
  component: DetailTestResult,
};

const Template: React.FC<{
  onCheck?: boolean;
  passed?: number;
  count?: number;
}> = ({ count = 20, onCheck = false, ...props }) => {
  const passed = props.passed || 15;
  const percents = Math.round((100 / count) * passed);

  return (
    <LocationWrapper>
      <DetailTestResult
        title="Итоговое тестирование"
        hasResults={!!props.passed}
        link="/"
        onCheck={onCheck}
        isSuccess={percents > 80}
        count={count}
        passed={passed}
        percents={`${percents}%`}
      />
    </LocationWrapper>
  );
};

export const Success = () => <Template passed={18} />;
export const OnCheck = () => <Template passed={10} onCheck />;
export const Failed = () => <Template passed={3} />;
export const NoResults = () => <Template />;
