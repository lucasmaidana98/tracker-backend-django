import RowPreloader from './RowPreloader';

export default {
  title: 'RowPreloader',
  component: RowPreloader,
};

export const Template = () => <RowPreloader />;
