import { USER_ACTIVITY_MOCK } from '../mocks/admin';
import DeviceTimeItem from './DeviceTimeItem';

export default {
  title: 'DeviceTimeItem',
  component: DeviceTimeItem,
};

export const Less = () => <DeviceTimeItem currentDay="14.11.2022" devices={USER_ACTIVITY_MOCK} />;
export const More = () => <DeviceTimeItem currentDay="16.11.2022" devices={USER_ACTIVITY_MOCK} />;
