import BookmarkForm from './BookmarkForm';

export default {
  title: 'BookmarkForm',
  component: BookmarkForm,
};

export const Promrgroup = () => (
  <BookmarkForm
    onClose={() => null}
    isIntelleka={false}
    material=""
    submit={(data) => console.log(data)}
    loading={false}
    page={1}
  />
);

export const Intelleka = () => (
  <BookmarkForm
    onClose={() => null}
    isIntelleka
    material="Новая закладка"
    submit={(data) => console.log(data)}
    loading={false}
    page={1}
  />
);
