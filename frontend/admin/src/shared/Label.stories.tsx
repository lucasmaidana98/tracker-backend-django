import { useState } from 'react';
import Label from './Label';

export default {
  title: 'Label',
  component: Label,
};

export const Input = () => (
  <Label label="ФИО">
    <input className="input" />
  </Label>
);

export const Checkbox = () => {
  const [isIntelleka, setIsIntelleka] = useState<boolean>(true);

  return (
    <>
      <Label label="Интеллека">
        <input
          type="radio"
          name="group"
          checked={isIntelleka}
          onChange={() => setIsIntelleka(true)}
        />
      </Label>
      <Label label="ПромРесурс">
        <input
          type="radio"
          name="group"
          checked={!isIntelleka}
          onChange={() => setIsIntelleka(false)}
        />
      </Label>
    </>
  );
};
