import { useState } from 'react';
import RadioButton from './RadioButton';

const code = `
const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka }) => {
  const checkBoxValues = [1, 2, 3];
  const radioValues = [1, 2, 3];
  const [checkBoxValuesState, setCheckBoxValuesState] = useState<number[]>([1]);
  const [radioValuesState, setrRadioValuesState] = useState<number[]>([1]);

  return (
    <div className="flex max-w-[300px] justify-between">
      <div>
        <div>Checkbox</div>
        <div>
          {checkBoxValues.map((v) => {
            const isChecked = checkBoxValuesState.includes(v);
            return (
              <div
                onClick={() =>
                  isChecked
                    ? setCheckBoxValuesState(checkBoxValuesState.filter((el) => el !== v))
                    : setCheckBoxValuesState(checkBoxValuesState.concat(v))
                }
                className="flex my-2 cursor-pointer"
              >
                <RadioButton checked={isChecked} isCheckBox isIntelleka={isIntelleka} />
                <div className="select-none">checked {v}</div>
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <div>Radio</div>
        <div>
          {radioValues.map((v) => {
            const isChecked = radioValuesState.includes(v);
            return (
              <div onClick={() => setrRadioValuesState([v])} className="flex my-2 cursor-pointer">
                <RadioButton checked={isChecked} isIntelleka={isIntelleka} />
                <div className="select-none">checked {v}</div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
`;

export default {
  title: 'RadioButton',
  component: RadioButton,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka }) => {
  const checkBoxValues = [1, 2, 3];
  const radioValues = [1, 2, 3];
  const [checkBoxValuesState, setCheckBoxValuesState] = useState<number[]>([1]);
  const [radioValuesState, setrRadioValuesState] = useState<number[]>([1]);

  return (
    <div className="flex max-w-[300px] justify-between">
      <div>
        <div>Checkbox</div>
        <div>
          {checkBoxValues.map((v) => {
            const isChecked = checkBoxValuesState.includes(v);
            return (
              <div
                onClick={() =>
                  isChecked
                    ? setCheckBoxValuesState(checkBoxValuesState.filter((el) => el !== v))
                    : setCheckBoxValuesState(checkBoxValuesState.concat(v))
                }
                className="flex my-2 cursor-pointer"
              >
                <RadioButton checked={isChecked} isCheckBox isIntelleka={isIntelleka} />
                <div className="select-none">checked {v}</div>
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <div>Radio</div>
        <div>
          {radioValues.map((v) => {
            const isChecked = radioValuesState.includes(v);
            return (
              <div onClick={() => setrRadioValuesState([v])} className="flex my-2 cursor-pointer">
                <RadioButton checked={isChecked} isIntelleka={isIntelleka} />
                <div className="select-none">checked {v}</div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
