import NotificationItem from './NotificationItem';

export default {
  title: 'NotificationItem',
  component: NotificationItem,
};

export const ShortMessage = () => (
  <NotificationItem
    author="admin"
    message_text="Добрый день, как у вас дела?"
    onClick={() => null}
    onClose={() => null}
  />
);

export const LongMessage = () => (
  <NotificationItem
    author="admin"
    message_text="Добрый день, как у вас дела? Это текст с очень длинным сообщением длинным сообщением длинным сообщением длинным сообщением  длинным сообщением длинным сообщением длинным сообщением длинным сообщением длинным сообщением"
    onClick={() => null}
    onClose={() => null}
  />
);
