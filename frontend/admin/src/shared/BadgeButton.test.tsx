import { render, screen } from '@testing-library/react';
import BadgeButton from './BadgeButton';

describe('тесты компонента BadgeButton', () => {
  it('component BadgeButton should be in document', () => {
    render(<BadgeButton>Hello</BadgeButton>);
    expect(screen.getByTestId('badge-button').innerHTML).toBe('Hello');
  });
});
