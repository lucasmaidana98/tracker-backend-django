import MessageLink from './MessageLink';

export default {
  title: 'MessageLink',
  component: MessageLink,
};

export const Author = () => (
  <MessageLink link="https://www.youtube.com/watch?v=VNoF88M7SfQ" isAuthorMessage />
);
export const Companion = () => <MessageLink link="https://www.youtube.com/watch?v=VNoF88M7SfQ" />;

export const File = () => (
  <MessageLink
    isAuthorMessage
    link="https://firebasestorage.googleapis.com/v0/b/promr-group.appspot.com/o/task.txt?alt=media|text/|task.txt"
  />
);
