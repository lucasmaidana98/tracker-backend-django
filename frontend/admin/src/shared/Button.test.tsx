import { fireEvent, render, screen } from '@testing-library/react';
import Button from './Button';

describe('Button tests', () => {
  it('при клике срабатывает колбек', () => {
    const mockCallBack = jest.fn();
    render(<Button onClick={mockCallBack}>text</Button>);
    const button = screen.getByTestId('button');
    fireEvent.click(button);

    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
