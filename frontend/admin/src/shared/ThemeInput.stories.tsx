import ThemeInput from './ThemeInput';

export default {
  title: 'ThemeInput',
  component: ThemeInput,
};

export const Promrgroup = () => <ThemeInput isIntelleka={false} className="w-full resize-none" />;
export const Intelleka = () => <ThemeInput isIntelleka={true} className="w-full resize-none" />;
