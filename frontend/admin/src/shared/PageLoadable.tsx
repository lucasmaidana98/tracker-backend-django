import { useRef } from 'react';
import { useOnScreen } from '../hooks/useOnScreen';

/**
 * Комопнент для рендера больших pdf файлов
 */
const PageLoadable: React.FC = (props) => {
  const { children } = props;
  const ref = useRef<HTMLDivElement>(null);
  const onScreen = useOnScreen(ref, '1500px 0px 1500px 0px', 'material-view');

  return (
    <div ref={ref}>
      {onScreen ? (
        <div>{children}</div>
      ) : (
        <div className="bg-white overflow-hidden w-full h-[100vh] select-none m-auto" />
      )}
    </div>
  );
};

export default PageLoadable;
