import Header from './Header';
import CurrentUserInfo from './CurrentUserInfo';

export default {
  title: 'Header',
  component: Header,
};

const UserInfo = () => (
  <CurrentUserInfo name="admin" description="Администратор" onLogout={() => null} />
);

export const Promrgroup = () => <Header userInfo={<UserInfo />} />;
export const Intelleka = () => <Header userInfo={<UserInfo />} isIntelleka />;
