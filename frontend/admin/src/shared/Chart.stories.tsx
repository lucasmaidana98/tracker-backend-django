import { ONLINE_HISTORY_MOCK } from '../mocks/admin';
import Chart from './Chart';

export default {
  title: 'Chart',
  component: Chart,
};

export const Default = () => (
  <Chart history={ONLINE_HISTORY_MOCK} isFetching={false} width={500} height={250} />
);
export const Loading = () => <Chart history={ONLINE_HISTORY_MOCK} isFetching={true} />;
