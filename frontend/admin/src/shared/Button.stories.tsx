import Button from './Button';

export default {
  title: 'Button',
  component: Button,
};

export const Promrgroup = () => {
  return (
    <>
      <Button isSecondary onClick={() => null} className="mr-2">
        Отменить
      </Button>
      <Button isSecondary={false} onClick={() => null}>
        Отправить
      </Button>
    </>
  );
};

export const Intelleka = () => {
  return (
    <>
      <Button isSecondary onClick={() => null} className="mr-2" isIntelleka>
        Отменить
      </Button>
      <Button isSecondary={false} onClick={() => null} isIntelleka>
        Отправить
      </Button>
    </>
  );
};
