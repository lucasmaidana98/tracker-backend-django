import { default as AvatarComponent } from './Avatar';

export default {
  title: 'Avatar',
  component: AvatarComponent,
};

export const Promrgroup = () => {
  return (
    <div className="flex justify-between max-w-[130px]">
      <AvatarComponent isAuthorMessage login="Vasya" />
      <AvatarComponent login="Petya" />
    </div>
  );
};

export const Intelleka = () => {
  return (
    <div className="flex justify-between max-w-[130px]">
      <AvatarComponent isAuthorMessage login="Vasya" isIntelleka />
      <AvatarComponent login="Petya" isIntelleka />
    </div>
  );
};
