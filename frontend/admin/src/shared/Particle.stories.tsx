import { times } from 'lodash';
import Particle from './Particle';

const code = `
{times(80).map((_, index) => (
  <Particle key={index}>{index < 45 ? 4 : 0}</Particle>
))}
`;

export default {
  title: 'Particle',
  component: Particle,
  parameters: {
    docs: {
      source: {
        code,
        language: 'tsx',
        type: 'auto',
      },
    },
  },
};

const Template: React.FC<{ isIntelleka?: boolean }> = ({ isIntelleka = false }) => {
  return (
    <div
      className={`relative flex items-center justify-center h-[300px] bg-white overflow-hidden ${
        isIntelleka ? 'text-active_i' : 'text-active_p'
      }`}
    >
      {times(80).map((_, index) => (
        <Particle key={index}>{index < 45 ? 4 : 0}</Particle>
      ))}
    </div>
  );
};

export const Promrgroup = () => <Template />;
export const Intelleka = () => <Template isIntelleka />;
