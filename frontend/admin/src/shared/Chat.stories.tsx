import { useCallback, useEffect, useRef, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { CHAT_MESSAGES_MOCK, getMockMessage, USER_MOCK } from '../mocks/common';
import Component from './Chat';

export default {
  title: 'Chat',
  component: Component,
};

const Chat: React.FC<{ isIntelleka: boolean }> = (props) => {
  const scrollBarRef = useRef<Scrollbars>(null);

  const [messages, setMessages] = useState(CHAT_MESSAGES_MOCK);
  const [newMessage, setNewMessage] = useState<string>('');

  useEffect(() => {
    if (newMessage) {
      const message = getMockMessage(newMessage);
      const newMessages = [...messages, message];
      setMessages(newMessages);
      scrollToBottom();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newMessage]);

  const scrollToBottom = useCallback(() => {
    setTimeout(() => scrollBarRef.current?.scrollToBottom(), 0);
  }, []);

  useEffect(() => {
    scrollToBottom();
  }, [scrollToBottom]);

  return (
    <Component
      messageHistory={messages}
      sendJsonMessage={(data) => setNewMessage(data.message)}
      sending={false}
      setSending={() => null}
      WS_CONNECTING={false}
      WS_CONNECTED
      WS_CLOSED={false}
      currentUser={USER_MOCK}
      scrollBarRef={scrollBarRef}
      isIntelleka={props.isIntelleka}
    />
  );
};

export const Promrgroup = () => <Chat isIntelleka={false} />;
export const Intelleka = () => <Chat isIntelleka />;
