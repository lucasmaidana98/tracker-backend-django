import { render, screen } from '@testing-library/react';
import Avatar from './Avatar';

describe('тесты компонента Avatar', () => {
  it('component Avatar should be in document', () => {
    render(<Avatar />);
    expect(screen.getByTestId('avatar')).toBeInTheDocument();
  });

  it('должны рендериться две заглавные буквы логина', () => {
    render(<Avatar login="vasya" />);
    expect(screen.getByTestId('login').innerHTML).toBe('ва');
  });
});
