import { useState } from 'react';
import { ADMINS_RES_MOCK, USERS_RES_MOCK } from '../mocks/admin';
import OneClickSetting from './OneClickSetting';

export default {
  title: 'OneClickSetting',
  component: OneClickSetting,
};

export const Users = () => {
  const [username, setUserName] = useState('');

  return (
    <OneClickSetting
      loading={false}
      list={USERS_RES_MOCK}
      onChose={(checked, username) => setUserName(checked ? '' : username)}
      label="Пользователь"
      userName={username || 'не выбран'}
    />
  );
};

export const Admins = () => {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  return (
    <OneClickSetting
      loading={false}
      list={ADMINS_RES_MOCK}
      onChose={(checked, username) => setUserName(checked ? '' : username)}
      label="Админ"
      userName={username || 'не выбран'}
      onChangeInput={(value) => setPassword(value)}
      password={password}
    />
  );
};
