import ContactInfo from './ContactInfo';

export default {
  title: 'ContactInfo',
  component: ContactInfo,
};

export const Promrgroup: React.FC = () => {
  return (
    <div className="flex justify-between max-w-[400px]">
      <ContactInfo />
    </div>
  );
};

export const Intelleka: React.FC = () => {
  return (
    <div className="flex justify-between max-w-[400px]">
      <ContactInfo isIntelleka />
    </div>
  );
};
