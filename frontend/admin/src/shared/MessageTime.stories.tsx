import MessageTime from './MessageTime';

export default {
  title: 'MessageTime',
  component: MessageTime,
};

export const Companion = () => <MessageTime date={`${new Date()}`} />;
export const Author = () => <MessageTime date={`${new Date()}`} isAuthorMessage />;
