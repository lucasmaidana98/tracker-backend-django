const SvgNext: React.FC = () => (
  <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" fill="currentColor">
    <path d="M12 8V4l8 8-8 8v-4H4V8z"></path>
  </svg>
);

export default SvgNext;
