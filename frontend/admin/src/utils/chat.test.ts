import { CHAT_MOCK } from '../mocks/admin';
import { getChatCompanies, filterSupportChats } from './chat';

describe('утилиты по работе с чатами', () => {
  test('функция getChatCompanies должна отсортировать айди чатов по компаниям', () => {
    const input = [CHAT_MOCK];
    const output = { '1': [1] };
    expect(getChatCompanies(input, 1)).toStrictEqual(output);
  });

  test('функция filterSupportChats должна отфильтровать массив чатов по наличию сообщений а также пользователей со статусом 2', () => {
    const input = [
      CHAT_MOCK,
      { ...CHAT_MOCK, num_messages: undefined },
      { ...CHAT_MOCK, list_users: [] },
    ];
    const output = [CHAT_MOCK];
    expect(filterSupportChats(input)).toStrictEqual(output);
  });
});
