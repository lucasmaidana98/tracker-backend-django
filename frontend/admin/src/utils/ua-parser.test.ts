import { getClientBrowserAndOs } from './ua-parser';

describe('утилиты по работе с библиотекой [ua-parser-js]', () => {
  test('функция возвращает данные ОС и браузера текущего клиента', () => {
    expect(Object.keys(getClientBrowserAndOs())).toStrictEqual(['os', 'browser']);
  });
});
