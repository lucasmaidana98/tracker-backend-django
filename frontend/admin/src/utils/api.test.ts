import { AxiosError } from 'axios';
import { createMemoryHistory } from 'history';
import {
  getError,
  getErrorMessage,
  parseJwt,
  loginRedirects,
  getAllowedRoutes,
  toFormData,
} from './api';

describe('утилиты по работе с http-запросами', () => {
  test('функция getError должна возвращаеть строку с обьектом ошибки', () => {
    expect(getError({} as AxiosError)).toEqual('Неизвестная ошибка');
    expect(
      getError({
        response: {
          data: {
            error: 'текст ошибки',
          },
        },
      } as AxiosError),
    ).toEqual('{"error":"текст ошибки"}');
  });

  test('функция getErrorMessage должна возвращаеть строку с текстом ошибки', () => {
    expect(typeof getErrorMessage({} as AxiosError)).toBe('string');
  });

  test('функция parseJwt должна распарсить токен в объект', () => {
    expect(
      parseJwt(
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU5ODczNzIzLCJqdGkiOiJkNDg5OGNiY2Q5MDQ0NWY3OGZlZTJhNDIyMjg2ODI3YyIsInVzZXJfaWQiOjEsInN0YXR1cyI6MCwic3RhdHVzX2Rpc3BsYXkiOiJcdTA0MTBcdTA0MzRcdTA0M2NcdTA0MzhcdTA0M2RcdTA0MzhcdTA0NDFcdTA0NDJcdTA0NDBcdTA0MzBcdTA0NDJcdTA0M2VcdTA0NDAifQ.XT5P6QaQH5y7g4LmpfquB7D_rns0XO5faoGMEnGsMws',
      ),
    ).toEqual({
      token_type: 'access',
      exp: 1659873723,
      jti: 'd4898cbcd90445f78fee2a422286827c',
      user_id: 1,
      status: 0,
      status_display: 'Администратор',
    });
  });

  test('функция loginRedirects должна переадресовать роут в интерфейс администратора', () => {
    const history = createMemoryHistory({ initialEntries: ['/'] });
    loginRedirects(
      (data) => history.replace(data),
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU5ODczNzIzLCJqdGkiOiJkNDg5OGNiY2Q5MDQ0NWY3OGZlZTJhNDIyMjg2ODI3YyIsInVzZXJfaWQiOjEsInN0YXR1cyI6MCwic3RhdHVzX2Rpc3BsYXkiOiJcdTA0MTBcdTA0MzRcdTA0M2NcdTA0MzhcdTA0M2RcdTA0MzhcdTA0NDFcdTA0NDJcdTA0NDBcdTA0MzBcdTA0NDJcdTA0M2VcdTA0NDAifQ.XT5P6QaQH5y7g4LmpfquB7D_rns0XO5faoGMEnGsMws',
    );
    expect(history.location.pathname).toBe('/admin');
  });

  test('функция getAllowedRoutes должна вернуть не пустой массив роутов', () => {
    expect(
      getAllowedRoutes(
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU5ODc2ODM2LCJqdGkiOiJlN2UzYmIyYjllYmI0NWFiYjQwODU3Y2JhYjc5NDI3ZiIsInVzZXJfaWQiOjQ2NDksInN0YXR1cyI6Miwic3RhdHVzX2Rpc3BsYXkiOiJcdTA0MWFcdTA0M2JcdTA0MzhcdTA0MzVcdTA0M2RcdTA0NDIifQ.BsiZpTTFWEJc-VWcBgTdqAiS6CnQnDN6AolqBtZSKs4',
      ),
    ).toHaveLength(1);
  });

  test('функция toFormData должна преобразовать объект в formData', () => {
    expect(toFormData({ test: 'data' })).toBeInstanceOf(FormData);
  });
});
