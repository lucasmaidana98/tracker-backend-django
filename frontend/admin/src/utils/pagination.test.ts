import { getCountPages, getPagination } from './pagination';

describe('утилиты по работе с компонентом пагинации', () => {
  test('функция getCountPages должна высчитать количество страниц по заданным параметрам', () => {
    expect(getCountPages(100, 10)).toBe(10);
  });

  test('функция getPagination должна вернуть массив значений для селекта в пагинации', () => {
    const output = [
      { label: '50', value: 50 },
      { label: '100', value: 100 },
      { label: '500', value: 500 },
      { label: '1000', value: 1000 },
      { label: 'Всех', value: 100 },
    ];

    expect(getPagination(100, 10)).toStrictEqual(output);
  });
});
