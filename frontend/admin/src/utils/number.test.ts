import { getRandomInt, countByPercent } from './number';

describe('утилиты по работе с числами', () => {
  test('функция getRandomInt должна сгенеровать случайное число от нуля до указанного в параметре', () => {
    expect(typeof getRandomInt(20)).toBe('number');
    expect(getRandomInt(20)).toBeGreaterThanOrEqual(0);
    expect(getRandomInt(20)).toBeLessThanOrEqual(20);
  });
  test('функция высчитывает число из процента', () => {
    expect(countByPercent(100, 20)).toBe(20);
    expect(countByPercent(5, 20)).toBe(1);
    expect(countByPercent(1000, 25)).toBe(250);
    expect(countByPercent(20, 10)).toBe(2);
  });
});
