import { isElementVisibleOnScreen } from './dom';

describe('утилиты по работе с DOM-элементами', () => {
  test('функция isElementVisibleOnScreen должна проверить видимость дом-элемента на экране', () => {
    expect(isElementVisibleOnScreen(document.createElement('div'))).toBe(null);
  });
});
