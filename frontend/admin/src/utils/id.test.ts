import { generateIdDashed, generateID } from './id';

describe('утилиты по генерации ID', () => {
  test('функция generateIdDashed должна сгенерировать ID', () => {
    expect(typeof generateIdDashed()).toBe('string');
    expect(generateIdDashed().length).toBe(36);
  });

  test('функция generateID должна сгенерировать ID', () => {
    expect(typeof generateID()).toBe('string');
    expect(generateID().length).toBeGreaterThanOrEqual(7);
  });
});
