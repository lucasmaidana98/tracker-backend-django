import { parseResponseRate } from './passing';

describe('утилиты по работе с прохождениями материалов и тестов', () => {
  test('Функция parseResponseRate должна распарсить результат прохождения тестовой попытки в читаемую строку', () => {
    const input = '12:20',
      output = 'Зачтено 12 вопросов из 20';

    expect(parseResponseRate(input)).toBe(output);
  });
});
