import {
  clearStringHtml,
  getLastWord,
  getNumbersFromString,
  maxStringLength,
  russianEndings,
  sliceBefore,
} from './string';

describe('утилиты по работе со строками', () => {
  test('функция russianEndings принимает число с вариантами окончания и возвращает подходящее, в зависимости от числа', () => {
    expect(russianEndings(2, ['вопрос', 'вопроса', 'вопросов'])).toBe('вопроса');
    expect(russianEndings(8, ['тест', 'теста', 'тестов'])).toBe('тестов');
  });

  test('функция clearStringHtml очищает строку от html тегов, возвращая только ее контент', () => {
    expect(clearStringHtml('<div>hello</div>')).toBe('hello');
    expect(clearStringHtml('<h1>world</h1>')).toBe('world');
  });

  test('функция maxStringLength обезает строку по параметру максимальной длинны и добавляет троеточие в конце', () => {
    expect(maxStringLength('string', 2)).toBe('st...');
    expect(maxStringLength('long long string', 10)).toBe('long long ...');
  });

  test('функция sliceBefore обрезает строку до условного символа или строки', () => {
    expect(sliceBefore('short string', ' ')).toBe('string');
    expect(sliceBefore('123+4_', '+')).toBe('4_');
  });

  test('функция getLastWord возвращает последнее слово строки', () => {
    expect(getLastWord('short string')).toBe('string');
    expect(getLastWord('hello world')).toBe('world');
  });

  test('функция getNumbersFromString вырезает числа из строки, на основе регулярного выражения', () => {
    expect(getNumbersFromString('string2lklkj23')).toBe('223');
    expect(getNumbersFromString('string2lklkj21wed0dewded9')).toBe('22109');
  });
});
