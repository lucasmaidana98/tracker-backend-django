import { strucutureObj } from '../mocks/admin';
import { cleanObject, existParams, getObjectSize, getValueByKey, objectToArray } from './object';

const objectInput = { name: 'John', age: undefined, username: null, position: 'director' };
const objectOutput = { name: 'John', position: 'director' };

describe('утилиты по работе с объектами', () => {
  test('Функция cleanObject должна очистить объект от полей со значением (null, undefined)', () => {
    expect(cleanObject(objectInput)).toStrictEqual(objectOutput);
  });

  test('Функция existParams должна очистить объект от полей со значением (null, undefined)', () => {
    expect(existParams(objectInput)).toStrictEqual(objectOutput);
    expect(existParams({})).toBeFalsy();
  });

  test('Функция getObjectSize должна посчитать количество полей в объекте', () => {
    expect(getObjectSize(objectInput)).toBe(2);
  });

  test('Функция objectToArray должна преобразовать объект в массив', () => {
    expect(objectToArray(objectInput)).toStrictEqual(['John', 'director']);
  });

  test('функция getValueByKey должна вернуть значение из объекта по ключу', () => {
    expect(getValueByKey(strucutureObj, 'Обеспечение заданной точности обработки в ГПС')).toBe(
      11360,
    );
  });
});
