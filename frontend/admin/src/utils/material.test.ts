import {
  getCurrentSectionMaterials,
  getStructuredMaterials,
  getTaskTopic,
  sortMaterials,
} from './material';

describe('утилиты по работе с материалами курса', () => {
  test('Функция должна рекурсивно структурировать материалы по вложенностям', () => {
    const input = [
      {
        id: 1,
        title: 'string',
        parent: null,
        rank: 0,
        is_download: false,
        pdf: 'string',
      },
      {
        id: 2,
        title: 'string',
        parent: 1,
        rank: 0,
        is_download: false,
        pdf: 'string',
      },
    ];
    const output = [
      {
        id: 1,
        title: 'string',
        parent: null,
        rank: 0,
        is_download: false,
        pdf: 'string',
        children: [
          {
            id: 2,
            title: 'string',
            parent: 1,
            rank: 0,
            is_download: false,
            pdf: 'string',
          },
        ],
      },
    ];

    expect(getStructuredMaterials(input)).toStrictEqual(output);
  });

  test('Функция sortMaterials должна отсортировать материал по ключу, представленном в параметрах', () => {
    const input = [
      { id: 1, rank: 0 },
      { id: 3, rank: 2 },
      { id: 2, rank: 1 },
    ];
    const output = [
      { id: 1, rank: 0 },
      { id: 2, rank: 1 },
      { id: 3, rank: 2 },
    ];
    expect(sortMaterials(input, 'rank')).toStrictEqual(output);
  });

  test('функция getTaskTopic должна вернуть заголовок материала верхнего уровня', () => {
    const input = [
      {
        id: 1,
        course: 1,
        title: 'title 1',
        rank: 0,
        text: 'text',
        video: '',
        video_link: '',
        pdf: 'pdf file',
        is_download: false,
        parent: null,
        original_link: 1,
        original_data: '',
        created: '',
        updated: '',
        is_active: false,
      },
    ];
    expect(getTaskTopic(2)).toBe('');
    expect(getTaskTopic()).toBe('');
    expect(getTaskTopic(1, input)).toBe('title 1');
  });

  test('функция getCurrentSectionMaterials должна вычислить кол-во материалов в секции прогрессбара', () => {
    const materialShort = {
      id: 1,
      title: 'Topic 1',
      parent: null,
      rank: 0,
      is_download: false,
      pdf: '',
    };
    const materialFull = {
      id: 2,
      course: 2,
      title: 'Topic 1',
      rank: 0,
      text: 'text',
      video: '',
      video_link: '',
      pdf: '',
      is_download: false,
      parent: null,
      original_link: 0,
      original_data: '',
      created: '',
      updated: '',
      is_active: true,
    };
    const materialChild1 = { ...materialFull, parent: 1, title: 'child 1' };
    const materialChild2 = { ...materialFull, parent: 1, title: 'child 2', id: 3 };
    const materialChild3 = { ...materialFull, parent: 1, title: 'child 3', id: 4 };
    const materialChild4 = { ...materialFull, parent: 1, title: 'child 3', id: 5 };
    const fullStructured = [
      {
        ...materialShort,
        children: [materialChild1, materialChild2, materialChild3, materialChild4],
      },
    ];
    const materials = [
      materialFull,
      materialChild1,
      materialChild2,
      materialChild3,
      materialChild4,
    ];
    expect(getCurrentSectionMaterials(materialShort, materials, fullStructured)).toStrictEqual([5]);
  });
});
