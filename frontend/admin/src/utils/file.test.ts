import { toBase64 } from './file';

describe('утилиты по работе с файлами', () => {
  test('функция toBase64 должна преобразовать файл изображения в строку base64', async () => {
    const file = new File([new Blob()], 'image.png', { type: 'image/png' });
    const res = await toBase64(file);
    expect(typeof res).toBe('string');
  });
});
