import {
  checkIsSSL,
  checkFileLink,
  checkSingleFileLink,
  checkIsFileEmpty,
  changeHostMaterial,
  createWebsocketUrl,
  checkIsIntelleka,
  getChatStatusByChatName,
  createUrlChat,
  getLocalStorageItem,
  checkingLinkText,
  checkIsBase64,
  hasLocationMatches,
} from './link';

describe('утилиты по работе со ссылками', () => {
  test('функция checkIsSSL должна проверить протокол', () => {
    expect(checkIsSSL()).toBeFalsy();
  });

  test('функция checkFileLink должна проверить является ли параметр ссылкой', () => {
    const file = new File(['text'], 'file');
    expect(checkFileLink(file)).not.toBeDefined();
    expect(checkFileLink('')).toBeFalsy();
  });

  test('checkSingleFileLink checkFileLink должна проверить является ли параметр файлом', () => {
    const file = new File(['text'], 'file');
    expect(checkSingleFileLink('')).not.toBeDefined();
    expect(checkSingleFileLink(file)).toBeDefined();
  });

  test('функция checkIsFileEmpty должна проверить является ли файл пустым', () => {
    const file = new File([''], 'file');
    expect(checkIsFileEmpty(file)).toBeDefined();
    expect(checkIsFileEmpty('')).not.toBeDefined();
  });

  test('функция changeHostMaterial должна изменить ссылку тестового сервера на ту, которая в адресной строке', () => {
    expect(changeHostMaterial('https://lms.promr-group.pp.ua/admin/material/1')).toBe(
      'https://localhost/admin/material/1',
    );
  });

  test('функция createWebsocketUrl должна создать урл для вебсокета в текущем домене', () => {
    expect(
      createWebsocketUrl(
        '/ws/support_chat/4459/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYwMTUzODM3LCJqdGkiOiI3MWViYzRlMTA3MjU0NWJkODc2ODNlOTAzOWZhN2I2ZiIsInVzZXJfaWQiOjQ0NTksInN0YXR1cyI6Miwic3RhdHVzX2Rpc3BsYXkiOiJcdTA0MWFcdTA0M2JcdTA0MzhcdTA0MzVcdTA0M2RcdTA0NDIifQ.9P6OOvaBVAAlNRX2jUEcJkYlPhrAWKtXHO78erg6cvU/',
      ),
    ).toBe(
      'ws://api.promr-group.pp.ua/ws//ws/support_chat/4459/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYwMTUzODM3LCJqdGkiOiI3MWViYzRlMTA3MjU0NWJkODc2ODNlOTAzOWZhN2I2ZiIsInVzZXJfaWQiOjQ0NTksInN0YXR1cyI6Miwic3RhdHVzX2Rpc3BsYXkiOiJcdTA0MWFcdTA0M2JcdTA0MzhcdTA0MzVcdTA0M2RcdTA0NDIifQ.9P6OOvaBVAAlNRX2jUEcJkYlPhrAWKtXHO78erg6cvU/',
    );
  });

  test('функция checkIsIntelleka должна определить принадлежность текущего домена группе intellekaDomains', () => {
    expect(checkIsIntelleka()).toBeFalsy();
  });

  test('функция getChatStatusByChatName должна определить статус чата по имени чата', () => {
    expect(getChatStatusByChatName('group_chat')).toBe(2);
    expect(getChatStatusByChatName('g_chat')).toBe(1);
    expect(getChatStatusByChatName('support_chat')).toBe(0);
  });

  test('функция createUrlChat должна сгенерировать урл чата для открытия вебсокета', () => {
    expect(typeof createUrlChat('support_chat', 1)).toBe('string');
    expect(createUrlChat('support_chat', 1).length).toBe(25);
  });

  test('Функция getLocalStorageItem должна вернуть значение элемента по ключу в localstorage, очищая его от кавычек', () => {
    localStorage.setItem('item', '"value-1"');
    expect(getLocalStorageItem('item')).toBe('value-1');
  });

  test('Функция checkingLinkText должна проверить наличие ссылки в тексте', () => {
    expect(checkingLinkText('item')).toBeFalsy();
    expect(checkingLinkText('http://website.com')).toBeTruthy();
  });

  test('функция checkIsBase64 должна проверяить является ли текст изображением base64', () => {
    expect(checkIsBase64('data:image/png;base64,iVBORw0KGgoAAA')).toBeTruthy();
    expect(checkIsBase64('iVBORw0KGgoAAA')).toBeFalsy();
  });

  test('функция hasLocationMatches проверяет, соответствует ли параметр адресу страницы', async () => {
    window.history.pushState({}, 'Page Title', 'admin/courses');
    const isCoursesPage = hasLocationMatches('admin/courses');
    const isCoursePage = hasLocationMatches('admin/courses/course/1');
    const isMessagesPage = hasLocationMatches('admin/messages/');
    const isAdminRootPage = hasLocationMatches('admin');
    expect(isCoursesPage).toBeTruthy();
    expect(isCoursePage).toBeFalsy();
    expect(isMessagesPage).toBeFalsy();
    expect(isAdminRootPage).toBeTruthy();
  });
});
