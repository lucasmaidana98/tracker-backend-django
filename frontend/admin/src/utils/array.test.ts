import { ADMIN_COURSES_MOCK, ADMIN_MOCK_USER } from '../mocks/admin';
import {
  splitToChunks,
  onlyUnique,
  randomize,
  rangeNumbers,
  alphaBetSort,
  getDataForSelect,
  getOnCheckUsers,
  sortChatsByLastMessage,
} from './array';

describe('утилиты по работе с массивами', () => {
  test('функция splitToChunks длжна разбить массив на три мелких массива', () => {
    expect(splitToChunks([1, 2, 3, 4, 5, 6, 7, 8, 9], 3)).toStrictEqual([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]);
  });

  test('функция onlyUnique должна отфильтровать только уникальные значения массива', () => {
    expect([1, 3, 4, 5, 5, 7, 3, 1].filter(onlyUnique)).toStrictEqual([1, 3, 4, 5, 7]);
  });

  test('функция randomize должна отсортировать массив в случайном порядке', () =>
    expect(randomize([1, 2, 3, 4, 5])).not.toBe([1, 2, 3, 4, 5]));

  test('функция rangeNumbers должна преобразовать число в массив из элементов', () =>
    expect(rangeNumbers(5)).toStrictEqual([1, 2, 3, 4, 5]));

  test('функция alphaBetSort должна отсортировать массив по алфавиту', () => {
    const input = [{ title: 'я' }, { title: 'он' }, { title: 'мы' }, { title: 'вы' }];
    const output = [{ title: 'вы' }, { title: 'мы' }, { title: 'он' }, { title: 'я' }];
    expect(alphaBetSort(input, 'title')).toEqual(output);
  });

  test('функция getDataForSelect должна вернуть массив для селекта', () => {
    const input = ADMIN_COURSES_MOCK;
    const output = ADMIN_COURSES_MOCK.map((c) => ({ label: c.title, value: c.id }));
    expect(input.map((el) => getDataForSelect(el))).toStrictEqual(output);
  });

  test('функция getOnCheckUsers массив пользователей с непроверенными тестами', () => {
    const input = [ADMIN_MOCK_USER, { ...ADMIN_MOCK_USER, on_check: 1 }];
    const output = [ADMIN_MOCK_USER].filter((_, i) => i === 1);
    expect(getOnCheckUsers(input)).toStrictEqual(output);
  });

  test('функция sortChatsByLastMessage должна отсортировать массив чатов по последнему сообщению', () => {
    const input = [
      { last_message_date: '2022-06-06T10:26:23.110528Z' },
      { last_message_date: '2022-09-06T10:26:23.110528Z' },
      { last_message_date: '2022-08-06T10:26:23.110528Z' },
    ];
    const output = [
      { last_message_date: '2022-09-06T10:26:23.110528Z' },
      { last_message_date: '2022-08-06T10:26:23.110528Z' },
      { last_message_date: '2022-06-06T10:26:23.110528Z' },
    ];

    expect(sortChatsByLastMessage(input)).toStrictEqual(output);
  });
});
