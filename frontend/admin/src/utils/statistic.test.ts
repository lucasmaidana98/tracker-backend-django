import {
  ADMIN_USER_ACTIVITIES_MOCK,
  ADMIN_USER_ACTIVITIES_MOCK_SORTED_BY_DATE,
} from '../mocks/admin';
import { getStuddingPosition, getUserActivityFormatted, userActivitySortByDate } from './statistic';

describe('утилиты по работе со статистикой пользователя', () => {
  test('Функция getStuddingPosition должна определить процент прохождения курса пользователем на основе даты начала и окончания обучения, а также сегодняшней даты относительно этого срока', () => {
    const start = '1-08.2022',
      end = '10-08-2022',
      output = 80;

    expect(getStuddingPosition(start, end)).toBeGreaterThan(output);
  });

  test('функция getUserActivityFormatted должна преобразовать данные статистики пользователя в читаемый вид', () => {
    expect(getUserActivityFormatted(ADMIN_USER_ACTIVITIES_MOCK)).toBe('03:29');
    expect(getUserActivityFormatted()).toBeNull();
  });

  test('функция userActivitySortByDate должна отсортировать данные пользовательской активности по дате', () => {
    expect(userActivitySortByDate(ADMIN_USER_ACTIVITIES_MOCK)).toStrictEqual(
      ADMIN_USER_ACTIVITIES_MOCK_SORTED_BY_DATE,
    );
  });
});
