import {
  stringDateToSeconds,
  localDateTimeToSeconds,
  localDateTimeToTimestamp,
  convertSecondsToFormat,
  convertTimestampToDate,
  convertLocalTimeToFormat,
  toHoursAndMinutes,
  stringToSeconds,
  formattedTime,
  getFromNow,
  normalizeDate,
  renderDate,
  renderShortDate,
  secondsToDays,
  secondsToTime,
} from './time';

describe('утилиты по работе со временем', () => {
  test('функция stringDateToSeconds преобразует дату в секунды', () => {
    expect(stringDateToSeconds('2022-06-29T13:27:10.306960Z')).toBe(1656509230);
    expect(stringDateToSeconds('2021-03-29T13:29:00.206061Z')).toBe(1617024540);
  });

  test('функция localDateTimeToSeconds преобразует локальное время в секунды', () => {
    expect(localDateTimeToSeconds('2021-03-29T13:29:00.206061Z')).toBe(1617024540.206);
    expect(localDateTimeToSeconds('2022-06-29T13:27:10.306960Z')).toBe(1656509230.306);
  });

  test('функция localDateTimeToTimestamp преобразует локальное время в timestamp', () => {
    expect(localDateTimeToTimestamp('2021-03-29T13:29:00.206061Z')).toBe(1617024540206);
    expect(localDateTimeToTimestamp('2022-06-29T13:27:10.306960Z')).toBe(1656509230306);
  });

  test('функция convertSecondsToFormat преобразует секунды в выбранный формат', () => {
    expect(convertSecondsToFormat(1617024540, 'DD-MM-YYYY')).toBe('29-03-2021');
    expect(convertSecondsToFormat(1617324540, 'DD-MM-YYYY')).toBe('02-04-2021');
  });

  test('функция convertTimestampToDate преобразует timestamp в выбранный формат с помощью библиотеки', () => {
    expect(convertTimestampToDate(1617024540000, 'DD-MM-YYYY')).toBe('29-03-2021');
    expect(convertTimestampToDate(1617324540000, 'DD-MM-YYYY')).toBe('02-04-2021');
  });

  test('функция convertLocalTimeToFormat преобразует локальное время в выбранный формат с помощью библиотеки', () => {
    expect(convertLocalTimeToFormat('Wed Aug 17 2022 23:55:58 GMT+0300', 'YYYY-MM-DD')).toBe(
      '2022-08-17',
    );
    expect(convertLocalTimeToFormat('Tue Aug 16 2022 23:55:58 GMT+0300', 'YYYY-MM-DD')).toBe(
      '2022-08-16',
    );
  });

  test('функция toHoursAndMinutes конвертирует секунды в часы и минуты', () => {
    expect(toHoursAndMinutes(300)).toBe('5 минут');
    expect(toHoursAndMinutes(600)).toBe('10 минут');
    expect(toHoursAndMinutes(3800)).toBe('1 час 3 минуты');
    expect(toHoursAndMinutes(4800, true)).toBe('1 час 20 минут,');
  });

  test('функция stringToSeconds превращает время в формате 00:00:00 в секунды', () => {
    expect(stringToSeconds('2:00:00')).toBe(7200);
    expect(stringToSeconds('1:45:00')).toBe(6300);
    expect(stringToSeconds('00:00:10')).toBe(10);
  });

  test('функция formattedTime преобразует время из формата 00:00:00 в формат ХХ часов ХХ минут ХХ секунд', () => {
    expect(formattedTime('00:00:10')).toBe('10 сек. ');
    expect(formattedTime('00:00:40')).toBe('40 сек. ');
    expect(formattedTime('02:40:00')).toBe('2 часа 40 минут');
  });

  test('функция getFromNow возвращает возвращает таймстемп времени, после вычета указанных в параметрах дней', () => {
    expect(new Date(getFromNow(2)).getDate()).toBe(new Date().getDate() - 2);
    expect(new Date(getFromNow(4)).getDate()).toBe(new Date().getDate() - 4);
    expect(new Date(getFromNow(10)).getDate()).toBe(new Date().getDate() - 10);
  });

  test('функция normalizeDate отнимает один месяц от значения даты для нормализации строки времени', () => {
    expect(normalizeDate('2022-12-20', '-')).toStrictEqual(new Date('2022-12-19T22:00:00.000Z'));
    expect(normalizeDate('2021-06-03', '-')).toStrictEqual(new Date('2021-06-02T21:00:00.000Z'));
    expect(normalizeDate('2021-05-16', '-')).toStrictEqual(new Date('2021-05-15T21:00:00.000Z'));
  });

  test('функция renderDate преобразует строку в дату', () => {
    expect(renderDate('2022-04-11T20:59:16.409659Z')).toBe('11 апр. 2022 г.');
    expect(renderDate('2021-05-16T20:59:16.409659Z')).toBe('16 мая 2021 г.');
    expect(renderDate('2021-01-18T20:59:16.409659Z')).toBe('18 янв. 2021 г.');
  });

  test('функция renderShortDate преобразует строку в дату', () => {
    expect(renderShortDate('2022-04-11T20:59:16.409659Z')).toBe('11.04.22');
    expect(renderShortDate('2021-05-16T20:59:16.409659Z')).toBe('16.05.21');
    expect(renderShortDate('2021-01-18T20:59:16.409659Z')).toBe('18.01.21');
  });

  test('Функция secondsToDays конвертирует секунды в дни', () => {
    expect(secondsToDays(1232112)).toBe(14);
    expect(secondsToDays(3452521)).toBe(39);
    expect(secondsToDays(2512112)).toBe(29);
  });

  test('Функция secondsToTime преобразует секунды в строку времени', () => {
    expect(secondsToTime(1232112)).toBe('06:15:12');
    expect(secondsToTime(3452521)).toBe('23:02:01');
    expect(secondsToTime(2512112)).toBe('01:48:32');
  });
});
