import axios from 'axios';

import { toFormData } from '@/utils/api';

export interface IMaterialsParams {
  id?: number;
  course?: number;
  title?: string;
  is_download?: boolean;
  parent?: string;
  ordering?: string;
}

export interface IMaterial {
  id: number;
  created: string;
  updated: string;
  title: string;
  text: string;
  video: string;
  pdf: string;
  is_active: boolean;
  is_download: boolean;
  rank: number;
  video_link: string;
  course: number;
  parent: number;
  original_link: number;
}

export async function getAllMaterials(params?: IMaterialsParams) {
  try {
    const { data } = await axios.get<IMaterial[]>(`/api/v1/client/courses/material/`, {
      params,
    });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export type TPassingStatus = 0 | 1 | 2;

export interface IMateriaPassingsParams {
  id?: number;
  material?: number;
  status?: TPassingStatus;
  ordering?: string;
}

export interface IMaterialPassingResponse {
  id: number;
  created: string;
  updated: string;
  status: TPassingStatus;
  material: number;
  user: number;
  userName: string;
  text: string;
}

export const CLIENT_MATERIALS_PASSING_URL = '/api/v1/client/courses/material_passing/';
export const CLIENT_ALL_MATERIALS_PASSING_URL = '/api/v1/client/courses/material_passing_all/';

/** прохождения текущего пользователя */
export async function getMaterialPassings(params?: IMateriaPassingsParams) {
  try {
    const { data } = await axios.get<IMaterialPassingResponse[]>(CLIENT_MATERIALS_PASSING_URL, {
      params,
    });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

/** прохождения всех пользователей */
export async function getAllMaterialPassings(params?: IMateriaPassingsParams) {
  try {
    const { data } = await axios.get<IMaterialPassingResponse[]>(CLIENT_ALL_MATERIALS_PASSING_URL, {
      params,
    });

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export interface ICreateMaterialPassing {
  status: number;
  material: number;
  user: number;
  userName?: string;
  text?: string;
}

export interface IUpdateMaterialPassing extends ICreateMaterialPassing {
  id: number;
}

export async function createMaterialPassing(model: ICreateMaterialPassing) {
  try {
    const { data } = await axios.post<IMaterialPassingResponse>(
      CLIENT_MATERIALS_PASSING_URL,
      toFormData(model),
    );

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function updateMaterialPassing({ id, ...model }: IUpdateMaterialPassing) {
  try {
    const { data } = await axios.patch<IMaterialPassingResponse>(
      `${CLIENT_MATERIALS_PASSING_URL}${id}/`,
      model,
    );

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
