import axios from 'axios';

import { IAppVersion } from './admin';

export interface IAuthParams {
  username?: string;
  password?: string;
}

export interface IAuthResponce {
  access: string;
  refresh: string;
}

export async function getToken(params: IAuthParams) {
  try {
    const { data } = await axios.post<IAuthResponce>('/api/v1/token/', params);

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export type IUserStatus = 0 | 1 | 2 | 3;

export interface ICurrentUser {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  design: number;
  old_password: string;
  user_status: IUserStatus;
  is_active: boolean;
  teacher_chat: boolean;
  group_chat: boolean;
  tech_chat: boolean;
}

export async function getCurrentUser() {
  try {
    const { data } = await axios.get<ICurrentUser>('/api/v1/user/user/');

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function getAppVersion() {
  try {
    const { data } = await axios.get<IAppVersion>('/api/v1/conference/apikey/get/');

    return data.apikey;
  } catch (error) {
    console.log(error);
    throw error;
  }
}
