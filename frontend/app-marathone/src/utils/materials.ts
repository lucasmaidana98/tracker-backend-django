export type IStructured = {
  id: number;
  title: string;
  parent?: number | null;
  children?: IStructured[];
  rank: number;
  is_download: boolean;
  pdf: string | File;
};

/**
 * Функция рекурсивно структурирует материалы по вложенностям
 */
export const getStructuredMaterials = <T extends IStructured>(arr: T[], parent?: number) => {
  const resultArray: IStructured[] = [];

  arr.forEach((el) => {
    /*eslint eqeqeq: "off"*/
    if (el.parent == parent) {
      const children = getStructuredMaterials(arr, el.id);

      if (children.length) {
        el.children = children;
      }
      resultArray.push(el);
    }
  });

  return resultArray;
};
