import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

type TElement = string | number | React.ReactElement;

const BasicTable: React.FC<{ titles: string[]; rows?: TElement[][]; isLoading?: boolean }> = (
  props,
) => {
  return (
    <TableContainer component={Paper} sx={{ maxHeight: { xs: 'calc(100vh - 95px)', lg: '100%' } }}>
      <Table
        sx={{ lg: { minWidth: 650 }, xs: { minWidth: '100%' } }}
        aria-label="simple table"
        stickyHeader
      >
        <TableHead>
          <TableRow>
            {props.titles.map((title, i) => (
              <TableCell align={!i ? 'left' : 'center'} key={i}>
                {title}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.rows?.map((el, i) => (
            <TableRow key={i}>
              {el.map((el, i) => (
                <TableCell
                  align={!i ? 'left' : 'center'}
                  data-label={
                    typeof props.titles[i] === 'object'
                      ? // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        props.titles[i].props?.children
                      : props.titles[i]
                  }
                  key={i}
                >
                  {el}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default BasicTable;
