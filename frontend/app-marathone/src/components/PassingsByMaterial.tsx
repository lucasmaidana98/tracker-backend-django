import { useQuery } from 'react-query';

import { Box, Grid, Typography } from '@mui/material';

import { getAllMaterialPassings } from '@/api/client';

export const PassingsByMaterial: React.FC<{ material: number; title: string }> = ({
  material,
  title,
}) => {
  const { data } = useQuery(['passings-by-material', material], () =>
    getAllMaterialPassings({ material }),
  );
  const passings = data?.sort((a, b) => {
    if (a.userName < b.userName) {
      return -1;
    }
    if (a.userName > b.userName) {
      return 1;
    }
    return 0;
  });

  return (
    <Box sx={{ mb: 1 }}>
      <Typography fontWeight="bold">{title}:</Typography>
      <Grid container>
        {!passings?.length && (
          <Typography sx={{ color: '#999' }}>Никем не запланировано</Typography>
        )}
        {passings?.map((p, i, self) => {
          const prev = self[i - 1];
          // предотвратить повторяющиеся попытки
          if (prev && prev.userName === p.userName) return null;

          return (
            <Grid
              item
              key={p.id}
              style={{
                paddingRight: 5,
                color: p.status === 1 ? 'red' : p.status === 0 ? 'orange' : 'green',
              }}
            >
              {`${p.userName} (${p.text || '?'})`}
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
};
