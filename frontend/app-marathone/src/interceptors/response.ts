import axios from 'axios';
import Cookie from 'js-cookie';

import { cookieKeys } from '@/hooks/queryClient';

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
      Cookie.remove(cookieKeys.token);
      if (!/^\/api\/v1\/conference/.test(error.response.config.url)) window.location.reload();
    }
    return Promise.reject(error);
  },
);
