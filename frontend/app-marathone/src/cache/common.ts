import { useQuery } from 'react-query';

import { getCurrentUser } from '@/api/common';

export const useCurrentUser = (enabled?: boolean) => {
  return useQuery('user', getCurrentUser, {
    enabled: enabled === undefined ? true : enabled,
    refetchOnWindowFocus: false,
    staleTime: 99999999,
  });
};
