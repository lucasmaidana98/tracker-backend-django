import { useMutation, useQuery } from 'react-query';

import {
  TPassingStatus,
  createMaterialPassing,
  getMaterialPassings,
  updateMaterialPassing,
} from '@/api/client';
import { ICurrentUser } from '@/api/common';
import { queryClient } from '@/hooks/queryClient';

const getPassingByMaterialKey = (material: number) => ['passing-by-material', material];

export const usePassingByMaterial = (material: number) => {
  const key = getPassingByMaterialKey(material);

  return useQuery(key, () => getMaterialPassings({ material }), {
    refetchOnWindowFocus: false,
    refetchOnMount: false,
  });
};

export const useCheck = (material: number, onError: () => void) => {
  const key = getPassingByMaterialKey(material);

  return useMutation<
    unknown,
    unknown,
    { status: TPassingStatus; user: ICurrentUser; text: string; id: number }
  >(
    async ({ status, user, text, id }) => {
      try {
        const res = await updateMaterialPassing({
          id,
          status: status,
          material,
          user: user.id,
          text,
          userName: user.first_name,
        });

        queryClient.setQueryData(key, [res]);
      } catch (e) {
        throw Error();
      }
    },
    { onError },
  );
};

export const usePlaninig = (material: number) => {
  const key = getPassingByMaterialKey(material);

  return useMutation<unknown, unknown, { user: ICurrentUser; text: string }>(
    async ({ user, text }) => {
      try {
        const res = await createMaterialPassing({
          material,
          user: user.id,
          status: 0,
          text,
          userName: user.first_name,
        });

        queryClient.setQueryData(key, [res]);
      } catch (e) {
        throw Error();
      }
    },
  );
};
