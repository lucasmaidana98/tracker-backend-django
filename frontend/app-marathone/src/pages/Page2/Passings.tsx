import { useQuery } from 'react-query';

import { Box } from '@mui/material';

import { getAllMaterials } from '@/api/client';
import { PassingsByMaterial } from '@/components/PassingsByMaterial';

export const Passings: React.FC<{ material: number }> = ({ material }) => {
  const { data: materials } = useQuery('materials', () => getAllMaterials({ is_download: true }));

  const childMaterials = materials?.filter((m) => m.parent === material);

  return (
    <Box>
      {childMaterials?.map((m) => (
        <PassingsByMaterial key={m.id} material={m.id} title={m.title} />
      ))}
    </Box>
  );
};
