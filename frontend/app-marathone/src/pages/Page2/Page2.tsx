import { useState } from 'react';
import { useQuery } from 'react-query';

import { ArrowCircleLeftOutlined, ArrowCircleRightOutlined } from '@mui/icons-material';
import { Grid, Typography } from '@mui/material';

import { getAllMaterials } from '@/api/client';
import Meta from '@/components/Meta';

import { Passings } from './Passings';

function Page2() {
  const { data: materials } = useQuery('materials', () => getAllMaterials({ is_download: true }));
  const [currentCol, setCurrentCol] = useState(0);

  const days = materials ? materials?.filter((m) => !m.parent).map((m) => m) : [];

  const hasPrev = currentCol !== 0;
  const hasNext = currentCol < (days?.length || 0) - 1;

  return (
    <Grid container sx={{ p: 2 }}>
      <Meta title="Общая статистика" />
      <Typography sx={{ textAlign: 'center', width: '100%', py: 1, fontSize: 24 }}>
        Общая статистика
      </Typography>
      <Grid container sx={{ justifyContent: 'space-between', py: 2 }}>
        <ArrowCircleLeftOutlined
          onClick={() => hasPrev && setCurrentCol(currentCol - 1)}
          sx={{ opacity: hasPrev ? 1 : 0.2 }}
        />
        {!!days?.length && days[currentCol].title}
        <ArrowCircleRightOutlined
          onClick={() => hasNext && setCurrentCol(currentCol + 1)}
          sx={{ opacity: hasNext ? 1 : 0.2 }}
        />
      </Grid>
      {days[currentCol] && <Passings material={days[currentCol].id} />}
    </Grid>
  );
}

export default Page2;
