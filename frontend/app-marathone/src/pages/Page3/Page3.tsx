import Meta from '@/components/Meta';
import { FullStatistic } from './FullStatistic';

function Page3() {
  return (
    <>
      <Meta title="page 3" />
      <FullStatistic />
    </>
  );
}

export default Page3;
