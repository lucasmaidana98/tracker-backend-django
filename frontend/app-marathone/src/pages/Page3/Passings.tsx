import { Box } from '@mui/material';

import { IMaterial } from '@/api/client';
import { PassingsByMaterial } from '@/components/PassingsByMaterial';

export const Passings: React.FC<{ material: number; materials?: IMaterial[] }> = (props) => {
  const childMaterials = props.materials?.filter((m) => m.parent === props.material);

  return (
    <Box sx={{ p: 2 }}>
      {childMaterials?.map((m) => (
        <PassingsByMaterial key={m.id} material={m.id} title={m.title} />
      ))}
    </Box>
  );
};
