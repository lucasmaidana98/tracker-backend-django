import { useState } from 'react';
import { useQuery } from 'react-query';

import ArrowLeft from '@mui/icons-material/ArrowLeftOutlined';
import {
  Box,
  Button,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Typography,
} from '@mui/material';

import { getAllMaterials } from '@/api/client';
import { CenteredFlexBox } from '@/components/styled';

import { Passings } from './Passings';

export const FullStatistic = () => {
  const { data: materials } = useQuery(
    'all-client-materials',
    () => getAllMaterials({ is_download: false, ordering: '-id' }),
    {
      staleTime: 999999,
      refetchOnWindowFocus: false,
    },
  );
  const filtered = materials?.filter((m) => !m.parent);
  const [material, setMaterial] = useState<number>(0);
  const [day, setDay] = useState<string>('');

  const handleToggle = (materialId: number, day: string) => {
    setMaterial(materialId);
    setDay(day);
  };

  function transformArrayToObject(arr: any) {
    if (!arr) return [];
    const obj: any = {};
    arr.forEach((elem: any) => {
      const day = elem.created.slice(0, 10);
      if (!obj[day]) {
        obj[day] = [];
      }
      obj[day].push(elem);
    });
    return obj;
  }
  const obj = transformArrayToObject(filtered);

  return (
    <>
      <CenteredFlexBox flexDirection="column">
        <Typography fontSize={25} mt={1}>
          История
        </Typography>
        <Typography fontSize={17} mb={1} color="#999">
          {day}
        </Typography>
      </CenteredFlexBox>
      {!!material && (
        <Box sx={{ mt: 2, ml: 2 }} onClick={() => handleToggle(0, '')}>
          <Button variant="outlined" disabled startIcon={<ArrowLeft />}>
            Назад
          </Button>
        </Box>
      )}
      {!!material && <Passings material={material} materials={materials} />}

      <List
        sx={{
          width: '100%',
          maxWidth: '100%',
          bgcolor: 'background.paper',
          position: 'relative',
          overflow: 'auto',
          maxHeight: 'calc(100vh - 100px)',
          '& ul': { padding: 0 },
        }}
        subheader={<li />}
      >
        {!material &&
          Object.keys(obj).map((k, i, self) => (
            <li key={`section-${k}`}>
              <ul>
                <ListSubheader sx={{ bgcolor: '#f1f1f1' }}>
                  <Grid container justifyContent="space-between">
                    <Grid item>
                      <b style={{ textTransform: 'uppercase' }}>Неделя-{self.length - i} </b>
                    </Grid>
                    <Grid item>
                      <span style={{ opacity: 0.7 }}>{k}</span>
                    </Grid>
                  </Grid>
                </ListSubheader>
                {obj[k]
                  .sort((a: any, b: any) => b.rank - a.rank)
                  .map((el: any) => (
                    <ListItem
                      key={`item-${k}-${el.id}`}
                      onClick={() => handleToggle(el.id, el.title)}
                    >
                      <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item>
                          <ListItemText primary={el.title} />
                        </Grid>
                        <Grid item>
                          <Button variant="outlined" size="small">
                            Статистика
                          </Button>
                        </Grid>
                      </Grid>
                    </ListItem>
                  ))}
              </ul>
            </li>
          ))}
      </List>
    </>
  );
};
