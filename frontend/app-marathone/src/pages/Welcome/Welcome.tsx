import { useCurrentUser } from '@/cache/common';
import Meta from '@/components/Meta';
import Admin from '@/layout/Admin';
import Client from '@/layout/Client';

function Welcome() {
  const { data: user } = useCurrentUser();

  if (user?.user_status === 2) return <Client />;
  if (user?.user_status === 0) return <Admin />;

  return <Meta title="Главная" />;
}

export default Welcome;
