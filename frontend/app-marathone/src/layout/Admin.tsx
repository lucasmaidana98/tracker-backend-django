import { FullSizeCenteredFlexBox } from '@/components/styled';

const Admin: React.FC = () => {
  return <FullSizeCenteredFlexBox>admin</FullSizeCenteredFlexBox>;
};

export default Admin;
