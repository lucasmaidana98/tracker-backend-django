import React, { useEffect, useMemo, useState } from 'react';
import { useQuery } from 'react-query';

import { Box, TextareaAutosize } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import { TPassingStatus, getAllMaterials } from '@/api/client';
import { useCheck, usePassingByMaterial, usePlaninig } from '@/cache/client';
import { useCurrentUser } from '@/cache/common';
import CheckItem from '@/components/CheckItem';
import Meta from '@/components/Meta';
import { getStructuredMaterials } from '@/utils/materials';

const border = '1px solid rgba(224, 224, 224, 1)';

const Client: React.FC = () => {
  const { data } = useQuery('materials', () => getAllMaterials({ is_download: true }));

  const materials = useMemo(() => (data ? getStructuredMaterials(data) : []), [data]);

  return (
    <>
      <Meta title="Marathone" />

      <TableContainer style={{ height: 'calc(100vh - 65px)' }}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell
                style={{
                  minWidth: 10,
                  textAlign: 'center',
                  borderRight: border,
                  position: 'sticky',
                  left: 0,
                  background: 'white',
                  zIndex: 10,
                }}
              >
                День
              </TableCell>
              <TableCell
                style={{
                  minWidth: 20,
                  textAlign: 'center',
                  borderRight: border,
                }}
              >
                Подъем
              </TableCell>
              <TableCell style={{ minWidth: 20, textAlign: 'center', borderRight: border }}>
                Чтение Библии
              </TableCell>
              <TableCell
                style={{
                  minWidth: 20,
                  textAlign: 'center',
                  whiteSpace: 'nowrap',
                }}
              >
                Своя цель
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {materials?.map((m) => {
              return (
                <TableRow key={m.id} sx={{ height: "auto" }}>
                  <TableCell
                    align="left"
                    sx={{
                      borderRight: border,
                      position: 'sticky',
                      left: 0,
                      background: 'white',
                    }}
                  >
                    {m.title}
                  </TableCell>
                  {m.children?.map((child, i) => (
                    <Passing
                      key={child.id}
                      material={child.id}
                      isWakeUp={i === 0}
                      isGoal={i == 2}
                    />
                  ))}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Client;

const Passing: React.FC<{
  material: number;
  isWakeUp?: boolean;
  isGoal: boolean;
}> = ({ material, isWakeUp, isGoal }) => {
  const { data: user } = useCurrentUser();
  const [status, setStatus] = useState<TPassingStatus>(0);

  const { data, isLoading } = usePassingByMaterial(material);
  const lastPassing = data?.length ? data[0] : undefined;

  const { mutate, isLoading: mutating } = useCheck(material, () =>
    setStatus(lastPassing?.status || 0),
  );

  const { mutate: plan, isLoading: planning } = usePlaninig(material);

  const [text, setText] = useState('');

  useEffect(() => {
    if (lastPassing) {
      setStatus(lastPassing.status);
      setText(lastPassing.text);
    }
  }, [lastPassing]);

  const handleChange = (status: TPassingStatus) => {
    if (!lastPassing) return;
    setStatus(status);
    sendRequest(lastPassing.id, status);
  };

  const sendRequest = (id: number, status: TPassingStatus) =>
    user && mutate({ status, user, text, id });

  return (
    <TableCell align="center" sx={{ borderRight: border }}>
      {!lastPassing ? (
        <button className="button" onClick={() => user && plan({ user, text })} disabled={planning}>
          Запланировать
        </button>
      ) : (
        <>
          {isGoal ? (
            <TextareaAutosize
              className='textarea'
              placeholder="Цель"
              value={text}
              onChange={(e) => setText(e.target.value)}
              onFocus={() => handleChange(0)}
              onBlur={() => sendRequest(lastPassing.id, status)}
            />
          ) : (
            <input
              className="input"
              placeholder={isWakeUp ? 'Время' : 'Ссылка'}
              value={text}
              onChange={(e) => setText(e.target.value)}
              type="text"
              onFocus={() => handleChange(0)}
              onBlur={() => sendRequest(lastPassing.id, status)}
            />
          )}
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              textTransform: 'uppercase',
              my: 1,
            }}
          >
            <CheckItem
              onClick={() => !mutating && handleChange(status === 2 ? 0 : 2)}
              active={!isLoading && status === 2}
              value={2}
            >
              ✓
            </CheckItem>

            <CheckItem
              onClick={() => !mutating && handleChange(status === 1 ? 0 : 1)}
              active={!isLoading && status === 1}
              value={1}
            >
              ×
            </CheckItem>
          </Box>
        </>
      )}
    </TableCell>
  );
};
